$().ready(function () {
    /**
     * Product Add/Edit form
     */
    $("#ProductForm").validate({
        rules: {
            p_name: {
                    required: true,
                    remote: {
                        url: base_url + "products/checkname/product",
                        type: "post"
                    }
            },
            p_code: {
                    required: true,
                    remote: {
                        url: base_url + "products/checkname/code",
                        type: "post"
                    }
            },
//            type_id: "required",
//            unit_id: "required",
            no_of_qty: "required"
        },
        messages: {
            p_name: {
                    required: "Please enter product name.",
                    remote: "Product name is already in use."
                },
            p_code: {
                    required: "Please enter reference code.",
                    remote: "Reference code is already in use."
                },
//            type_id: "Please select type.",
//            unit_id: "Please select unit.",
            no_of_qty: "Enter number of quantity."
        }
    });

    /**
     * Company Add/Edit form
     */
    $("#CompanyForm").validate({
        wrapper: "div",
        rules: {
            name: "required",
            short_name: "required",
        },
        messages: {
            name: "Please enter manufacturer/stockist name.",
            short_name: "Please enter short name.",
        }
    });

    /**
     * Unit Add/Edit form
     */
    $("#UnitForm").validate({
        wrapper: "div",
        rules: {
            name: "required",
        },
        messages: {
            name: "Please enter unit name.",
        }
    });

    /**
     * Type Add/Edit form
     */
    $("#TypeForm").validate({
        wrapper: "div",
        rules: {
            name: "required",
        },
        messages: {
            name: "Please enter type name.",
        }
    });

    /**
     * Batch Add/Edit form
     */
    $("#BatchForm").validate({
        wrapper: "div",
        rules: {
            b_name: "required",
            p_id: "required",
        },
        messages: {
            b_name: "Please enter batch name.",
            p_id: "Please select product.",
        }
    });
    
    /**
     * User Add/Edit form
     */
    $("#UserForm").validate({
        rules: {
            name: "required",
            username: {
                    required: true,
                    remote: {
                        url: base_url + "user/checkname",
                        type: "post"
                    }
            },
            password: "required"
        },
        messages: {
            
            name: "Please enter name.",
            username: {
                    required: "Please enter username.",
                    remote: "Username is already in use."
                },
            password: "Enter password."
        }
    });
});