/*---LEFT BAR ACCORDION----*/
$(function () {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
    $('.default-date-picker').datepicker({
        format: defaultDateFormat
    }).on('changeDate', function () {
        $('.datepicker').hide();
    });
    
    

    $('.price_per_person_table input[type="text"]').attr("disabled", 'disabled');

    $('.price_per_person_table input[type="checkbox"]').change(function () {
        if ($(this).is(":checked")) {
            $(this).parent().parent().find('input[type="text"]').removeAttr('disabled');
            $(this).parent().parent().find('input[type="text"]').each(function () {
                $(this).rules("add", {
                    required: true
                });
            });
        } else {
            $(this).parent().parent().find('input[type="text"]').attr("disabled", 'disabled');
            $(this).parent().parent().find('input[type="text"]').removeClass('error');
            $(this).parent().parent().find('label.error').remove();
            $(this).parent().parent().find('input[type="text"]').each(function () {
                $(this).rules("add", {
                    required: false
                });
            });
        }
    });
    
    $('#city_id').on('change',function(){
        
        var countryid = $('#country_id').val();
        var cityid  =   $('#city_id').val();
        if(countryid!='' && cityid==''){
            var data = { country_id:countryid} ;   
        }
        else {
            var data = { country_id:countryid,city_id:cityid} ;
        }
        $.ajax({
            method: "POST",
            url: base_url+"product/getTopAttraction",
            data: data
        })
        .done(function( msg ) {
            $('.attraction_id').html(msg);
        })
        .error(function(){
             //alert('Please Try Again!')
        });
    });
    
    
    $('#pickup_type').change(function () {
        $("#meeting_point_div").hide();
        $("#dropoff_location_div").hide()
        if ($(this).val() == 'mpdo') {
            $("#meeting_point_div").show();
            $("#dropoff_location_div").show();
        }
        if ($(this).val() == 'hpdo') {
            $("#dropoff_location_div").show();
        }
    });
    $('#price_type').change(function () {
        if ($(this).val() == 'persona') {
            $('#price_per_person_div').show();
            $('#price_per_group_div').hide();
        } else if ($(this).val() == 'group') {
            $('#price_per_person_div').hide();
            $('#price_per_group_div').show();
        } else {
            $('#price_per_person_div').hide();
            $('#price_per_group_div').hide();
        }
    });
    $('#guidance_methode_div input[type="radio"]').change(function () {
        if ($(this).val() == 'live_tour') {
            $('#guidance_language_div').show();
        } else {
            $('#guidance_language_div').hide();
        }
    });
    $('.display_rank').on('change',function(){
       var rank = $(this).val();
       var id = $(this).data('id');
       var url = base_url+"top_attractions/update_rank";
       $.ajax({
            method: "POST",
            url: url,
            data: { id:id,display_rank:rank}
            })
            .done(function( msg ) {
                 location.reload();
            }).error(function(){
                //alert('Please Try Again!')
            });
    });
    
    $('.display_rank_product').on('change',function(){
       var obj = $(this);
       var rank = $(this).val();
       var id = $(this).data('id');
       var url = base_url+"product/update_rank";
       $.ajax({
            method: "POST",
            url: url,
            data: { id:id,display_rank:rank}
            })
            .done(function( msg ) {
                location.reload();
                
            }).error(function(){
               // alert('Please Try Again!')
            });
    });
    
    
    
    $('.display_rank_destination').on('change',function(){
       var rank = $(this).val();
       var id = $(this).data('id');
       var url = base_url+"top_destinations/update_rank";
       $.ajax({
            method: "POST",
            url: url,
            data: { id:id,display_rank:rank}
            })
            .done(function( msg ) {
              location.reload();
            }).error(function(){
               // alert('Please Try Again!')
            });
    });
    $('#product_duration').change(function () {
        if ($(this).val() == 'days') {
            $('#product_duration_days_div').show();
            $('#product_duration_hr_div').hide();
        } else if ($(this).val() == 'hours') {
            $('#product_duration_days_div').hide();
            $('#product_duration_hr_div').show();
        } else {
            $('#product_duration_days_div').hide();
            $('#product_duration_hr_div').hide();
        }
    });
    $('#date_varies').change(function () {
        $('#add_new_date_range_btn_div').toggle();
        $('.date_range_div').toggle();
        $('.NewAddedPriceDiv').remove();
        price_div_count = 0;
        price_div_array = [];
    });
    $('#date_varies_group').change(function () {
        $('#add_new_date_range_btn_div_group').toggle();
        $('.date_range_div_group').toggle();
        $('.NewAddedPriceDivGroup').remove();
        group_price_div_count = 0;
        group_price_div_array = [];
    });
    $('#availability_type').change(function () {
        $('#start_time_div').hide()
        $('#opening_hour_div').hide()
        if ($(this).val() == 'starting_time') {
            $('#start_time_div').show()
        } else if ($(this).val() == 'operating_hour') {
            $('#opening_hour_div').show()
        }
    });
    $('input[name="price_scale"]').change(function () {
        update_price_table('scalechanged');
        $('.afterscaleDiv').show();
        $('.price_scale_to').removeClass('error');
        if ($(this).val() == 'yes') {
            $('#price_scale_div').show();
        } else {
            $('#price_scale_div').hide();
        }
    });
    $('#all').change(function () {
        if ($(this).is(":checked")) {
            $('.working-plan').find('input[type="checkbox"]').prop('checked', true);
        } else {
            $('.working-plan').find('input[type="checkbox"]').prop('checked', false);
        }

    });

    $('.price_per_person_table input[type="checkbox"]').each(function () {
        if ($(this).is(":checked")) {
            $(this).parent().parent().find('input[type="text"]').removeAttr('disabled');
            /*$(this).parent().parent().find('input[type="text"]').each(function(){
             $(this).rules( "add", {
             required: true
             }); 
             });*/
        } else {
            $(this).parent().parent().find('input[type="text"]').attr("disabled", 'disabled');
            $(this).parent().parent().find('input[type="text"]').removeClass('error');
            $(this).parent().parent().find('label.error').remove();
            /*$(this).parent().parent().find('input[type="text"]').each(function(){
             $(this).rules( "add", {
             required: false
             }); 
             });*/
        }
    });
    
    $('.isDefault').on('click',function(){
       $('.isDefault').not($(this)).removeAttr('checked');
       $('.isDefault').next('.error').hide();
       //$(this).attr('checked',true) ; 
       //alert($(this).val() + "dsds") ;
        
    });
    
   /* 
    $('#is_iban').click(function () {
        if ($(this).is(':checked'))
        {
            $(this).parents('.payment_information').find('.hideClass').removeClass('hideClass').addClass('showClass');
            $('.ibanClass').attr('required', '');
            $('.swift_account').removeClass('showClass').addClass('hideClass').find('input').removeAttr('required');
        }
        else
        {
            $(this).val('0');
            $(this).parents('.payment_information').find('.showClass').removeClass('showClass').addClass('hideClass');
            $('.ibanClass').removeAttr('required');
            $('.swift_account').removeClass('hideClass').addClass('showClass').find('input').attr('required', '');

        }

    });*/
        
        $('#is_iban').on('click',function(){
                if($(this).is(":checked"))
                {
                   $('.is_iban').removeClass('hideClass'); 
                }
                else
                {
                    $('.is_iban').removeClass('showClass').addClass('hideClass'); 
                }
            
        });
        
        $('#is_australian_supplier').on('click', function () {
            if ($(this).is(":checked"))
            {
                $('.is_australian_supplier').removeClass('hideClass');
            }
            else
            {
                $('.is_australian_supplier').removeClass('showClass').addClass('hideClass');
            }
        });
        
         $('#is_swift_code').on('click', function () {
            if ($(this).is(":checked"))
            {
                $('.is_swift_code').removeClass('hideClass');
            }
            else
            {
                $('.is_swift_code').removeClass('showClass').addClass('hideClass');
            }
        });
        
         $('#is_routing').on('click', function () {
            if ($(this).is(":checked"))
            {
                $('.is_routing').removeClass('hideClass');
            }
            else
            {
                $('.is_routing').removeClass('showClass').addClass('hideClass');
            }
        });
        
        
        $('.removeImage').click(function(){
            var id = $(this).data('id') ;
            var image_name = $(this).data('name');
            var url =  $(this).data('url');
            $.ajax({
            method: "POST",
            url: url,
            data: { id:id,banner:image_name}
            })
            .done(function( msg ) {
                $('.'+id).attr('src',base_url+"assets/uploads/img/noimage.png") ;
                $('.image_name').attr('required', '');
                $('.image_name').data('flag','0') ;
                $('.removebtn').hide();
            }).error(function(){
                //alert('Please Try Again!')
            });
        });
        
        
        
        
        
        
        
        
        
        $('.removelogo').click(function(){
            var id = $(this).data('id') ;
            var image_name = $(this).data('name');
            var url =  $(this).data('url');
            $.ajax({
            method: "POST",
            url: url,
            data: { id:id,banner:image_name}
            })
            .done(function( msg ) {
                
               // $('.logo_setting').attr('src',base_url+"assets/uploads/img/noimage.png") ;
                $('.image_name').attr('required', '');
                $('.image_name').data('flag','0') ;
                $('.removebtn').hide();
            }).error(function(){
               // alert('Please Try Again!')
            });
        });
        
        $('.topAttractionImageFile').on('change',function(){
            $('.removeTopAttractionbtn').show();
            $('.topAttractionImage').css('display','none') ;
            $(".topAttractionImagePreview").css('display','inline-block')
            $('.btn-file >.fileupload-exists ').css('display','inline-block');
            $('.btn-file > .fileupload-new').css('display','none');
        });
        
        $('.removeTopAttractionbtn').click(function(){
            var id = $(this).data('id') ;
            if(id!='' && id!=undefined) { 
                var image_name = $(this).data('name');
                $.ajax({
                method: "POST",
                url: base_url+"top_attractions/delete_image",
                data: { id:id,banner:image_name}
                })
                .done(function( msg ) {
                        $('.topAttractionImage > img').attr('src','').attr('src',base_url+"assets/uploads/img/noimage.png") ;
                    //$('.removeTopAttractionbtn').hide();
                        $(".topAttractionImageFile").val('');
                        $('.removeTopAttractionbtn').removeClass('showRemovebtn').hide();
                        $(".topAttractionImagePreview").css('display','none').html(''); 
                        $('.topAttractionImage').css('display','inline-block') ;
                        $('.btn-file >.fileupload-exists ').css('display','none');
                        $('.btn-file > .fileupload-new').css('display','inline-block');
                }).error(function(){
                   // alert('Please Try Again!')
                });
            }
            else
            {
                //$('.topAttractionImage').css('display','inline-block') ;
                $(".topAttractionImageFile").val('');
                $('.removeTopAttractionbtn').hide();
                $(".topAttractionImagePreview").css('display','none').html(''); 
                $('.topAttractionImage').css('display','inline-block') ;
                $('.btn-file >.fileupload-exists ').css('display','none');
                $('.btn-file > .fileupload-new').css('display','inline-block');
                /*$('.fileupload-new').css('display','inline-block') ;*/
                
            }
        });
        
        $('.destinationImage').on('change',function(){
            $('.removeTopDestination ').show();
            $('.topDestinationImage').css('display','none') ;
            $(".fileupload-preview").css('display','inline-block')
            $('.btn-file >.fileupload-exists ').css('display','inline-block');
            $('.btn-file > .fileupload-new').css('display','none');
        });
        
        $('.removeTopDestination').click(function(){
            var id = $(this).data('id') ;
            if(id!='' && id!=undefined) { 
                var image_name = $(this).data('name');
                $.ajax({
                method: "POST",
                url: base_url+"top_destinations/delete_image",
                data: { id:id,image_name:image_name}
                })
                .done(function( msg ) {
                        $('.topDestinationImage > img').attr('src','').attr('src',base_url+"assets/uploads/img/noimage.png") ;
                        $(".destinationImage").val('');
                        $('.removeTopDestination').removeClass('showRemovebtn').hide();
                        $(".fileupload-preview").css('display','none').html(''); 
                        $('.topDestinationImage').css('display','inline-block') ;
                        $('.btn-file >.fileupload-exists ').css('display','none');
                        $('.btn-file > .fileupload-new').css('display','inline-block');
                }).error(function(){
                    //alert('Please Try Again!')
                });
            }
            else
            {
                $(".destinationImage").val('');
                $('.removeTopDestination').hide();
                $(".fileupload-preview").css('display','none').html(''); 
                $('.topDestinationImage').css('display','inline-block') ;
                $('.btn-file >.fileupload-exists ').css('display','none');
                $('.btn-file > .fileupload-new').css('display','inline-block');
            }
        });
        
        $('.bannerImageFile').on('change',function(){
            $('.removeBanner').show();
            $('.bannerImage').css('display','none') ;
            $(".fileupload-preview").css('display','inline-block')
            $('.btn-file >.fileupload-exists ').css('display','inline-block');
            $('.btn-file > .fileupload-new').css('display','none');
            //$('#image_name').data('flag','1') ;
        });
        
        $('.removeBanner').click(function(){
            var id = $(this).data('id') ;
            if(id!='' && id!=undefined) { 
                var image_name = $(this).data('name');
                $.ajax({
                method: "POST",
                url: base_url+"banner/delete_image",
                data: { id:id,image_name:image_name}
                })
                .done(function( msg ) {
                        $('.bannerImage > img').attr('src','').attr('src',base_url+"assets/uploads/img/noimage.png") ;
                        $(".bannerImageFile").val('');
                        $('#image_name').attr('required', '');
                        $('#image_name').data('flag','0') ;
                        $('.removeBanner').removeClass('showRemovebtn').hide();
                        $(".fileupload-preview").css('display','none').html(''); 
                        $('.bannerImage').css('display','inline-block') ;
                        $('.btn-file >.fileupload-exists ').css('display','none');
                        $('.btn-file > .fileupload-new').css('display','inline-block');
                }).error(function(){
                   // alert('Please Try Again!')
                });
            }
            else
            {
                $(".bannerImageFile").val('');
                $('.removeBanner').hide();
                $(".fileupload-preview").css('display','none').html(''); 
                $('.bannerImage').css('display','inline-block') ;
                $('.btn-file >.fileupload-exists ').css('display','none');
                $('.btn-file > .fileupload-new').css('display','inline-block');
            }
        });
        
         
        
    
});

//var add_ons_count = 1;
//var add_ons_array = [];
//var scale_count = 0;
//var scale_array = [];
//var price_div_count = 0;
//var price_div_array = [];
//var group_price_div_count = 0;
//var group_price_div_array = [];
//var special_date_count = 0;
//var special_date_array = [];
//var customer_request_count = 0;
//var customer_request_array = [];
//var contacts_count = 0;
//var contacts_array = [];

var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        /*if (diff > 0)
            $("#sidebar").scrollTo("-=" + Math.abs(diff), 500);
        else
            $("#sidebar").scrollTo("+=" + Math.abs(diff), 500);*/
    });

//    sidebar toggle

    $(function () {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#container').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#container').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });

// custom scrollbar
    $("#sidebar").niceScroll({styler: "fb", cursorcolor: "#428bca", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: ''});

    $("html").niceScroll({styler: "fb", cursorcolor: "#428bca", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled: false, cursorborder: '', zindex: '1000'});

// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


//    tool tips

    $('.tooltips').tooltip();

//    popovers

    $('.popovers').popover();



// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

    $('#country_id').change(function () {
        if ($('#country_id').val() != "") {
            $.ajax({
                url: base_url + "location/getstates",
                type: "POST",
                data: {country_id: $('#country_id').val()},
                success: function (result) {
                    $("#state_id").html(result);
                }
            });
            $.ajax({
                url: base_url + "location/getcitiesjson",
                type: "POST",
                dataType :'html',
                data: {country_id: $('#country_id').val()},
                success: function (result) {
                    var data = $.parseJSON(result);
                    $('#city_id').html('');
                    var html = '<option value="">--Select City---</option>';
                    $.each( data, function( i, val ) {
                        if(val.is_front=='1'){
                            html+="<option value="+val.city_id+">"+val.city_name+'--front'+"</option>";
                        }
                        else
                        {
                            html+="<option value="+val.city_id+">"+val.city_name+"</option>";
                        }
                            if(i==data.length-1){
                             $('#city_id').html(html);
                           }
                    });
                    $('#city_id').trigger('change');
                }
            });
        }
    });
    
    $('#country_id_status').change(function () {
        if ($('#country_id_status').val() != "") {
            $.ajax({
                url: base_url + "location/getstates",
                type: "POST",
                data: {country_id: $('#country_id_status').val()},
                success: function (result) {
                    $("#state_id").html(result);
                }
            });
            $.ajax({
                url: base_url + "location/getcitiesjson",
                type: "POST",
                dataType :'html',
                data: {country_id: $('#country_id_status').val(),'status':1,'is_front':1},
                success: function (result) {
                    var data = $.parseJSON(result);
                    $('#city_id').html('');
                    var html = '<option value="">--Select City---</option>';
                    $.each( data, function( i, val ) {
                        html+="<option value="+val.city_id+">"+val.city_name+'--'+val.is_front+"</option>";
                         if(i==data.length-1){
                             $('#city_id').html(html);
                           }
                    });
                    $('#city_id').trigger('change');
                }
            });
        }
    });
    

    $('#state_id').change(function () {
        if ($('#state_id').val() != "") {
            $.ajax({
                url: base_url + "location/getcities",
                type: "POST",
                data: {state_id: $('#state_id').val()},
                success: function (result) {
                    $("#city_id").html(result);
                }
            });
        }
    });
    //var upload_button_count = 3;
    $('#ProductForm #add_more_images').click(function () {
        if (upload_button_count < 35) {
            upload_button_count = upload_button_count + 1;
            var UploadButtomHtml = '<div class="fileupload fileupload-new col-md-4" data-provides="fileupload"><div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div><div> <span class="btn btn-white btn-file"><span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select image</span> <span class="fileupload-exists">Change</span><input type="file" class="default imageClass" id="image_name_'+upload_button_count+'" name="images_' + upload_button_count + '"/> </span> <a href="javascript:void(0);" data-radio="'+upload_button_count+'" class="btn btn-danger fileupload-exists removeProduct" data-dismiss="fileupload"><i class="fa fa-trash"></i>Remove</a></div><span>Note: Please upload image with minimum image dimension 1200 X 800 & Recommended image size is 2MB</span><div class="form-group"><label for="status" style="white-space:nowrap;" class="col-lg-4 control-label require">Main Image</label><div class="radio-inline isPrimary"><input  type="radio" value="1"  class="isDefault r_'+upload_button_count+'" name="images[' + upload_button_count + '][is_default]"></div></div>  <textarea class="form-control" maxlength="100" style="margin-top: 10px;" name="images[' + upload_button_count + '][image_desc]" id="image_desc_' + upload_button_count + '" placeholder="Enter Description" rows="8"></textarea></div>';
            $('#extra_file_upload_div').append(UploadButtomHtml);
            $('#image_desc_' + upload_button_count).MaxLength({
                MaxLength: 100,
                //CharacterCountControl: $('#image_desc_' + upload_button_count + '_counter')
            });
        }
    });

    $('#supplierAddForm #add_new_contact').click(function () {
        if (contacts_array.length < 5) {
            contacts_count = contacts_count + 1;
            contacts_array.push(contacts_count);
            var ContactsHtml = '<div id="contact_details_0"><legend>Contact: <input type="button" name="delete_contact_0" id="delete_contact_0" class="btn btn-danger pull-right" value="Delete Contact" onclick="removeContact(' + contacts_count + ')"></legend>                    <div class="form-group">                        <label for="contact_title_0" class="require col-lg-2 control-label">Title</label>                        <div class="col-lg-3">                            <select name="contact[0][title]" id="contact_title_0" class="form-control" required="">                                <option value=""> -- Select Title -- </option>                                <option value="Mr">Mr.</option>                                <option value="Mrs">Mrs.</option>                                <option value="Miss">Miss.</option>                            </select>                        </div>                    </div>                    <div class="form-group">                        <label for="contact_first_name_0" class="require col-lg-2 control-label">First Name</label>                        <div class="col-lg-6">                            <input type="text" maxlength="50" placeholder="" id="contact_first_name_0" name="contact[0][first_name]" class="form-control" required="">                        </div>                    </div>                    <div class="form-group">                        <label for="contact_last_name_0" class="require col-lg-2 control-label">Last Name</label>                        <div class="col-lg-6">                            <input type="text" maxlength="50" placeholder="" id="contact_last_name_0" name="contact[0][last_name]" class="form-control" required="">                        </div>                    </div>                    <div class="form-group">                        <label for="contact_phone_0" class="require col-lg-2 control-label">Phone</label>                        <div class="col-lg-6">                            <input type="text" maxlength="50" placeholder="" id="contact_no_0" name="contact[0][contact_no]" class="form-control phone_number" required="">                        </div>                    </div>                    <div class="form-group">                        <label for="contact_email_0" class="require col-lg-2 control-label">Email</label>                        <div class="col-lg-6">                            <input type="text" maxlength="50" placeholder="" id="contact_email_0" name="contact[0][email]" class="form-control email" required="">                        </div>                    </div>                    <div class="form-group">                        <label for="contact_notification_language_0" class="col-lg-2 control-label">Notification Language</label> <div class="col-lg-6"><input type="text" maxlength="50" placeholder="" id="contact_notification_language_0" name="contact[0][notification_language]" class="form-control" readonly="readonly" value="English">                        </div>                    </div>                <legend>Email Notifications</legend>                <div class="form-group ">   <div class="col-lg-12">     <div class="checkbox">                        <label class="">                            <input type="checkbox" value="1" name="contact[0][notification_product_quality]" id="Notification_product_quality_0" class="checkbox "> Product Quality (Products added or amended)                        </label>                    </div><div class="checkbox"><label class=""> <input type="checkbox" value="1" name="contact[0][notification_booking]" id="Notification_booking_0" class="checkbox "> Bookings (Notifies the supplier a booking has been made) </label> </div>                  <div class="checkbox">  <label class="">     <input type="checkbox" value="1" name="contact[0][notification_review]" id="Notification_review_0" class="checkbox "> Reviews (Notifies a supplier a review for a product has been approved)</label></div><div class="checkbox"><label class=""> <input type="checkbox" value="1" name="contact[0][notification_payment]" id="Notification_payment_0" class="checkbox "> Payment Details (This should be the contact person for their accounts person receiving payments for bookings)                        </label>                    </div>                                    <div class="checkbox">                        <label class="">                            <input type="checkbox" value="1" name="contact[0][notification_newsletter]" id="Notification_newsletter_0" class="checkbox " > Newsletter (This is the email we can send newsletters to)</label></div></div></div></div>';
            //ContactsHtml = ContactsHtml.replace('Contacts 0 :','Contacts '+contacts_count+' :');
            ContactsHtml = ContactsHtml.replace(/_0/g, '_' + contacts_count);
            ContactsHtml = ContactsHtml.replace(/\[0\]/g, '[' + contacts_count + ']');
            $('#contact_details_div').append(ContactsHtml);
        }
    });
    $('#ProductForm #add_add_ons').click(function () {
        if (add_ons_array.length < 5) {
            add_ons_count = add_ons_count + 1;
            add_ons_array.push(add_ons_count);
            var AddOnsHtml = '<div id="add_ons_div_' + add_ons_count + '" class="form-group col-lg-12"><div class="form-group"><label class="col-lg-4 control-label">Enter name of Add On</label><div class="col-lg-6"><input type="text" maxlength="50" class="form-control" name="add_ons[' + add_ons_count + '][addon_name]" id="add_ons_name_'+add_ons_count+'" required></div></div><div class="form-group"><label class="col-lg-4 control-label">Price Per Person</label><div class="col-lg-6"><input type="number" min="0" maxlength="5" class="form-control add_ons_price" name="add_ons[' + add_ons_count + '][price]" id="add_ons_price_'+add_ons_count+'" required></div><div class="col-lg-2"><a href="javascript:void(0);" onclick="removeAddons(' + add_ons_count + ')" class="btn btn-danger">Remove</a></div></div></div>';
            $(this).parent().parent().append(AddOnsHtml);
        }
    });

    $('#ProductForm #add_new_date_range_btn_group').click(function () {
        if (group_price_div_array.length < 2) {
            group_price_div_count = group_price_div_count + 1;
            group_price_div_array.push(group_price_div_count);
            var PriceDivsHtml = '<div id="group_price_div_' + group_price_div_count + '" class="NewAddedPriceDiv">' + $('#group_price_div_0').html() + '</div>';
            PriceDivsHtml = PriceDivsHtml.replace(/_0/g, '_' + group_price_div_count);
            PriceDivsHtml = PriceDivsHtml.replace(/\[0\]/g, '[' + group_price_div_count + ']');
            $('#group_price_with_date_range_div').append(PriceDivsHtml);
            $('#group_price_with_date_range_div #date_range_from_' + group_price_div_count).val("");
            $('#group_price_with_date_range_div #group_date_range_to_' + group_price_div_count).val("");
            $('#group_price_with_date_range_div #price_per_group_' + group_price_div_count).val("");
            $('#group_price_div_' + group_price_div_count + ' .date_range_div_group').append("<div class='col-md-2'><input type='button' class='btn btn-danger' value='Delete' onclick='removeDateRangeGroup(" + group_price_div_count + ")'></div>")
            //updateFromToDatepicker();
            var dpd1 = $('#group_price_div_' + group_price_div_count).find('.dpd1');
            var dpd2 = $('#group_price_div_' + group_price_div_count).find('.dpd2');
            updateFromToDatepickerbyid(dpd1,dpd2);
        }
    });
    $('#ProductForm #add_new_date_range_btn').click(function () {
        if (price_div_array.length < 5) {  // for development purpose we have added length which can be removed aftrwards if needed
            price_div_count = price_div_count + 1;
            price_div_array.push(price_div_count);
            var PriceDivsHtml = '<div id="price_div_' + price_div_count + '" class="NewAddedPriceDiv">' + $('#price_div_0').html() + '</div>';
            PriceDivsHtml = PriceDivsHtml.replace(/_0/g, '_' + price_div_count);
            PriceDivsHtml = PriceDivsHtml.replace(/\[0\]/g, '[' + price_div_count + ']');
            
            $('#price_with_date_range_div').append(PriceDivsHtml);
            
            //console.log($('#price_div_'+price_div_count).html())
            $('#price_div_'+price_div_count).find('input[type="text"]').val('').not($('.datepicker')).attr('disabled','disabled');
            $('#price_div_'+price_div_count).find('input[type="checkbox"]').attr('checked',false);
            
            $('#price_div_' + price_div_count + ' .date_range_div').append("<div class='col-md-2'><input type='button' class='btn btn-danger' value='Delete' onclick='removeDateRange(" + price_div_count + ")'></div>")
            
            //updateFromToDatepicker();
            var dpd1 = $('#price_div_' + price_div_count).find('.dpd1');
            var dpd2 = $('#price_div_' + price_div_count).find('.dpd2');
            updateFromToDatepickerbyid(dpd1,dpd2);
            
            $('.price_per_person_table input[type="checkbox"]').change(function () {
                if ($(this).is(":checked")) {
                    $(this).parent().parent().find('input[type="text"]').removeAttr('disabled');
                    $(this).parent().parent().find('input[type="text"]').each(function () {
                        $(this).rules("add", {
                            required: true
                        });
                    });
                } else {
                  
                    $(this).parent().parent().find('input[type="text"]').attr("disabled", 'disabled');
                    $(this).parent().parent().find('input[type="text"]').removeClass('error');
                    $(this).parent().parent().find('label.error').remove();
                    $(this).parent().parent().find('input[type="text"]').each(function () {
                        $(this).rules("add", {
                            required: false
                        });
                    });
                }
            });
            $(this).parent().find('.priceDivMessage').remove();
            $(this).parent().append("<label class='priceDivMessage success'>New Date Range Added.</label>")
        } else {
            $(this).parent().find('.priceDivMessage').remove();
            $(this).parent().append("<label class='error priceDivMessage'>You reached the maximum limit 5.</label>");
        }
        setTimeout(function(){
            $('.priceDivMessage').fadeOut();
        },5000);
    });

    $('#ProductForm #price_scale_save_btn').click(function () {
        update_price_table('saveButton');
    });
    $('#ProductForm #price_scale_add_btn').click(function () {
        if (scale_array.length < 5) {
            var last_value = parseInt($('#price_scale_to_' + scale_count).val()) + 1;
            last_value = parseInt($('#price_scale_sub_div').find('.form-group:last').find('input.price_scale_to').val()) + 1;
            if (!(last_value)) {
                last_value = "";
            }
            scale_count = scale_count + 1;
            scale_array.push(scale_count);
            $('#price_scale_sub_div').append('<div class="form-group" id="scaled_' + scale_count + '"><div class="col-lg-2"><input type="text" readonly="readonly" placeholder="From" value="' + last_value + '" id="price_scale_from_' + scale_count + '" name="scale_json[from][]" class="form-control price_scale_from" maxlength="20"></div> <div class="col-lg-2"><input type="text" placeholder="To" value="" id="price_scale_to_' + scale_count + '" name="scale_json[to][]" class="form-control price_scale_to" maxlength="20"></div><div class="col-lg-2"><input type="button" value="Delete" id="price_scale_delete_btn_' + scale_count + '" onclick="removeScale(' + scale_count + ')" name="price_scale_delete_btn_' + scale_count + '" class="btn btn-danger"></div></div>');
            $('input.price_scale_to').change(function () {
                if ($(this).val() != "") {
                    $(this).parent().parent().next().find('input.price_scale_from').val(parseInt($(this).val()) + 1);
                } else {
                    $(this).parent().parent().next().find('input.price_scale_from').val("");
                }
            });
        }
    });

    $('#ProductForm #add_new_special_date_btn').click(function () {
        if (special_date_array.length < 5) {
            special_date_count = special_date_count + 1;
            special_date_array.push(special_date_count);
            var SpeciaDataHtml = '<div class="col-lg-12 form-group" id="special_date_div_' + special_date_count + '"><div class="col-lg-6"><input type="text" maxlength="50" class="form-control form-control-inline input-medium default-date-picker" size="16" value="" name="special_date[]" id="special_date_0"></div><input type="button" class="btn btn-danger" value="Delete" name="add_new_special_date_delete_btn" id="add_new_special_date_delete_btn_' + special_date_count + '" onclick="removeSpecialData(' + special_date_count + ')"></div>';
            $(this).parent().parent().append(SpeciaDataHtml);
            $('.default-date-picker').datepicker({
                format: defaultDateFormat,
            }).on('changeDate', function () {
                $('.datepicker').hide();
            });
        }
    });

    $('#ProductForm #add_customer_request_detail_btn').click(function () {
        if (customer_request_array.length < 5) {
            customer_request_count = customer_request_count + 1;
            customer_request_array.push(customer_request_count);
            var SpeciaDataHtml = '<div id="customer_request_div_' + customer_request_count + '" class="col-lg-12 form-group"><div class="col-lg-8"><textarea id="customer_detail_request_' + customer_request_count + '" name="customer_detail_request[]" maxlength="500" class="form-control" rows="5"></textarea><div class="counter" id="customer_detail_request_' + customer_request_count + '_counter"></div> </div><div class="col-lg-3"><input type="button" id="add_customer_request_detail_delete_btn_' + customer_request_count + '" name="add_customer_request_detail_btn" value="Delete" class="btn btn-danger" onclick="RemoveCustomerRequest(' + customer_request_count + ')"></div></div>';
            $(this).parent().parent().append(SpeciaDataHtml);
            $('#customer_detail_request_' + customer_request_count).MaxLength({
                MaxLength: 500,
                CharacterCountControl: $('#customer_detail_request_' + customer_request_count + '_counter')
            });
        }
    });

    $('#ProductForm textarea').each(function () {
        var char_limit = $(this).attr('maxlength');
        if (char_limit) {
            $(this).MaxLength({
                MaxLength: char_limit,
                CharacterCountControl: $('#' + $(this).attr('id') + '_counter')
            });
        }
    });
    
    $('#FaqForm textarea').each(function () {
        var char_limit = $(this).attr('maxlength');
        if (char_limit) {
            $(this).MaxLength({
                MaxLength: char_limit,
                CharacterCountControl: $('#' + $(this).attr('id') + '_counter')
            });
        }
    });

    $('.working-plan input.days_checkbox[type="checkbox"]').click(function () {
        var flag = true;
        $('.working-plan input.days_checkbox[type="checkbox"]').each(function () {
            if (!($(this).prop('checked'))) {
                flag = false;
            }
        });
        if ($('.working-plan input.days_checkbox[type="checkbox"]:checked').length > 0) {
            $('.working-plan input#all[type="checkbox"]').prop('checked', flag);
        } else {
            $('.working-plan input#all[type="checkbox"]').prop('checked', false);
        }

    });
    $('.working-plan input[type="checkbox"]').click(function () {
        var id = $(this).attr('id');
        if ($(this).prop('checked') == true) {
            $('#' + id + '-start').prop('disabled', false).val('09:00');
            $('#' + id + '-end').prop('disabled', false).val('18:00');
        } else {
            $('#' + id + '-start').prop('disabled', true).val('');
            $('#' + id + '-end').prop('disabled', true).val('');
        }
    });

    //$('.working-plan input[type="text"]').timepicker();
    $('.working-plan input[type="text"]').prop('disabled', true);


    $('input[type="text"],input[type="email"],input[type="password"],input[type="url"]').each(function () {
        if ($(this).attr('maxlength') == undefined) {
            $(this).attr('maxlength', "80");
        }
    });
    $('textarea').each(function () {
        if ($(this).attr('rows') == undefined) {
            $(this).attr('rows','8')
        }
        /*if ($(this).attr('maxlength') == undefined) {
            $(this).attr('maxlength', "500");
        }*/
    });

    setTimeout(function () {
        $('.alert .close').trigger('click');
    }, 5000);

}();

function removeDateRangeGroup(id) {
    $('#group_price_div_' + id).remove();
    var index = group_price_div_array.indexOf(id);
    if (index >= 0) {
        group_price_div_array.splice(index, 1);
    }
}
function removeDateRange(id) {
    $('#price_div_' + id).remove();
    var index = price_div_array.indexOf(id);
    if (index >= 0) {
        price_div_array.splice(index, 1);
    }
}
function removeAddons(id) {
    $('#add_ons_div_' + id).remove();
    var index = add_ons_array.indexOf(id);
    if (index >= 0) {
        add_ons_array.splice(index, 1);
    }
}
function removeContact(id) {
    $('#contact_details_' + id).remove();
    var index = contacts_array.indexOf(id);
    if (index >= 0) {
        contacts_array.splice(index, 1);
    }
}
function removeSpecialData(id) {
    $('#special_date_div_' + id).remove();
    var index = special_date_array.indexOf(id);
    if (index >= 0) {
        special_date_array.splice(index, 1);
    }
}
function RemoveCustomerRequest(id) {
    $('#customer_request_div_' + id).remove();
    var index = customer_request_array.indexOf(id);
    if (index >= 0) {
        customer_request_array.splice(index, 1);
    }
}
function removeScale(id) {
    for(var i = 0 ; i < jsPrices.length ; i++ ) {
        $.each(jsPrices[i].price,function(k,v) {
            if(jsPrices[i].price[k].price !== undefined)
            jsPrices[i].price[k].price.splice(id,1);
        });
    }
    var prev_ele = $('#scaled_' + id).prev();
    var next_ele = $('#scaled_' + id).next();
    //alert( '#scaled_' + id + " " + $('#scaled_' + id).prev().attr('id'));
    if ($(prev_ele).find('input.price_scale_to').val()) {
        //alert($(prev_ele).find('input.price_scale_to').val());
        $(next_ele).find('input.price_scale_from').val(parseInt($(prev_ele).find('input.price_scale_to').val()) + 1);
    } else {
        //alert("Not found");
    }
    $('#price_scale_delete_btn_' + id).parent().parent().remove();
    var index = scale_array.indexOf(id);
    if (index >= 0) {
        scale_array.splice(index, 1);
    }
}
function showloading() {
    $('body').append('<div id="loading"><div class="popup"><img src="' + base_url + 'assets/admin/img/spinner1.gif"></div></div>');
}
function hideloading() {
    $('body').find('#loading').remove();
}

function CustomDailog(title, desc, link) {
    $('a#myModalDailog').click();
    $('#myModal h4.modal-title').html(title);
    $('#myModal div.modal-body').html(desc);
    $('#myModal .modal-footer a').attr('href', link);
}
function CustomConfirm(title, desc, link) {
    $('a#myModalConfirm').click();
    $('#myModal2 h4.modal-title').html(title);
    $('#myModal2 div.modal-body').html(desc);
    $('#myModal2 .modal-footer a').attr('href', link);
}
function CustomAlert(title, desc, link) {
    $('a#myModalAlert').click();
    $('#myModal3 h4.modal-title').html(title);
    $('#myModal3 div.modal-body').html(desc);
    $('#myModal3 .modal-footer a').attr('href', link);
}
function update_price_table(fromCalled) {
    var price_scale = $('input[name="price_scale"]:checked').val();
    $('.alert').hide();
    if (price_scale == 'yes') {
        var flag = true;
        
        $('#price_scale_sub_div input[type=text]').each(function () {
            if ($(this).val() == '') {
                $(this).addClass('error');
                flag = false;
            }
        });
        
        var pre_value = 1;
        if($('#price_scale_sub_div .error').length > 0 ) {
            flag = false;
        }
        $('#price_scale_sub_div .form-group input[type="text"]').each(function () {
            //alert($(this).val() + " < " + pre_value);
            if (parseInt($(this).val()) < parseInt(pre_value)) {
                flag = false;
                $(this).addClass('error');
            }
            pre_value = $(this).val();
        });
        $('.price_scale_from').removeClass('error');
        if (flag == true) {
            var total_scale = $('#price_scale_sub_div .form-group').length;
            if (total_scale >= 1) {

                $('.price_per_person_table').each(function () {
                    $(this).find('thead tr:first th:last').removeAttr('rowspan');
                    $(this).find('thead tr:first th:last').attr('colspan', total_scale);
                    var j = 0;
                    $(this).find('thead tr:last th').each(function () {
                        if (j >= 2) {
                            $(this).remove();
                        }
                        j++;
                    });
                });
                $('#price_scale_sub_div .form-group').each(function () {
                    var fromInt = $(this).find('input.price_scale_from').val();
                    var toInt = $(this).find('input.price_scale_to').val();
                    $('.price_per_person_table').each(function () {
                        $(this).find('thead tr:last').append('<th class="sorting_disabled" role="columnheader" >' + fromInt + ' to ' + toInt + '</th>');
                    });
                });

                $('.price_per_person_table tbody tr').each(function () {
                    var td = 0;
                    $(this).find('td').each(function () {
                        if (td >= 4) {
                            $(this).remove();
                        }
                        td++
                    });
                });
                var typeCount = 0;
                var counter = 0;
                
                $('.price_per_person_table tbody tr').each(function () {
                    var disabled_text = "disabled";
                    if ($(this).find('input[type="checkbox"]').is(":checked")) {
                        disabled_text = " required ";
                    }
                    var length_td = $(this).find('td').length;
                    var ageType = $(this).find('td:nth-child(2)').html().toLowerCase();
                    var k = 0;
                    for (var i = length_td; i < parseInt(total_scale) + parseInt(4); i++) {
                        if(jsPrices[typeCount] === undefined) {
                            $(this).append('<td class=""><input type="text" id="' + ageType + '_price_group' + i + '" name="price[' + typeCount + '][' + ageType + '][price][]" class="form-control small" value="" maxlength="5" ' + disabled_text + ' ></td>');
                        } else {
                            if(jsPrices[typeCount].price[ageType] === undefined) {
                                $(this).append('<td class=""><input type="text" id="' + ageType + '_price_group' + i + '" name="price[' + typeCount + '][' + ageType + '][price][]" class="form-control small" value="" maxlength="5" ' + disabled_text + ' ></td>');
                            } else {
                                if(jsPrices[typeCount].price[ageType].price[k] === undefined) {
                                    $(this).append('<td class=""><input type="text" id="' + ageType + '_price_group' + i + '" name="price[' + typeCount + '][' + ageType + '][price][]" class="form-control small" value="" maxlength="5" ' + disabled_text + ' ></td>');
                                } else {
                                    $(this).append('<td class=""><input type="text" id="' + ageType + '_price_group' + i + '" name="price[' + typeCount + '][' + ageType + '][price][]" class="form-control small" value="'+jsPrices[typeCount].price[ageType].price[k]+'" maxlength="5" ' + disabled_text + ' ></td>');
                                }
                            }
                        }
                        
                        
                        k++;
                    }
                    if (counter == 6) {
                        typeCount++;
                        counter = 0;
                    }
                    counter++
                });
            }

            $('.price_per_person_table input[type="checkbox"]').change(function () {
                if ($(this).is(":checked")) {
                    $(this).parent().parent().find('input[type="text"]').removeAttr('disabled');
                } else {
                    $(this).parent().parent().find('input[type="text"]').attr("disabled", 'disabled');
                }
            });
            if(fromCalled != 'scalechanged'){
                $('#price_scale_save_btn').parent().next().append('<div class="alert alert-success fade in" style="margin:0;padding:7px 15px;"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Price Table Updated Successfully.</strong></div>');
            }
        } else {
            if(fromCalled != 'scalechanged'){
                $('#price_scale_save_btn').parent().next().append('<div class="alert alert-block alert-danger fade in" style="margin:0;padding:7px 15px;"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Error In Scale Range.</strong></div>');
            }
        }
        setTimeout(function () {
            $('.alert .close').trigger('click');
        }, 3000);

    } else {
        $('.price_per_person_table').each(function () {
            $(this).find('thead tr:first th:last').removeAttr('colspan');
            $(this).find('thead tr:first th:last').attr('rowspan', 2);
            var j = 0;
            $(this).find('thead tr:last th').each(function () {
                if (j >= 2) {
                    $(this).remove();
                }
                j++;
            });
        });
        $('.price_per_person_table tbody tr').each(function () {
            var td = 0;
            $(this).find('td').each(function () {
                if (td >= 4) {
                    $(this).remove();
                }
                td++
            });
        });
        $('.price_per_person_table tbody tr').each(function () {
            var disabled_text = "disabled";
            if ($(this).find('input[type="checkbox"]').is(":checked")) {
                disabled_text = " required ";
            }
            var ageType = $(this).find('td:nth-child(2)').html().toLowerCase();
            $(this).append('<td class=""><input type="text" id="' + ageType + '_price_group0" name="price[0][' + ageType + '][price][]" class="form-control small" value="" maxlength="10" ' + disabled_text + ' ></td>');
        });
    }
}
var nowDate = new Date();
var now = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
function updateFromToDatepicker() {
    var dpOptions = {
        format: 'dd-mm-yyyy',
        startDate: now,
        setDate: now,
    };
    var datePicker1 = $(".dpd1").
        datepicker(dpOptions).
        on('changeDate', function (e) {
            datePicker2.datepicker('setStartDate', e.date);
            datePicker2.datepicker('update');
            $(".dpd1").datepicker('hide');
        });

    var datePicker2 = $(".dpd2").
        datepicker(dpOptions).
        on('changeDate', function (e) {
            datePicker1.datepicker('setEndDate', e.date);
            datePicker1.datepicker('update');
            $(".dpd2").datepicker('hide');
        });
}

function updateFromToDatepickerbyid(dpd1,dpd2) {
    var dpOptions = {
        format: 'dd-mm-yyyy',
        startDate: now,
        setDate: now,
    };
    //console.log(price_div_count);
    var datePicker1 = dpd1.
        datepicker(dpOptions).
        on('changeDate', function (e) {
            datePicker2.datepicker('setStartDate', e.date);
            datePicker2.datepicker('update');
            $(".dpd1").datepicker('hide');
        });

    var datePicker2 = dpd2.
        datepicker(dpOptions).
        on('changeDate', function (e) {
            datePicker1.datepicker('setEndDate', e.date);
            datePicker1.datepicker('update');
            $(".dpd2").datepicker('hide');
        });
}

$(function () {
    var dpOptions = {
        format: 'dd-mm-yyyy',
        startDate: now,
        forceParse:false,
        //endDate :now,
        autoclose: true,
        setDate: now,
    };
    var dpOptions_dpt1 = {
        format: 'dd-mm-yyyy',
        //startDate: now,
        forceParse:false,
        //endDate :now,
        autoclose: true,
        //setDate: now,
    };
    var dpOptions_dpt2 = {
        //format: 'mm-yyyy',
        //startDate: now,
        forceParse:false,
        //endDate :now,
        autoclose: true,
        format: "M-yyyy",
        viewMode: "months", 
        minViewMode: "months"
        //dateFormat: 'MM yy',
        //setDate: now,
    };
    var datePicker1 = $(".dpd1").datepicker(dpOptions).on('changeDate', function (e) {
                datePicker2.datepicker('setStartDate', e.date);
                datePicker2.datepicker('update');
                $(".dpd1").datepicker('hide');
            });

    var datePicker2 = $(".dpd2").datepicker(dpOptions).on('changeDate', function (e) {
                datePicker1.datepicker('setEndDate', e.date);
                datePicker1.datepicker('update');
                $(".dpd2").datepicker('hide');
            });
    var datePicker3 = $(".from_date ").datepicker(dpOptions).on('changeDate', function (e) {
                datePicker4.datepicker('setStartDate', e.date);
                datePicker4.datepicker('update');
                $(".from_date ").datepicker('hide');
            });

    var datePicker4 = $(".to_date").datepicker(dpOptions).on('changeDate', function (e) {
                datePicker3.datepicker('setEndDate', e.date);
                datePicker3.datepicker('update');
                $(".to_date").datepicker('hide');
            }); 
            
    var datePicker5 = $(".dtp1").datepicker(dpOptions_dpt1).on('changeDate', function (e) {
                datePicker6.datepicker('setStartDate', e.date);
                datePicker6.datepicker('update');
                $(".dtp1 ").datepicker('hide');
            });

    var datePicker6 = $(".dtp2").datepicker(dpOptions_dpt1).on('changeDate', function (e) {
                datePicker5.datepicker('setEndDate', e.date);
                datePicker5.datepicker('update');
                $(".dtp2").datepicker('hide');
            });          
    var datePicker7 = $(".drp1").datepicker(dpOptions_dpt2).on('changeDate', function (e) {
                console.log(e.date);
                datePicker8.datepicker('setStartDate', e.date);
                var myDate=new Date(e.date);
                myDate.setDate(myDate.getDate()+365);
                datePicker8.datepicker('setEndDate', myDate);
                datePicker8.datepicker('update');
                $(".dtp1 ").datepicker('hide');
            });

    var datePicker8 = $(".drp2").datepicker(dpOptions_dpt2).on('changeDate', function (e) {
                console.log(e.date);
                datePicker7.datepicker('setEndDate', e.date);
                var myDate=new Date(e.date);
                myDate.setDate(myDate.getDate()-365);
                datePicker7.datepicker('setStartDate', myDate);
                $(".dtp2").datepicker('hide');
            });          
});
$(document).on('change', '.imageClass', function(){ 
//$('.imageClass').on('change',function(){
             if($(this).val()!='') {
                 $(this).parents('.fileupload').find('.removeProduct').show();  
             }
             if($(this).data('id')!='undefined')
             {
                 $('.rp_'+$(this).data('id')).show();
             }
        });
        $(document).on('click', '.removeProduct', function(){ 
            var $this = $(this) ;
            var id = $(this).data('id') ;
            var image_name = $(this).data('name');
            var url =  base_url+'countrybanner/delete_product_image';
            var radioChk = $(this).data('radio');
            
            $.ajax({
            method: "POST",
            url: url,
            data: { id:id,banner:image_name}
            })
            .done(function( msg ) {
                
                if(id == undefined || id==''){
                  
                    $this.parents('.fileupload-exists').find('img').attr('src',base_url+"assets/uploads/img/noimage.png");
                    $this.parents('.fileupload-exists').find('.image_name').val('');
                }
                else
                {
                    
                    $this.parents('.fileupload-exists').find('img').attr('src',base_url+"assets/uploads/img/noimage.png");
                    $('.'+id).attr('src',base_url+"assets/uploads/img/noimage.png") ;
                }
                $('.r_'+radioChk).prop('checked', false);
                $("[name^='images["+radioChk+"][image_name]']").val('');
                $("input[name=images_"+radioChk+"]").val('');
                $this.hide();
                //$('#'+id).hide();
            }).error(function(){
                
                //alert('Please Try Again!')
            });
 });
function showPopupup(url, divId, title, data)
{
    $('#'+divId).html('');
    if(!data){
        data = '';
    }
    var modal_id= $('#'+divId).parents('.modal').attr('id');
    $('#'+modal_id).modal({backdrop:'static',keyboard:false});
    $('.modal-title').html(title);
    $('#'+divId).load(url,{data:data.toString()}, function(){
    });
//        $("#"+modal_id+" .modal-content").draggable({
//          handle: ".modal-header"
//        });
}