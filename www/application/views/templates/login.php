<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $meta_description;?>">
    <meta name="keyword" content="<?php echo $meta_keywords;?>">
    <link rel="shortcut icon" href="">
    <title><?php echo $title;?></title>

    <!-- Bootstrap core CSS -->
    <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/style.css" rel="stylesheet">
    <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript">
        var defaultDateFormat =  '<?php echo DEFINE_DATE_FORMATE_JS; ?>' ;
        var changeDateFormat = '<?php echo DEFINE_DATE_Ymd_FORMATE_JS; ?>' ;
    </script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">
    
        <?php print $content; ?>
        
    </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.js"></script>
    <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.validate.min.js"></script>
    
    <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/form-validation-script.js"></script>
    <script src="http://127.0.0.1:<?php echo PORT;?>/assets/frontend/js/config.js"></script>
   <div id="LoaderDiv" style="z-index: 9999999; width: 32px; height: 32px; position: fixed; top: 317.5px; left: 658.5px; display: none;"><img src="<?php echo 'http://127.0.0.1:'. PORT . COMMON_IMAGE_PATH . 'ajax-loader.gif' ?>" width="32" height="32" style="border:0;" alt="Loading Icon"></div>

  </body>
</html>