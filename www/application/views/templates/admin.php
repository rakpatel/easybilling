<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/img/favicon.png">
        <title><?php echo $title; ?></title>
        <!-- Bootstrap core CSS -->
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/bootstrap-reset.css" rel="stylesheet">
        <!--external css-->
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
        <link rel="stylesheet" href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/owl.carousel.css" type="text/css">
        <!-- Custom styles for this template -->
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/style.css" rel="stylesheet">
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/custom.css" rel="stylesheet">
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/admin/css/style-responsive.css" rel="stylesheet" />
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
        <link href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-datetimepicker/css/datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-daterangepicker/daterangepicker-bs3.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/jquery-multi-select/css/multi-select.css" />
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery-2.1.4.js"></script>
        <script type="text/javascript" language="javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/advanced-datatable/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript">
            var base_url = 'http://127.0.0.1:<?php echo PORT;?>/';
            var defaultDateFormat =  '<?php echo DEFINE_DATE_FORMATE_JS; ?>' ;
            var changeDateFormat = '<?php echo DEFINE_DATE_Ymd_FORMATE_JS; ?>' ;
        </script>
    </head>

    <body>
        <section id="container" >
            <!--header start-->
            <header class="header white-bg">
                <div class="top-nav ">
                    <!--search & user info start-->
                    <ul class="nav pull-right top-menu">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                                <span class="username"><span class="from">Transactions</span></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <?php if(in_array('manage_stock', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('products/stock');?>">Manage Stock</a></li>
                                <?php }?>
                                <?php if(in_array('sales_invoice', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('sales/create');?>">Sales Invoice</a></li>
                                <?php }?>
                                <?php if(in_array('manage_sales', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('sales/index');?>">Manage Sales</a></li>
                                <?php }?>
                                <?php if(in_array('purchase_invoice', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('purchase/create');?>">Purchase Invoice</a></li>
                                <?php }?>
                                <?php if(in_array('manage_purchase', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('purchase/index');?>">Manage Purchases</a></li>
                                <?php }?>
                                <?php if(in_array('purchase_return', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('purchase/return');?>">Purchases Return</a></li>
                                <?php }?>
                                <?php if(in_array('sales_return', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('sales/return');?>">Sales Retrun</a></li>
                                <?php }?>
                                <?php if(in_array('adjustment', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('product/adjustment');?>">Adjustment</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                                <span class="username"><span class="from">Masters</span></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <?php if(in_array('product_master', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('products/index');?>">Product Master</a></li>
                                <?php }?>
                                <?php if(in_array('party_master', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('company/index');?>">Party Master</a></li>
                                <?php }?>
                                <?php if(in_array('batch_master', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('batch/index');?>">Batch Master</a></li>
                                <?php }?>
                                <?php if(in_array('unit_master', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('unit/index');?>">Unit Master</a></li>
                                <?php }?>
                                <?php if(in_array('type_master', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('type/index');?>">Type Master</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        <?php if(in_array('reports', $arrModules)){?>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                                <span class="username"><span class="from">Reports</span></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <?php if(in_array('reports', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('reports/index');?>">Manage Report</a></li>
                                <?php }?>
                            </ul>
                        </li>
                        <?php }?>
                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                                <span class="username"><span class="from">User</span></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <?php if(in_array('profile', $arrModules)){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('user/profile');?>">Profile</a></li>
                                <li style="width:100%;"><a href="<?php echo __gurl('user/index_doctor');?>">Doctors</a></li>
                                <li style="width:100%;"><a href="<?php echo __gurl('user/index_customer');?>">Customers</a></li>
                                <li style="width:100%;"><a href="<?php echo __gurl('user/index_admin');?>">Admins</a></li>
                                <?php }?>
                                <?php if(in_array('setting', $arrModules) && $user_data['role_id'] == 1){?>
                                <li style="width:100%;"><a href="<?php echo __gurl('setting/index');?>">Settings</a></li>
                                <li style="width:100%;"><a href="<?php echo __gurl('setting/updateDatabase');?>">Database</a></li>
                                <?php }?>
                                <li><a href="<?php echo __gurl('user/logout');?>"><?php echo __t("Log Out"); ?></a></li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                </div>
            </header>
            <!--header end-->


            <?php //print $sidebar; ?>
            <!--main content start-->
            <section id="main-content" style="margin-left: 0px;">
                <section class="wrapper site-min-height">
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('error')) { ?>
                        <div class="alert alert-block alert-danger fade in">
                            <button type="button" class="close close-sm" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <?php echo $this->session->flashdata('error'); ?>
                        </div>
                    <?php } ?>
                    <?php print $content; ?>
                </section>
            </section>
            <!--main content end-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    <?php echo date('Y'); ?> &copy; Easy Billing System
                    <a href="javascript:void(0);" class="go-top">
                        <i class="fa fa-angle-up"></i>
                    </a>
                </div>
                <script type="text/javascript">
                    $(document).ready(
                        function() { 
                            $("html").niceScroll();
                        }
                    );
                </script>
            </footer>
            <!--footer end-->

        </section>
        <section class="panel123">
            <div class="panel-body123">
                <a class="btn btn-success" data-toggle="modal" id="myModalDailog" href="#myModal" style="display: none;">
                    Dialog
                </a>
                <a class="btn btn-warning" data-toggle="modal" id="myModalConfirm" href="#myModal2" style="display: none;">
                    Confirm
                </a>
                <a class="btn btn-danger" data-toggle="modal" id="myModalAlert" href="#myModal3" style="display: none;">
                    Alert !
                </a>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Modal Tittle</h4>
                            </div>
                            <div class="modal-body">

                                Body goes here...

                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-success" type="button">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->
                <!-- Modal -->
                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Modal Tittle</h4>
                            </div>
                            <div class="modal-body">

                                Body goes here...

                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <a class="btn btn-warning" href="javascript:void(0);"> Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->
                <!-- Modal -->
                <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Modal Tittle</h4>
                            </div>
                            <div class="modal-body">

                                Body goes here...

                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-danger" type="button"> Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- modal -->

            </div>
            <div class="modal fade" id="popupcontainerdiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id='alert_title'>Modal Title</h4>
                        </div>
                        <div class="modal-body">
                            <div id='popup_content'></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <a data-toggle="modal" href="#alertmsgdiv" id='showalert'></a>
            <div class="modal fade" id="alertmsgdiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id='alert_heading'>Modal Title</h4>
                        </div>
                        <div class="modal-body">
                            <div id='alert_msg'></div>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" style="float:right;" type="button">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.scrollTo.min.js"></script>
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.customSelect.min.js" ></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/additional-methods.js"></script>
        <!--common script for all pages-->
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/MaxLength.min.js"></script>
        <!-- bootstrap datepicker -->
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-daterangepicker/moment.min.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <!--  bootstrap datepicker --> 
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/common-scripts.js"></script>
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/count.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-fileupload/bootstrap-fileupload.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/jquery-multi-select/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/fuelux/js/spinner.min.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/advanced-form-components.js"></script>
        <script type="text/javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/jquery.battatech.excelexport.js"></script>

        <script>
            $(function () {
                $('select.styled').customSelect();
            });
            function redirectInternal(url)
            {
                window.location.href = url;
            }
            function showAlert(message,title)
            {
                if(title == '')
                {
                    title = 'Message';
                }

                $('#alert_heading').html(title);
                $('#alert_msg').html(message);
                $('#showalert').click();

            }
        </script>
        <script src="http://127.0.0.1:<?php echo PORT;?>/assets/admin/js/form-validation-script.js"></script>
    </body>
</html>
