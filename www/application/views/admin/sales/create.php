<link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/advanced-datatable/media/css/jquery.dataTables.css">
<link rel="stylesheet" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/jquery-ui/jquery-ui-1.11.4.custom.min.css">
<script type="text/javascript" language="javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/advanced-datatable/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/jquery-ui/jquery-ui-1.11.4.custom.min.js"></script>

<style>
    .fw-container{min-height: 490px; }
    .form-group{margin-bottom:5px;}
    .panel-body{padding-bottom: 0px;}
    .input-width {width:70px !important;}
    .input-half-width {width:60px !important;}
</style>
<?php
    $today_date = date('d-m-Y');
?>
<!-- page start-->
<section class="panel">
    <div class="panel-body">
        <!--Basic-->
        <fieldset title="Basic" class="step" id="default-step-0" >
            <div class="form-group">
                <div class="col-lg-3">
                    <input type="text"  maxlength="200" class="form-control" name="bill_no" id="bill_no" value="<?php echo isset($invoice_id)?$invoice_id:'';?>" placeholder="Sale No">
                </div>
                <div class="col-lg-3">
                    <input type="text"  maxlength="200" class="form-control" name="customer_name" id="customer_name" placeholder="Customer Name">
                    <input type="hidden"  maxlength="200" class="form-control" name="c_id" id="c_id">
                </div>
                <div class="col-lg-3">
                    <input type="text"  maxlength="200" class="form-control" name="doctor_name" id="doctor_name" placeholder="Doctor Name">
                    <input type="hidden"  maxlength="200" class="form-control" name="d_id" id="d_id">
                </div>
                <div class="col-lg-3">
                    <input type="text"  maxlength="200" class="form-control default-date-picker dpd1" placeholder="Sale Date" name="p_date" id="p_date" value="<?php echo $today_date;?>">
                </div>
            </div>
        </fieldset>
        <div style="clear:both;"></div>
        <div class="fw-container">
            <div class="fw-body">
                <div class="content" style="min-height:150px;">
                            <table id="SaleTable" class="display" cellspacing="0" width="100%">
                                    <thead>
                                            <tr>
                                                <th style="width:300px !important;">Particular</th>
                                                <th class="input-width">Pack</th>
                                                <th class="input-width">Mfg.</th>
                                                <th class="input-width">QTY.</th>
                                                <th class="input-width">Batch</th>
                                                <th class="input-width">Exp.</th>
                                                <th class="input-width">MRP</th>
                                                <th class="input-half-width">Dis%</th>
                                                <th class="input-half-width">Dis Amt</th>
                                                <th class="input-width">Amount</th>
                                            </tr>
                                    </thead>
                                    <tbody class="scrollContent">
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="id" id="id">
                                                    <input style="width:300px !important;" type="text" name="p_name" id="p_name" placeholder="Product">
                                                </td>
                                                <td><input class="input-width" type="text" name="pack" id="pack" placeholder="Pack"></td>
                                                <td><input class="input-width" type="text" name="mfg" id="mfg" placeholder="MFG"></td>
                                                <td><input class="input-width" type="text" name="qty" id="qty" placeholder="QTY"></td>
                                                <td>
                                                    <input class="input-width" type="text" name="batch" id="batch" placeholder="Batch">
                                                    <input type="hidden" name="batch_info" id="batch_info">
                                                </td>
                                                <td><input class="default-date-picker dpd1 input-width" type="text" name="exp" id="exp" value="<?php echo $today_date?>"></td>
                                                <td><input class="input-width" type="text" name="mrp" id="mrp" placeholder="MRP"></td>
                                                <td><input class="input-half-width" type="text" name="dis" id="dis" placeholder="Dis%" value="0"></td>
                                                <td><input class="input-half-width" type="text" name="damt" id="damt" placeholder="Dis Amt" value="0"></td>
                                                <td><input class="input-width" type="text" name="amount" id="amount" placeholder="Amount"></td>
                                            </tr>
                                    </tbody>
                            </table>
                    </div>
            </div>
            <div class="form-group" style="float:right !important;">
                <div style="margin-top:10px;">
                    <input style="margin-right: 12px;width: 100px;text-align: right;" type="text" name="total" id="total">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <button class="btn btn-default" id="save">Save</button>
                <button class="btn btn-default" id="cancle">Cancel</button>
                <button class="btn btn-default" id="remove">Remove</button>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" class="init">
    var today_date = '<?php echo $today_date;?>';
    var table;
    var win=null;

    function printIt(printhtml)
    {
        win = window.open();
        win.title = 'Sales Invoice';
        self.focus();
        win.document.open();
        win.document.write(printhtml);
        win.document.close();
        win.print();
        win.close();
        window.location.href = '<?php echo __gurl("sales/create");?>';
    }

    $(document).ready(function(){
        table = $('#SaleTable').DataTable({
                "bPaginate": false,
                "bFilter": false,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,3,4,5,6,7,8,9 ] }],
                "aaSorting": [],
                "bDestroy": true,
                "bInfo": false 
        });

        $('#SaleTable').on( 'click', 'tr:gt(1)', function () {
                if ( $(this).hasClass('selected') ) {
                        $(this).removeClass('selected');
                }
                else {
                        table.$('tr.selected').removeClass('selected');
                        $(this).addClass('selected');
                }
        } );

        $('#remove').click( function () {
            var product_cart_remove_url = '<?php echo __gurl("sales/removeProduct");?>';
            var remove_index = $('#SaleTable tbody .selected').find("td:first")[0].lastElementChild.defaultValue;
            var arrProduct = {index : remove_index};
            $.ajax({
                    type: "POST",
                    url: product_cart_remove_url,
                    data: arrProduct,
                    success:function(data){
                        var arrVal = data.split('~~~');
                        $('#SaleTable tr:gt(1)').empty();
                        $('#SaleTable tbody tr:last').after(arrVal[1]);
                        if(arrVal[0] > 0)
                            $( "#total" ).val(Math.round(arrVal[0],2));
                        else
                            $( "#total" ).val('0.00');
                    }
            });
        } );

        $('#cancle').click( function () {
            var product_cart_cancle_url = '<?php echo __gurl("sales/cancleOrder");?>';
            $.ajax({
                    type: "POST",
                    url: product_cart_cancle_url,
                    data: {},
                    success:function(data){
                        $("#SaleTable tr:gt(1)").detach();
                        $( "#total" ).val('');
                    }
            });
        } );

        $('#save').click( function () {
//            if($( "#customer_name" ).val() == '')
//            {
//                showAlert('Please select customer', 'Sales Invoice');
//                $( "#customer_name" ).focus();
//                return false;
//            }
//            else if($( "#doctor_name" ).val() == '') 
//            {
//                showAlert('Please select doctor', 'Sales Invoice');
//                $( "#doctor_name" ).focus();
//                return false;
//            }

            if($( "#total" ).val() == '')
            {
                showAlert('Please add product into cart', 'Sales Invoice');
                $( "#p_name" ).focus();
                return false;
            }
            var order_url = '<?php echo __gurl("sales/placeOrder");?>';
            var arrSale = {invoice_id : $( "#bill_no" ).val(), customer_name:$( "#customer_name" ).val(), u_id:$( "#c_id" ).val(), doctor_name:$( "#doctor_name" ).val(), dr_id:$( "#d_id" ).val(), sales_date:$( "#p_date" ).val()};
            $.ajax({
                    type: "POST",
                    url: order_url,
                    data: arrSale,
                    success:function(data){
                        $("#SaleTable tr:gt(1)").detach();
                        $( "#bill_no" ).val('');
                        $( "#total" ).val('');
                        $( "#company_name" ).val('');
                        printIt(data);
                    }
            });
        } );

        var dataProduct = <?php echo $arrProJson;?>;
        $( "#p_name" ).autocomplete({
          source: dataProduct,
          change: function (event, ui) { 
            var product_url = '<?php echo __gurl("sales/getProduct");?>';
            var arrP = (this.value).split('_');
            var p_id = arrP[1];
            $( "#p_name" ).val(arrP[0]);
            $( "#id" ).val(arrP[1]);

            $.ajax({
                    type: "POST",
                    url: product_url,
                    data: {id:p_id},
                    success:function(data){
                        data = jQuery.parseJSON(data);
                        $( "#id" ).val(data.id);
                        $( "#p_name" ).val(data.p_name);
                        $( "#pack" ).val(data.unit_name);
                        $( "#mfg" ).val(data.short_name);
                    }
            });
          }
        });

        var dataCustomer = <?php echo $arrCusJson;?>;
        $( "#customer_name" ).autocomplete({
          source: dataCustomer,
          change: function (event, ui) { 
            var arrC = (this.value).split('_');
            $( "#customer_name" ).val(arrC[0]);
            $( "#c_id" ).val(arrC[1]);
          }
        });

        var dataDoctor = <?php echo $arrDocJson;?>;
        $( "#doctor_name" ).autocomplete({
          source: dataDoctor,
          change: function (event, ui) { 
            var arrC = (this.value).split('_');
            $( "#doctor_name" ).val(arrC[0]);
            $( "#d_id" ).val(arrC[1]);
          }
        });

        $( "#net" ).on('change',function(){
            var net = parseFloat(this.value);
            $( "#mrp" ).val(net.toFixed(2));
            if($( "#qty" ).val() != '')
            {
                var qty = parseFloat($( "#qty" ).val());
                var amount = parseFloat(net * qty);
                $( "#amount" ).val(amount.toFixed(2));
            }
        });

        $( "#vat" ).on('change',function(){
            var vat = parseFloat(this.value);
            var net = parseFloat($( "#net" ).val());
            var vat_amount = parseFloat((net*vat)/100);
            var mrp = parseFloat(vat_amount + net);
            if($( "#qty" ).val() != '')
            {
                var qty = parseFloat($( "#qty" ).val());
                var amount = parseFloat(mrp * qty);
                $( "#mrp" ).val(mrp.toFixed(2))
                $( "#amount" ).val(amount.toFixed(2));
            }
        });

        $( "#add" ).on('change',function(){
            var net = parseFloat($( "#net" ).val());
            var vat = parseFloat($( "#vat" ).val());
            var vat_amount = parseFloat((net*vat)/100);
            var add = parseFloat(this.value);
            var add_amount = parseFloat((net*add)/100);
            var mrp = parseFloat(vat_amount + add_amount + net);
            if($( "#qty" ).val() != '')
            {
                var qty = parseFloat($( "#qty" ).val());
                var amount = parseFloat(mrp * qty);
                $( "#mrp" ).val(mrp.toFixed(2))
                $( "#amount" ).val(amount.toFixed(2));
            }
        });

        $( "#dis" ).on('change',function(){
            var dis = parseFloat(this.value);
            if($( "#qty" ).val() != '')
            {
                var mrp = parseFloat($( "#mrp" ).val());
                var qty = parseFloat($( "#qty" ).val());
                var tAmt = parseFloat(mrp * qty);
                var dis_amount = parseFloat((tAmt * dis)/100);
                var amount = parseFloat(tAmt-dis_amount);

                $( "#damt" ).val(dis_amount.toFixed(2))
                $( "#amount" ).val(amount.toFixed(2));
            }
        });

        $( "#mrp" ).on('change',function(){
            var mrp = parseFloat(this.value);
            if($( "#qty" ).val() != '')
            {
                var qty = parseFloat($( "#qty" ).val());
                var amount = parseFloat(mrp * qty);
                $( "#amount" ).val(amount.toFixed(2));
            }
        });

        $( "#qty" ).on('change',function(){
            var qty = parseFloat(this.value);
            var product_cart_url = '<?php echo __gurl("sales/getBatch");?>';
            var arrProduct = {id:$( "#id" ).val(), p_name:$( "#p_name" ).val(), qty:qty};
            $.ajax({
                    type: "POST",
                    url: product_cart_url,
                    data: arrProduct,
                    success:function(data){
                        if(data == 'error')
                        {
                            showAlert('No stock available for this product', 'Sales Invoice');
                            return false;
                        }
                        data = jQuery.parseJSON(data);
                        var pro_info = data.arrUseBatch[0].split('__');
                        $( "#qty" ).val(data.avail);
                        $( "#batch" ).val(data.batches);
                        $( "#batch_info" ).val(data.arrUseBatch);
                        $( "#exp" ).val(pro_info[4]);
                        $( "#net" ).val(pro_info[3]);
                        $( "#vat" ).val(pro_info[5]);
                        $( "#add" ).val(pro_info[6]);
                        $( "#mrp" ).val(pro_info[7]);
                        $( "#mrp" ).change();
                    }
            });

            if($( "#mrp" ).val() != '')
            {
                var rate = parseFloat($( "#mrp" ).val());
                var amount = parseFloat(rate * qty);
                $( "#amount" ).val(amount.toFixed(2));
            }
        });

        $( "#amount" ).on('keydown',function(){
            
            if($( "#id" ).val() == '')
            {
                showAlert('Please select product', 'Sales Invoice');
                $( "#p_name" ).focus();
                return false;
            }
            else if($( "#qty" ).val() == '') 
            {
                showAlert('Please enter quantity', 'Sales Invoice');
                $( "#qty" ).focus();
                return false;
            }
            else if($( "#amount" ).val() == '') 
            {
                showAlert('No stock available for this product. Please select another product', 'Sales Invoice');
                $( "#qty" ).focus();
                return false;
            }
            
            var product_cart_url = '<?php echo __gurl("sales/addProduct");?>';
            var arrProduct = {id:$( "#id" ).val(), p_name:$( "#p_name" ).val(), qty:$( "#qty" ).val(), amount:$( "#amount" ).val(), batch:$( "#batch" ).val(), exp:$( "#exp" ).val(), net:$( "#net" ).val(), pack:$( "#pack" ).val(), mfg:$( "#mfg" ).val(), vat:$( "#vat" ).val(), add:$( "#add" ).val(), mrp:$( "#mrp" ).val(), dis:$( "#dis" ).val(), damt:$( "#damt" ).val(), batch_info:$( "#batch_info" ).val()};
            $.ajax({
                    type: "POST",
                    url: product_cart_url,
                    data: arrProduct,
                    success:function(data){
                        data = jQuery.parseJSON(data);
                        $("#SaleTable tr:last").after(data.html);
                        $( "#total" ).val(data.cart_total.toFixed(2));
                    }
            });
            $( "#id" ).val('');
            $( "#p_name" ).val('');
            $( "#pack" ).val('');
            $( "#mfg" ).val('');
            $( "#qty" ).val('')
            $( "#amount" ).val('')
            $( "#batch" ).val('');
            $( "#exp" ).val(today_date);
            $( "#net" ).val('');
            $( "#vat" ).val('');
            $( "#add" ).val('');
            $( "#dis" ).val(0);
            $( "#damt" ).val(0);
            $( "#mrp" ).val('');
            $( "#p_name" ).focus();
        });

    });
</script>