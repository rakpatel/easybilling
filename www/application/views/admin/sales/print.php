<html>
    <body>
        <?php $disamt = 0; ?>
        <table class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <td align="center"><b style="font-size: 30px !important;"><?php echo $business_name;?></b></td>
                </tr>
                <tr>
                    <td align="center"><?php echo $address;?></td>
                </tr>
                <tr>
                    <td align="center"><?php echo 'Tin No: '.$tin_no;?></td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="left">Retail Invoice: <b><?php echo $invoice_id;?></b></td>
                                <td align="center">Customer Name: <?php echo $customer_name!='' ? $customer_name : '__________________';?></td>
                                <td align="right">Inv. Date: <?php echo $sales_date;?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </thead>
        </table>
        <table class="display" cellspacing="0" width="100%" border="1px;">
            <thead>
                    <tr style="font-size:10px !important;">
                        <th style="width:7px !important;padding-left:5px;" align="left">Sr.</th>
                        <?php if(in_array('p_name', $product_print_column)){ ?>
                            <th style="width:200px !important;padding-left:5px;" align="left">Item(s)</th>
                        <?php }?>
                        <?php if(in_array('pack', $product_print_column)){ ?>
                            <th style="width:20px !important;padding-left:5px;" align="left">Pack</th>
                        <?php }?>
                        <?php if(in_array('mfg', $product_print_column)){ ?>
                            <th style="width:10px !important;padding-left:5px;" align="left">Mfg.</th>
                        <?php }?>
                        <?php if(in_array('qty', $product_print_column)){ ?>
                            <th style="width:10px !important;padding-left:5px;" align="left">QTY.</th>
                        <?php }?>
                        <?php if(in_array('batch', $product_print_column)){ ?>
                            <th style="width:20px !important;padding-left:5px;" align="left">Batch</th>
                        <?php }?>
                        <?php if(in_array('exp', $product_print_column)){ ?>
                            <th style="width:40px !important;padding-left:5px;" align="left">Exp.</th>
                        <?php }?>
                        <?php if(in_array('mrp', $product_print_column)){ ?>
                            <th style="width:10px !important;padding-right:5px;" align="right">MRP</th>
                        <?php }?>
                        <?php if(in_array('dis', $product_print_column)){ ?>
                            <th style="width:15px !important;padding-left:5px;" align="left">Dis%</th>
                        <?php }?>
                        <?php if(in_array('damt', $product_print_column)){ ?>
                            <th style="width:10px !important;padding-right:5px;" align="right">Dis Amt</th>
                        <?php }?>
                        <?php if(in_array('amount', $product_print_column)){ ?>
                            <th style="width:30px !important;padding-right:5px;" align="right">Amount</th>
                        <?php }?>
                    </tr>
            </thead>
            <tbody>
                <?php foreach ($arrPrintItem as $key => $row){ ?>
                    <tr style="font-size:10px !important;">
                        <td style="width:7px !important;padding-left:5px;" align="left"><?php echo $key+1;?></td>
                        <?php if(in_array('p_name', $product_print_column)){ ?>
                            <td style="width:200px !important;padding-left:5px;" align="left"><?php echo $row['p_name'];?></td>
                        <?php }?>
                        <?php if(in_array('pack', $product_print_column)){ ?>
                            <td style="width:20px !important;padding-left:5px;" align="left"><?php echo $row['pack'];?></td>
                        <?php }?>
                        <?php if(in_array('mfg', $product_print_column)){ ?>
                            <td style="width:10px !important;padding-left:5px;" align="left"><?php echo $row['mfg'];?></td>
                        <?php }?>
                        <?php if(in_array('qty', $product_print_column)){ ?>
                            <td style="width:10px !important;padding-left:5px;" align="left"><?php echo $row['qty'];?></td>
                        <?php }?>
                        <?php if(in_array('batch', $product_print_column)){ ?>
                            <td style="width:20px !important;padding-left:5px;" align="left"><?php echo $row['batch'];?></td>
                        <?php }?>
                        <?php if(in_array('exp', $product_print_column)){ ?>
                            <td style="width:140px !important;padding-left:5px;" align="left"><?php echo $row['exp'];?></td>
                        <?php }?>
                        <?php if(in_array('mrp', $product_print_column)){ ?>
                            <td style="width:10px !important;padding-right:5px;" align="right"><?php echo number_format($row['mrp'],2);?></td>
                        <?php }?>
                        <?php if(in_array('dis', $product_print_column)){ ?>
                            <td style="width:15px !important;padding-left:5px;" align="left"><?php echo $row['dis'];?></td>
                        <?php }?>
                        <?php if(in_array('damt', $product_print_column)){ ?>
                            <td style="width:10px !important;padding-right:5px;" align="right"><?php echo $row['damt'];?></td>
                        <?php
                            if($row['damt'] > 0) $disamt += $row['damt'];
                        }?>
                        <?php if(in_array('amount', $product_print_column)){ ?>
                            <td style="width:30px !important;padding-right:5px;" align="right"><?php echo $row['amount'];?></td>
                        <?php }?>
                    </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="<?php echo $cnt_product_print_column-2;?>" align="left" style="padding-left:5px;font-size: 10px;"><b><?php echo $footer_message;?></b></td>
                    <td colspan="2" align="right">
                        <table width="100%" cellspacing="0" class="display">
                            <tr><td align="right" width="39%">Total:</td><td align="right"><?php echo number_format($subtotal,2);?></td></tr>
                            <tr><td align="right" width="39%">Less:</td><td align="right"><?php echo number_format($disamt,2);?></td></tr>
                            <tr><td align="right" width="39%"><b>Net:</b></td><td align="right"><b><?php echo number_format($total,2);?></b></td></tr>
                        </table>
                    </td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>