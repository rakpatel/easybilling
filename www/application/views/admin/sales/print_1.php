<html>
    <head>
        <style>
            @media print {
                body, td {font-family: sans-serif; font-size: 10pt;}
                .input-width {width:70px !important;}
                .input-half-width {width:60px !important;}
                table.display {clear: both; margin: 0 auto; width: 60% !important;}
                table.display {
                    border-bottom: 1px solid black;
                    cursor: pointer;
                    font-weight: bold;
                    padding: 3px 18px 3px 10px;
                  }
            }
            
        </style>
    </head>
    <body>
        <table class="display" cellspacing="0" width="50%">
            <thead>
                    <tr>
                        <td align="center">Shivam Medical Store</td>
                    </tr>
                    <tr>
                        <td align="center">Agraval Tower Bhuyangdev cross road memnagar</td>
                    </tr>
                    <tr>
                        <td align="center">Agraval Tower Bhuyangdev cross road memnagar</td>
                    </tr>
            </thead>
        </table>
        <table class="display" cellspacing="0" width="50%">
            <thead>
                    <tr>
                        <th style="width:300px !important;">Parti.</th>
                        <th class="input-width">Pack</th>
                        <th class="input-width">Mfg.</th>
                        <th class="input-width">QTY.</th>
                        <th class="input-width">Batch</th>
                        <th class="input-width">Exp.</th>
                        <th class="input-width">MRP</th>
                        <th class="input-half-width">Dis%</th>
                        <th class="input-half-width">Dis Amt</th>
                        <th class="input-width" align="right">Amount</th>
                    </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width:300px !important;">test</td>
                    <td class="input-width">1*10</td>
                    <td class="input-width">CFYH</td>
                    <td class="input-width">20</td>
                    <td class="input-width">test123</td>
                    <td class="input-width">12-45-2015</td>
                    <td class="input-width">52</td>
                    <td class="input-half-width">12</td>
                    <td class="input-half-width">125</td>
                    <td class="input-half-width" align="right">1245.00</td>
                </tr>
                <tr>
                    <td style="width:300px !important;">test</td>
                    <td class="input-width">1*10</td>
                    <td class="input-width">CFYH</td>
                    <td class="input-width">20</td>
                    <td class="input-width">test123</td>
                    <td class="input-width">12-45-2015</td>
                    <td class="input-width">52</td>
                    <td class="input-half-width">12</td>
                    <td class="input-half-width">125</td>
                    <td class="input-half-width" align="right">1245.00</td>
                </tr>
                <tr>
                    <td style="width:300px !important;">test</td>
                    <td class="input-width">1*10</td>
                    <td class="input-width">CFYH</td>
                    <td class="input-width">20</td>
                    <td class="input-width">test123</td>
                    <td class="input-width">12-45-2015</td>
                    <td class="input-width">52</td>
                    <td class="input-half-width">12</td>
                    <td class="input-half-width">125</td>
                    <td class="input-half-width" align="right">1245.00</td>
                </tr>
                <?php //foreach ($arrPrintItem as $key => $row){ ?>
<!--                    <tr>
                        <td style="width:300px !important;"><?php echo $row['p_name'];?></td>
                        <td class="input-width"><?php echo $row['pack'];?></td>
                        <td class="input-width"><?php echo $row['mfg'];?></td>
                        <td class="input-width"><?php echo $row['qty'];?></td>
                        <td class="input-width"><?php echo $row['batch'];?></td>
                        <td class="input-width"><?php echo $row['exp'];?></td>
                        <td class="input-width"><?php echo $row['mrp'];?></td>
                        <td class="input-half-width"><?php echo $row['dis'];?></td>
                        <td class="input-half-width"><?php echo $row['damt'];?></td>
                        <td class="input-half-width"><?php echo $row['amount'];?></td>
                    </tr>-->
                <?php //}?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">Patient: XYXS FDTER</td>
                    <td colspan="4">Bill No.: SB00001</td>
                    <td colspan="4" align="right">Total: 1000.00</td>
                </tr>
                <tr>
                    <td colspan="2">Doctor: XYXS FDTER</td>
                    <td colspan="4">Date: 12-12-2015</td>
                    <td colspan="4" align="right">Less: 200.00</td>
                </tr>
                <tr>
                    <td colspan="2">Message: XYXS FDTER</td>
                    <td colspan="4">Sign: </td>
                    <td colspan="4" align="right">Net: 800.00</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>


<tr>
<!--                    <td colspan="2">Patient: <?php //echo $customer_name;?></td>
                    <td colspan="4">Bill No.: <?php //echo $invoice_id;?></td>-->
                    <td colspan="<?php echo $cnt_product_print_column;?>" align="right">Total: <?php echo number_format($subtotal,2);?></td>
                </tr>
                <tr>
<!--                    <td colspan="2">Doctor: <?php //echo $doctor_name;?></td>
                    <td colspan="4">Date: <?php //echo $sales_date;?></td>-->
                    <td colspan="<?php echo $cnt_product_print_column;?>" align="right">Less: <?php echo number_format($discount_amount,2);?></td>
                </tr>
                <tr>
                    <td colspan="<?php echo $cnt_product_print_column-2;?>"><b>Message: <?php echo $footer_message;?></b></td>
<!--                    <td colspan="4">Sign: </td>-->
                    <td colspan="2" align="right">Net: <?php echo number_format($total,2);?></td>
                </tr>