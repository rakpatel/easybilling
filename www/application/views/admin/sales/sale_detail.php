<style>
    .fw-container{min-height: 490px; }
    .form-group{margin-bottom:5px;}
    .panel-body{padding-bottom: 0px;}
    .input-width {width:70px !important;}
    .input-half-width {width:60px !important;}
    .modal-dialog { width: 80%; height: auto;}
</style>
<section class="panel">
    <div class="panel-body">
        <div class="fw-container">
            <div class="fw-body">
                <div class="content" style="max-height:150px;">
                        <table id="SalesTable" class="display" cellspacing="0" width="100%">
                            <thead>
                                    <tr>
                                        <th style="width:250px !important;">Particular</th>
                                        <th class="input-width">Batch</th>
                                        <th class="input-width">Exp.</th>
                                        <th class="input-half-width">QTY.</th>
                                        <th class="input-width">MRP</th>
                                        <th class="input-half-width">Dis</th>
                                        <th class="input-half-width">DAmt.</th>
                                        <th class="input-width">Amount</th>
                                    </tr>
                            </thead>
                            <tbody class="scrollContent">
                                <?php foreach ($arrSales as $key => $drow){ ?>
                                    <tr>
                                        <td style="width:250px !important;"><?php echo $drow['p_name'];?></td>
                                        <td class="input-width"><?php echo $drow['b_name'];?></td>
                                        <td class="input-width"><?php echo $drow['exp'];?></td>
                                        <td class="input-half-width"><?php echo $drow['qty'];?></td>
                                        <td class="input-width"><?php echo $drow['mrp'];?></td>
                                        <td class="input-half-width"><?php echo $drow['dis'];?></td>
                                        <td class="input-half-width"><?php echo $drow['damt'];?></td>
                                        <td class="input-width"><?php echo $drow['total_price'];?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" class="init">
$(document).ready(function(){
    table = $('#SalesTable').DataTable({
            "bPaginate": false,
            "bFilter": false,
            //"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,3,4,5,6,7,8,9 ] }],
            "aaSorting": [],
            "bDestroy": true,
            "bInfo": false 
    });
});