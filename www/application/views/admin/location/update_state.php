<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
            if (empty($id)) {
                echo __t('Add Province/State/Region');
                $formId = "StateForm";
            } else {
                echo __t('Edit Province/State/Region');
                $formId = "StateForm";
            }
            ?>
        </span>
    </header>
    <div class="panel-body">
        <form class="cmxform form-horizontal tasi-form" id="<?php echo $formId; ?>" method="post" action="">
            <!--<div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Status'); ?></label>
                <div class="col-lg-6">
                    <select class=" form-control" id="status" name="status">
                        <option value="1" <?php echo (isset($category['status']) && $category['status'] == 1) ? "selected" : ""; ?>><?php echo __t('Active'); ?></option>
                        <option value="0" <?php echo (isset($category['status']) && $category['status'] == 0) ? "selected" : ""; ?>><?php echo __t('Inactive'); ?></option>
                    </select>
                </div>
            </div>-->
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('Province/State/Region Name'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="20" id="state_name" name="state_name" type="text" value="<?php echo (isset($state['state_name'])) ? $state['state_name'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('Country Name'); ?></label>
                <div class="col-lg-6">
                    <select name="country_id" id="country_id" class=" form-control">
                        <option value=""><?php echo __t(" -- Select Country -- ") ?></option>
                        <?php
                        if (isset($countries) && !empty($countries)) {
                            foreach ($countries as $countrie) {
                                $selected = ($state['country_id'] == $countrie['country_id']) ? "selected" : "";
                                echo "<option $selected  value='" . $countrie['country_id'] . "'>" . $countrie['country_name'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Latitude'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="latitude" name="latitude" type="text" value="<?php echo (isset($state['latitude'])) ? $state['latitude'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Longitude'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="longitude" name="longitude" type="text" value="<?php echo (isset($state['longitude'])) ? $state['longitude'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="col-lg-2 control-label require"><?php echo __t("Status"); ?></label>
                <div class="col-lg-6">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" id="optionsRadios1" name="status" id="status_active" <?php echo (isset($state['status']) && ($state['status'] == 1)) ? "checked" : ""; ?>>
                            <?php __t("Active"); ?>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" id="optionsRadios1" name="status" id="status_inactive" <?php echo (isset($state['status']) && ($state['status'] == 0)) ? "checked" : ""; ?>>
                            <?php __t("Inactive"); ?>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button class="btn btn-primary" type="submit"><?php echo __t('Save'); ?></button>
                    <a class="btn btn-default" href="<?php echo $this->config->item('base_url'); ?>location/states"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- page end-->
