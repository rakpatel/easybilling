<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Country'); ?> 
            <a href="<?php echo $this->config->item('base_url'); ?>location/update_country/0" class="label label-primary" style="float: right;"><?php echo __t('Add Country'); ?></a>
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <?php echo $this->load->view('admin/common/index_listing', $dataTableObject, true); ?>
        </div>
    </div>
</section>
<!-- page end-->
