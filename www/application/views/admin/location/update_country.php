<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
            if (empty($id)) {
                echo __t('Add Country');
                $formId = "CountryForm";
            } else {
                echo __t('Edit Country');
                $formId = "CountryEditForm";
            }
            ?>
        </span>
    </header>
    <div class="panel-body">
        <form class="cmxform form-horizontal tasi-form" id="<?php echo $formId; ?>" method="post" action="">
            <input type="hidden" id="country_id" value="<?php echo (isset($country['country_id'])) ? $country['country_id'] : ""; ?>"/>
            <!--<div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Status'); ?></label>
                <div class="col-lg-6">
                    <select class=" form-control" id="status" name="status">
                        <option value="1" <?php echo (isset($category['status']) && $category['status'] == 1) ? "selected" : ""; ?>><?php echo __t('Active'); ?></option>
                        <option value="0" <?php echo (isset($category['status']) && $category['status'] == 0) ? "selected" : ""; ?>><?php echo __t('Inactive'); ?></option>
                    </select>
                </div>
            </div>-->
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('Country Code'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" id="iso3" maxlength="3" name="iso3" type="text" value="<?php echo (isset($country['iso3'])) ? $country['iso3'] : ""; ?>" <?php if (!empty($id)) {
                echo 'readonly=""';
            } ?> />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('Country Name'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="20" id="country_name" name="country_name" type="text" value="<?php echo (isset($country['country_name'])) ? $country['country_name'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('Local Name'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="20" id="local_name" name="local_name" type="text" value="<?php echo (isset($country['local_name'])) ? $country['local_name'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('ISO Code'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" id="iso2" maxlength="2" name="iso2" type="text" value="<?php echo (isset($country['iso2'])) ? $country['iso2'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Region'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="26" id="region" name="region" type="text" value="<?php echo (isset($country['region'])) ? $country['region'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Continent'); ?></label>
                <div class="col-lg-6">

                    <select class=" form-control" id="continent" name="continent">
                        <option value=""><?php echo __t("  -- Select Continent -- ") ?></option>
                        <option <?php echo (isset($country['continent']) && $country['continent'] == "Asia") ? "selected" : ""; ?> value="Asia">Asia</option>
                        <option <?php echo (isset($country['continent']) && $country['continent'] == "Europe") ? "selected" : ""; ?> value="Europe">Europe</option>
                        <option <?php echo (isset($country['continent']) && $country['continent'] == "North America") ? "selected" : ""; ?> value="North America">North America</option>
                        <option <?php echo (isset($country['continent']) && $country['continent'] == "Africa") ? "selected" : ""; ?> value="Africa">Africa</option>
                        <option <?php echo (isset($country['continent']) && $country['continent'] == "Oceania") ? "selected" : ""; ?> value="Oceania">Oceania</option>
                        <option <?php echo (isset($country['continent']) && $country['continent'] == "Antarctica") ? "selected" : ""; ?> value="Antarctica">Antarctica</option>
                        <option <?php echo (isset($country['continent']) && $country['continent'] == "South America") ? "selected" : ""; ?> value="South America">South America</option>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Latitude'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="latitude" name="latitude" type="text" value="<?php echo (isset($country['latitude'])) ? $country['latitude'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Longitude'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="longitude" name="longitude" type="text" value="<?php echo (isset($country['longitude'])) ? $country['longitude'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Surface Area'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="surface_area" name="surface_area" type="text" value="<?php echo (isset($country['surface_area'])) ? $country['surface_area'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Population'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="population" name="population" type="text" value="<?php echo (isset($country['population'])) ? $country['population'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="col-lg-2 control-label require"><?php echo __t("Status"); ?></label>
                <div class="col-lg-6">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" name="status" id="status_active" <?php echo (isset($country['status']) && ($country['status'] == 1)) ? "checked" : ""; ?>>
                            <?php __t("Active"); ?>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" name="status" id="status_inactive" <?php echo (isset($country['status']) && ($country['status'] == 0)) ? "checked" : ""; ?>>
                            <?php __t("Inactive"); ?>
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label for="status" class="col-lg-2 control-label require"><?php echo __t("Is front"); ?></label>
                <div class="col-lg-6">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" name="is_front" id="is_front" <?php echo (isset($country['is_front']) && ($country['is_front'] == 1)) ? "checked" : ""; ?>>
                            <?php __t("Yes"); ?>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" id="is_front" name="is_front"  <?php echo (isset($country['is_front']) && ($country['is_front'] == 0)) ? "checked" : ""; ?>>
                            <?php __t("No"); ?>
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button class="btn btn-primary" type="submit"><?php echo __t('Save'); ?></button>
                    <a class="btn btn-default" href="<?php echo $this->config->item('base_url'); ?>location/countries"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- page end-->
