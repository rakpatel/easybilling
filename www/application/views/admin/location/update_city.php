<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
            if (empty($id)) {
                echo __t('Add City');
                $formId = "CityForm";
            } else {
                echo __t('Edit City');
                $formId = "CityForm";
            }
            ?>
        </span>
    </header>
    <div class="panel-body">
        <form class="cmxform form-horizontal tasi-form" id="<?php echo $formId; ?>" method="post" action="">
            <!--<div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Status'); ?></label>
                <div class="col-lg-6">
                    <select class=" form-control" id="status" name="status">
                        <option value="1" <?php echo (isset($category['status']) && $category['status'] == 1) ? "selected" : ""; ?>><?php echo __t('Active'); ?></option>
                        <option value="0" <?php echo (isset($category['status']) && $category['status'] == 0) ? "selected" : ""; ?>><?php echo __t('Inactive'); ?></option>
                    </select>
                </div>
            </div>-->
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('City Name'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="100" id="city_name" name="city_name" type="text" value="<?php echo (isset($city['city_name'])) ? $city['city_name'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('Country Name'); ?></label>
                <div class="col-lg-6">
                    <select name="country_id" id="country_id" class=" form-control">
                        <option value=""><?php echo __t(" -- Select Country -- ") ?></option>
                        <?php
                        if (isset($countries) && !empty($countries)) {
                            foreach ($countries as $countrie) {
                                $selected = ($city['country_id'] == $countrie['country_id']) ? "selected" : "";
                                echo "<option $selected  value='" . $countrie['country_id'] . "'>" . $countrie['country_name'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 "><?php echo __t('Province/State/Region Name'); ?></label>
                <div class="col-lg-6">
                    <select name="state_id" id="state_id" class=" form-control">
                        <option value=""><?php echo __t(" -- Select Province/State/Region -- ") ?></option>
                        <?php
                        if (isset($states) && !empty($states)) {
                            foreach ($states as $state) {
                                $selected = ($city['state_id'] == $state['state_id']) ? "selected" : "";
                                echo "<option $selected  value='" . $state['state_id'] . "'>" . $state['state_name'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Latitude'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="latitude" name="latitude" type="text" value="<?php echo (isset($city['latitude'])) ? $city['latitude'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Longitude'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="10" id="longitude" name="longitude" type="text" value="<?php echo (isset($city['longitude'])) ? $city['longitude'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="col-lg-2 control-label require"><?php echo __t("Status"); ?></label>
                <div class="col-lg-6">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" name="status" id="status_active" <?php echo (isset($city['status']) && ($city['status'] == 1)) ? "checked" : ""; ?>>
                            <?php __t("Active"); ?>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" name="status" id="status_inactive" <?php echo (isset($city['status']) && ($city['status'] == 0)) ? "checked" : ""; ?>>
                            <?php __t("Inactive"); ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="status" class="col-lg-2 control-label require"><?php echo __t("Is front"); ?></label>
                <div class="col-lg-6">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" name="is_front" id="is_front" <?php echo (isset($city['is_front']) && ($city['is_front'] == 1)) ? "checked" : ""; ?>>
                            <?php __t("Yes"); ?>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" id="is_front" name="is_front"  <?php echo (isset($city['is_front']) && ($city['is_front'] == 0)) ? "checked" : ""; ?>>
                            <?php __t("No"); ?>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button class="btn btn-primary" type="submit"><?php echo __t('Save'); ?></button>
                    <a class="btn btn-default" href="<?php echo $this->config->item('base_url'); ?>location/cities"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- page end-->
