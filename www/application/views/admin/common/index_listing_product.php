<?php if (isset($dataTableObject['search_feilds']) && !empty($dataTableObject['search_feilds'])) { ?>
    <div class="panel-group m-bot20" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    <h4 class="panel-title">
                        <?php echo __t("Advance Search"); ?>
                        <span class="pull-right fa fa-chevron-down" style="color:#a7a7a7;"></span>
                    </h4>
                </a>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" style="border:1px solid #bec3c7;">
                <div class="panel-body">
                    <form action="" name="searchForm" id="searchForm" method="post" onsubmit="searchRecords(this);
                            return false;">
                        <?php
                        foreach ($dataTableObject['search_feilds'] as $key => $value) {
                            switch ($value['type']) {
                                case 'text':
                                    echo "<div class='form-group col-lg-4'><lable>" . $value['lable'] . "</lable>";
                                    $class = (isset($value['class'])) ? $value['class'] : "";
                                    echo "<input type='text' class='form-control $class ' name='$key' placeholder='Enter " . $value['lable'] . "'></div>";
                                    break;
                                case 'select':
                                    echo "<div class='form-group col-lg-4'><lable>" . $value['lable'] . "</lable>";
                                    echo "<select class='form-control' name='$key' id='$key'>" .
                                    "<option value=''>-- Select " . $value['lable'] . " --</option>";
                                    foreach ($value['options'] as $k => $v) {
                                        echo "<option value='$k'>$v</option>";
                                    }
                                    echo "</select></div>";
                                    break;
                                case 'checkbox':
                                    echo "<div class='form-group col-lg-4'><lable>" . $value['lable'] . "</lable>";
                                    foreach ($value['options'] as $option) {
                                        echo '<div class="checkbox"><label><input type="checkbox" name="' . $key . '[]" value="' . $option . '"> ' . $option . '</label></div>';
                                    }
                                    echo "</div>";
                                    break;
                                case 'daterange' :
                                    echo "<div class='form-group col-lg-4'><lable>" . $value['lable'] . "</lable>";
                                    ?>
                                    <div class="input-group input-large date_range">
                                        <input type="text" class="form-control default-date-picker nopad_r from_date" name="<?php echo $key;?>_from" id="<?php echo $key;?>_from" value="" placeholder="<?php echo $value['lable'];?> From" data-id="<?php echo $key;?>">
                                        <span class="input-group-addon">To</span>
                                        <input type="text" class="form-control default-date-picker nopad_r to_date" name="<?php echo $key;?>_to" id="<?php echo $key;?>_to" value="" placeholder="<?php echo $value['lable'];?> To" data-id="<?php echo $key;?>">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn date-reset" onclick="$('[name=<?php echo $key;?>_from]').val('');$('[name=<?php echo $key;?>_to]').val('');" alt="Clear Dates" title="Clear Dates"> <i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                    <?php
                                    echo "</div>";
                                    break;
                            }
                        }
                        echo "<div class='form-group col-lg-4' style='float:right;margin-top:15px;text-align:right;'><input type='submit' class='btn btn-info' value='Search'>&nbsp;&nbsp;&nbsp;<input type='reset' class='btn btn-danger'></div>";
                        ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php
$table_id = (isset($dataTableObject['table_id']) && $dataTableObject['table_id'] != '') ? 'id=' . $dataTableObject['table_id'] : '';
echo "<table " . $table_id . " class='display table table-bordered table-striped dataTable'>";
if (!empty($dataTableObject['columns'])) {
    echo "<thead><tr>";
    $newth = '';
    foreach ($dataTableObject['columns'] as $field => $arrFieldDetail) {
        $class = "";
        $width = (isset($arrFieldDetail['width']) && $arrFieldDetail['width'] > 0) ? ' style="width:' . $arrFieldDetail['width'].';"' : '';
        $class = (isset($arrFieldDetail['class']) && $arrFieldDetail['class'] != "") ? $arrFieldDetail['class'] : ' ';
        $class .= (isset($arrFieldDetail['type']) && $arrFieldDetail['type'] == "hidden") ? 'hidden' : '';
        $newth .= "<th " . $width . " class='" . $class . "'>" . $field . "</th>";
    }
    echo $newth;
    echo "</tr></thead>";
}
echo "</table>";
//die('dsds');

// Part remain for datatable initialization
?>
<script>
    var table_id = '<?php echo $dataTableObject['table_id']; ?>';
    var db_fields_list = <?php echo isset($dataTableObject['db_fields_list']) ? json_encode(array_flip($dataTableObject['db_fields_list'])) : ''; ?>;
    var sort_field = <?php echo isset($dataTableObject['sort_field']) ? json_encode($dataTableObject['sort_field']) : ''; ?>;
    var sort_field_default = <?php echo isset($dataTableObject['sort_field_default']) ? json_encode($dataTableObject['sort_field_default']) : ''; ?>;
    var oTable = "";
    var ajaxURL = '<?php echo $ajaxUrl; ?>';
    $(document).ready(function () {
        showloading();
        oTable = $('#tblProductList').dataTable({
                "bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": ajaxURL,
                "sPaginationType": "full_numbers",
                "fnServerData": function(sSource, aoData, fnCallback)
                {
                    $.ajax
                    ({
                        'dataType': 'json',
                        'type'    : 'POST',
                        'url'     : sSource,
                        'data'    : aoData,
                        'success' : fnCallback,
                    });
                },
            "aoColumnDefs": [
                {"bSortable": false, "aTargets": sort_field}
            ],
            //"bFilter": false,
            "aaSorting": [sort_field_default],
        });
        hideloading();
        $('input[type="reset"]').click(function () {
            fnResetAllFilters();
        });
        $('.accordion-toggle').click(function () {
            $(this).find('span').toggleClass('fa-chevron-down')
            $(this).find('span').toggleClass('fa-chevron-up')
        });
        $('.accordion-toggle .fa-chevron-down').click(function () {
            $(this).find('span').toggleClass('fa-chevron-down')
            $(this).find('span').toggleClass('fa-chevron-up')
        });
        $('.accordion-toggle .fa-chevron-up').click(function () {
            $(this).find('span').toggleClass('fa-chevron-down')
            $(this).find('span').toggleClass('fa-chevron-up')
        });
    });
    function searchRecords(obj) {
        showloading();
        fnResetAllFilters();
        var filter = [];
        $(obj).find('input').each(function () {
            if ($(this).attr('type') == 'checkbox') {
                if ($(this).is(':checked')) {
                    filter.push($(this).val());
                }
            }
        });
        //console.log(filter.join('|'));
        oTable.fnFilter(filter.join('|'), '3', true, false, true, true);
        $(obj).find('input').each(function () {
            if ($(this).val() != "" && $(this).attr('type') != 'submit' && $(this).attr('type') != 'reset' && $(this).attr('type') != 'checkbox'
                    && !($(this).hasClass('default-date-picker'))
                ) {
                oTable.fnFilter($(this).val(), db_fields_list[$(this).attr('name')]);
            }
        });
        $(obj).find('select').each(function () {
            if ($(this).val() != "") {
                oTable.fnFilter($(this).val(), db_fields_list[$(this).attr('name')]);
            }
        });
        
        $(".date_range").each(function(){
            
            var opening_from = $(this).find('.from_date').val();
            var opening_to = $(this).find('.to_date').val();
            var field_no = $(this).find('input[type="text"]').attr('data-id');
            
            if(opening_from != "" && opening_to != "") {
                $.fn.dataTableExt.afnFiltering.push(function( oSettings, aData, iDataIndex ){

                    opening_from = opening_from.replace(/-/g,'');
                    opening_to = opening_to.replace(/-/g,'');

                    var flag_for_get_data = false;
                    var date_of_open = aData[db_fields_list[field_no]];

                    if ( isNaN(opening_from) && isNaN(opening_to) )
                        flag_for_get_data = true;
                    else if ( opening_from <= date_of_open && isNaN(opening_to))
                        flag_for_get_data = true;
                    else if ( opening_to >= date_of_open && isNaN(opening_from))
                        flag_for_get_data = true;
                    else if (opening_from <= date_of_open && opening_to >= date_of_open)
                        flag_for_get_data = true;

                    return flag_for_get_data;

                });
            }
        });
        oTable.fnDraw();
        $('body').scrollTo($('.dataTables_wrapper'));
        hideloading();
        return false;
    }
    function fnResetAllFilters() {
        var oSettings = oTable.fnSettings();
        $.fn.dataTableExt.afnFiltering = [];
        for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
            oSettings.aoPreSearchCols[ iCol ].sSearch = '';
        }
        oSettings.oPreviousSearch.sSearch = '';
        oTable.fnDraw();
    }
</script>