<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <ul class="sidebar-menu" id="nav-accordion">
            <?php
            $controller = $this->uri->segment(1);
            $action = $this->uri->segment(2);
            $parameter = $this->uri->segment(3);
            $CurrentUrl = $controller . "/" . $action;
            if($parameter) {
                $CurrentUrl .= "/".$parameter;
            }
            if (isset($menu_array) && count($menu_array) > 0) {
                foreach ($menu_array as $key => $value) {
                    if (is_array($value)) {
                        if (isset($value['icon'])) {
                            $icon = $value['icon'];
                            unset($value['icon']);
                        } else
                            $icon = 'fa-folder';
                        if (isset($value['url'])) {
                            $url = $value['url'];
                            unset($value['url']);
                        } else
                            $url = '';
                        
                        $activeClassMain = '';
                        $activeClassSub = '';
                        
                       // echo $CurrentUrl . "--- " . $url  . " ---" . $controller . " ---" . $action; 
                        if($CurrentUrl == $url || ($controller == $url && $action == '')||($controller==$url) ) {
                            $activeClassMain = "active";
                        }
                        if (isset($value['actions']) && !empty($value['actions'])) {
                        foreach ($value['actions'] as $k => $v) {
                            //echo '--'.$k .'--<br/>' ; 
                            if($CurrentUrl == $v ) {
                                $activeClassMain = "active";
                            }
                            
                            if($k == 'Account Manager' && $controller == 'user' && in_array($action,array('edit_account')) ) $activeClassMain = "active";
                            if($k == 'Suppliers' && $controller == 'user' && in_array($action,array('edit_supplier')) ) $activeClassMain = "active";
                            if($k == 'Suppliers' && $controller == 'user' && in_array($action,array('edit_supplier')) ) $activeClassMain = "active";
                            if($k == 'Customers' && $controller == 'customer' && in_array($action,array('edit_customer')) ) $activeClassMain = "active";
                            if($k == 'Countries' && $controller == 'location' && in_array($action,array('update_country')) ) $activeClassMain = "active";
                            if($k == 'States' && $controller == 'location' && in_array($action,array('update_state')) ) $activeClassMain = "active";
                            if($k == 'Cities' && $controller == 'location' && in_array($action,array('update_city')) ) $activeClassMain = "active";
                        }
                        }
                        
                    }
                    ?>
                    <li class="sub-menu">
                        <a <?php echo ($url  == 'cms-faq-supplier')?"target='_blank'":""; ?> href="<?php echo $this->config->item('base_url') . $url ?>" class="<?php echo $activeClassMain;?>">
                            <i class="fa <?php echo $icon; ?>"></i>
                            <span><?php echo $key; ?></span>
                        </a>
                        <?php
                        if (isset($value['actions']) && !empty($value['actions'])) {
                            echo '<ul class="sub">';
                            foreach ($value['actions'] as $k => $v) {
                                $activeClassSub = "";
                                if($CurrentUrl == $v ) {
                                    $activeClassSub = "active";
                                }
                                if($k == 'Account Manager' && $controller == 'user' && in_array($action,array('edit_account')) ) $activeClassSub = "active";
                                if($k == 'Suppliers' && $controller == 'user' && in_array($action,array('edit_supplier')) ) $activeClassSub = "active";
                                if($k == 'Customers' && $controller == 'customer' && in_array($action,array('edit_customer')) ) $activeClassSub = "active";
                                if($k == 'Countries' && $controller == 'location' && in_array($action,array('update_country')) ) $activeClassSub = "active";
                                if($k == 'States' && $controller == 'location' && in_array($action,array('update_state')) ) $activeClassSub = "active";
                                if($k == 'Cities' && $controller == 'location' && in_array($action,array('update_city')) ) $activeClassSub = "active";
                                
                                if($k == 'Product Categories' && $controller == 'category' && in_array($action,array('update')) ) $activeClassSub = "active";
                                if($k == 'Recommended Categories' && $controller == 'category' && in_array($action,array('update_recommended_category')) ) $activeClassSub = "active";
                                if($k == 'Supplier Categories' && $controller == 'category' && in_array($action,array('update_supplier_category')) ) $activeClassSub = "active";
                                if($k == 'Tour/Activity Categories' && $controller == 'category' && in_array($action,array('update_tour_activity_category')) ) $activeClassSub = "active";
                                if($k == 'FAQ Categories' && $controller == 'category' && in_array($action,array('update_faq_category')) ) $activeClassSub = "active";
                                 if($k != 'actions') {
                                ?>
                                <li class="<?php echo $activeClassSub;?>"><a   href="<?php echo $this->config->item('base_url') . $v; ?>"><?php echo $k; ?></a></li>
                            <?php
                                 } else {
                                     echo '<li class="sub-menu dcjq-parent-li"><a href="javascript:void(0);" class="dcjq-parent active">Suppliers<span class="dcjq-icon"></span></a><ul class="sub" style="display: block;">';
                                     foreach($v as $sk => $sv) {
                                        $activeClassSub = ''; 
                                        if($CurrentUrl == $sv ) {
                                            $activeClassSub = "active";
                                        } 
                                         
                                         echo '<li class="'.$activeClassSub.'"><a href="'.  base_url() . $sv .'">'.$sk.'</a></li>';
                                    }
                                    echo '</ul></li>';
                                 }
                        }
                        echo '</ul>';
                    }
                    ?>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
</aside>
<!--sidebar end-->