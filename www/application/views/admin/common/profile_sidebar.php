<aside class="profile-nav col-lg-3">
    <section class="panel">
        <div class="user-heading round">
            <a href="javascript:void(0);">
                <img src="<?php echo base_url();?>/assets/admin/img/profile-avatar.jpg" alt="">
            </a>
            <h1><?php echo $user_data['first_name'] . " " . $user_data['last_name']; ?></h1>
            <p><?php echo $user_data['email']; ?></p>
        </div>
        <?php
            $controller = $this->uri->segment(1);
            $action = $this->uri->segment(2);
            $CurrentUrl = $controller . "/" . $action;
        ?>
        <ul class="nav nav-pills nav-stacked">
            <li class="<?php echo ($CurrentUrl == 'user/profile')?'active':''; ?>"><a href="<?php echo $this->config->item('base_url'); ?>user/profile"> <i class="fa fa-user"></i>Profile</a></li>
            <?php if($user_data['role_id'] == 3 ) { ?>
            <li class="<?php echo ($CurrentUrl == 'user/company_details')?'active':''; ?>"><a href="<?php echo $this->config->item('base_url'); ?>user/company_details"> <i class="fa fa-info"></i>Company Details</a></li>
            <li class="<?php echo ($CurrentUrl == 'user/other_contacts')?'active':''; ?>"><a href="<?php echo $this->config->item('base_url'); ?>user/other_contacts"> <i class="fa fa-tasks"></i>Other Contacts</a></li>
            <li class="<?php echo ($CurrentUrl == 'user/notification')?'active':''; ?>"><a href="<?php echo $this->config->item('base_url'); ?>user/notification"> <i class="fa  fa-bell-o"></i>Notifications</a></li>
            <li class="<?php echo ($CurrentUrl == 'user/payment_information')?'active':''; ?>"><a href="<?php echo $this->config->item('base_url'); ?>user/payment_information"> <i class="fa fa-calendar"></i>Payment Information</a></li>
            <li class="<?php echo ($CurrentUrl == 'user/upload_document')?'active':''; ?>"><a href="<?php echo $this->config->item('base_url'); ?>user/upload_document"> <i class="fa fa-upload"></i>Upload Document</a></li>
            <?php } ?>
            <li class="<?php echo ($CurrentUrl == 'user/change_password')?'active':''; ?>"><a href="<?php echo $this->config->item('base_url'); ?>user/change_password"> <i class="fa fa-key"></i>Change Password</a></li>
        </ul>

    </section>
</aside>