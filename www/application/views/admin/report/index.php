<!-- banner start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Reports'); ?> 
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="accordion" class="panel-group m-bot20">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <h4 class="panel-title">
                                Generate Reports                        <span class="pull-right fa fa-chevron-down" style="color:#a7a7a7;"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="collapseOne" class="panel-collapse in" style="border: 1px solid rgb(190, 195, 199); height: auto;">
                        <div class="panel-body">
                            <form name="reportForm" id="reportForm" method="post">
                                <div class="form-group col-lg-4">
                                    <label>Select Report</label>
                                    <select name="report_type" class="form-control" id="report_type">
                                        <option value=""> -- Select Report Type -- </option>
                                        <option <?php if(isset($data['report_type']) && $data['report_type'] == 'sales') { echo "selected";} ?> value="sales">Sales</option>
                                        <option <?php if(isset($data['report_type']) && $data['report_type'] == 'purchase') { echo "selected";} ?> value="purchase">Purchase</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>From Month</label>
                                    <input autocomplete="false" class="form-control default-date-picker nopad_r from_date" type="text" name="from_date" id="from_date" placeholder="Please Select From Month" value="<?php if(isset($data['from_date']) && !empty($data['from_date'])) { echo $data['from_date'];} ?>" readonly="readonly">
                                </div>
                                <div class="form-group col-lg-4">
                                    <label>To Month</label>
                                    <input autocomplete="false" class="form-control default-date-picker nopad_r to_date" type="text" name="to_date" id="to_date" placeholder="Please Select To Month" value="<?php if(isset($data['to_date']) && !empty($data['to_date'])) { echo $data['to_date'];} ?>" readonly="readonly">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-lg-4" style="float:right;margin-top:15px;text-align:right;">
                                    <input type="submit" class="btn btn-info" value="Generate Report">&nbsp;&nbsp;&nbsp;
                                    <input type="reset" class="btn btn-danger">
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="panel-body">
                        <?php if(isset($reportData) && !empty($reportData)) { ?>
                            <div class="clearfix"></div>
                            <br/>
                            <div style="overflow-y:auto;">
                                <table id='reportData' class='table table-bordered table-striped table-condensed'>
                                    <thead>
                                            <?php
                                                foreach ($reportFieldData as $key => $field) {
                                                    echo '<td>'.$field.'</td>';
                                                }
                                            ?>
                                    </thead>
                                    <?php
                                        if(isset($data['report_type']) && $data['report_type'] == 'sales')
                                        {
                                            $amount = 0;
                                            foreach ($reportData as $key => $value) {
                                                echo '<tr>';
                                                echo '<td>'.$value['invoice_id'].'</td>';
                                                echo '<td>'.$value['username'].'</td>';
                                                echo '<td>'.$value['sales_date'].'</td>';
                                                echo '<td>'.$value['discount_amount'].'</td>';
                                                echo '<td align="right">'.number_format($value['total'], 2).'</td>';
                                                echo '</tr>';
                                                if($value['total'] > 0) $amount = $amount + $value['total']; 
                                            }
                                            echo '<tr>
                                                <td colspan="5" align="right"><b>'. number_format($amount, 2) .'</b></td>
                                            </tr>';
                                        }
                                        elseif(isset($data['report_type']) && $data['report_type'] == 'purchase')
                                        {
                                            $amount = 0;
                                            foreach ($reportData as $key => $value) {
                                                echo '<tr>';
                                                echo '<td>'.$value['purchase_invoice_id'].'</td>';
                                                echo '<td>'.$value['name'].'</td>';
                                                echo '<td>'.$value['purchase_date'].'</td>';
                                                echo '<td>'.$value['discount_amount'].'</td>';
                                                echo '<td align="right">'.number_format($value['total'], 2).'</td>';
                                                echo '</tr>';
                                                if($value['total'] > 0) $amount = $amount + $value['total']; 
                                            }
                                            echo '<tr>
                                                <td colspan="5" align="right"><b>'. number_format($amount, 2) .'</b></td>
                                            </tr>';
                                        }
                                    ?>
                                </table>
                            </div>
                        <?php } else {  
                            if(isset($data) && !empty($data)) {
                        ?>
                            <h4>No data found for this period.</h4>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #fff !important;
            cursor: default !important;
          }
    </style>
</section>