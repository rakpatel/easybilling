<!-- banner start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Reports'); ?> 
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div id="accordion" class="panel-group m-bot20">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <h4 class="panel-title">
                                Generate Reports                        <span class="pull-right fa fa-chevron-down" style="color:#a7a7a7;"></span>
                            </h4>
                        </a>
                    </div>
                    <div id="collapseOne" class="panel-collapse in" style="border: 1px solid rgb(190, 195, 199); height: auto;">
                        <div class="panel-body">
                            <form name="CommiReportForm" id="CommiReportForm" method="post">
                                <div class="form-group col-lg-4">
                                    <label class="require">Month</label>
                                    <input autocomplete="false" class="form-control drp1" type="text" name="month" id="month" placeholder="Please Select Month" value="<?php
                                    if (isset($data['month']) && !empty($data['month'])) {
                                        echo $data['month'];
                                    }
                                    ?>">
                                </div>
                                <!--<div class="form-group col-lg-4">
                                    <label class="require">Currency</label>
                                    <select name="currencies" class="form-control" id="currencies">
                                        <option value=""> -- Select Currency -- </option>
                                <?php
                                foreach ($currencies as $currency) {
                                    $selected = (isset($data['currencies']) && $data['currencies'] == $currency['id']) ? "selected" : "";
                                    if (isset($data['currencies']) && $data['currencies'] == $currency['id']) {
                                        $selectedCurre = $currency['currency_name'];
                                        $currencySelected = '(' . $currency['currency_code'] . ')';
                                    }
                                    echo '<option ' . $selected . ' value="' . $currency['id'] . '">' . $currency['currency_name'] . '</option>';
                                }
                                ?>
                                    </select>
                                </div>-->
<?php if (isset($role_id) && $role_id != 3) { ?>

                                    <div class="form-group col-lg-4">
                                        <label class="require">Supplier</label>
                                        <select name="supplier_id" class="form-control" id="supplier_id">
                                            <option value=""> -- Select Supplier -- </option>
                                            <?php
                                            foreach ($suppliers as $supplier) {
                                                $selected = (isset($data['supplier_id']) && $data['supplier_id'] == $supplier['user_id']) ? "selected" : "";
                                                echo '<option ' . $selected . ' value="' . $supplier['user_id'] . '">' . $supplier['company_name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4" id="productDiv">
                                        <label>Products</label>
                                        <select name="product_id" class="form-control" id="product_id">
                                            <option value=""> -- Select Product -- </option>
                                            <?php
                                            foreach ($products as $product) {
                                                $selected = (isset($data['product_id']) && $data['product_id'] == $product['id']) ? "selected" : "";
                                                if (isset($data['product_id']) && $data['product_id'] == $product['id']) {
                                                    $selectedPro = $product['product_name'];
                                                }
                                                echo '<option ' . $selected . ' value="' . $product['id'] . '">' . $product['product_name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>

<?php } ?>
                                <div class="form-group col-lg-4" style="float:right;margin-top:15px;text-align:right;">
                                    <input type="submit" class="btn btn-info" value="Generate Report">
                                    <!--<input type="reset" class="btn btn-danger">-->
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="panel-body">
                        <?php if (isset($reportData) && !empty($reportData)) { ?>
                            Export to: <a class="btn btn-primary" href="javascript:exportToExcel();" style="margin: 15px 0 15px 15px;">Excel</a>
                            <a class="btn btn-primary" href="<?php echo base_url() . 'report/generate_pdf/' . base64_encode(json_encode($data)); ?>" style="margin:15px 0 15px 15px;">PDF</a>
                            <br/>
                            <div style="overflow-y:auto;">
                                <?php
                                //pr($currencySelected,false);
                                $currencySelected = (isset($currencySelected['currency_code']) && !empty($currencySelected['currency_code'])) ? '(' . $currencySelected['currency_code'] . ')' : '';
                                //$currencySelected = (isset($data['currencies']) && !empty($data['currencies']))?'('.$data['currencies'].')':'';
                                echo "<table id='reportData' class='table table-bordered table-striped table-condensed'>";
                                ?>
                                <thead id="logoHead" style="display:none;">
                                    <tr>
                                        <td style="height: 65px;" colspan="4"><img src="<?php echo base_url(); ?>assets/frontend/images/logo.jpg" style="width: 160px; height: 65px;" /></td>
                                        <td style="text-align: right;" colspan="3" valign="top" ><strong><br/>Commistion Report</strong></td>
                                    </tr>
                                </thead>
                                <?php
                                echo "<thead><tr>";
                                echo "<th data-type='string'>Date</th>";
                                echo "<th data-type='string'>Booking Reference</th>";
                                echo "<th data-type='string'>Product Title</th>";
                                echo "<th data-type='string' style='text-align:right;'>Sales Amount $currencySelected</th>";
                                echo "<th data-type='string' style='text-align:right;'>Sales Amount After Special Discount $currencySelected</th>";
                                echo "<th data-type='string' style='text-align:right;'>Commission Amount</th>";
                                if (isset($role_id) && $role_id != 3) {
                                    echo "<th data-type='string' style='text-align:right;'>Total Payable Amount</th>";
                                } else {
                                    echo "<th data-type='string' style='text-align:right;'>Total Receivable Amount</th>";
                                }
                                echo "</tr></thead>";

                                $saleAmtTotal = $saleAmtSpeDicTotal = $commiAmtTotal = $payableAmtTotal = 0;

                                echo "<tbody>";
                                foreach ($reportData as $repdata) {
                                    $json_data = json_decode($repdata['json_data'], true);
                                    //pr($json_data,false);
                                    echo "<tr>";
                                    echo "<td>" . date('d-m-Y', strtotime($repdata['date_booking'])) . "</td>";
                                    echo "<td>" . $repdata['reference_code'] . "</td>";
                                    echo "<td>" . $json_data['product']['product_name'] . "</td>";
                                    if (isset($json_data['specialOffer'])) {
                                        echo "<td align='right'>" . ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']) . "</td>";
                                        echo "<td align='right'>" . (ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) . "</td>";
                                        $saleAmtTotal += ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']);
                                        $saleAmtSpeDicTotal += (ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] );
                                    } else {
                                        echo "<td align='right'>" . ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']) . "</td>";
                                        echo "<td align='right'>" . ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']) . "</td>";
                                        $saleAmtTotal += ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']);
                                        $saleAmtSpeDicTotal += ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']);
                                    }
                                    if (isset($repdata['commission_rate']) && !empty($repdata['commission_rate'])) {
                                        echo "<td align='right'>" . ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100) . "</td>";
                                        echo "<td align='right'>" . ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) - ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100)) . "</td>";
                                        $commiAmtTotal += ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100);
                                        $payableAmtTotal += ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) - ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100));
                                    } else {
                                        echo "<td align='right'>0</td>";
                                        echo "<td align='right'>" . $json_data['supplierPrice'] . "</td>";
                                        $payableAmtTotal += $json_data['supplierPrice'];
                                    }
                                    echo "</tr>";
                                }
                                echo "<thead><tr>";
                                echo "<th colspan='3' style='text-align: right;'>Total</th>";
                                echo "<th style='text-align: right;'>" . $saleAmtTotal . "</th>";
                                echo "<th style='text-align: right;'>" . $saleAmtSpeDicTotal . "</th>";
                                echo "<th style='text-align: right;'>" . $commiAmtTotal . "</th>";
                                echo "<th style='text-align: right;'>" . $payableAmtTotal . "</th>";
                                echo "</thead></tr>";
                                echo "</tbody>";
                                echo "</table>";
                                ?>
                            </div>
                            <script type="text/javascript">
                                function exportToExcel() {
                                    $("#logoHead").removeAttr('style');
                                    $("#reportData").btechco_excelexport({
                                        containerid: "reportData",
                                        filename: 'reportData',
                                        datatype: $datatype.Table,
                                        name: 'reportData'
                                    });
                                    $("#logoHead").hide();
                                }
                            </script>
                            <?php
                        } else {
                            if (isset($data) && !empty($data)) {
                                ?>
                                <h4>No data found for this period.</h4>
                            <?php }
                        }
                        ?>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#supplier_id').change(function () {
                                    if ($(this).val() != '') {
                                        $.ajax({
                                            url: base_url + "report/get_supplier_products/" + $(this).val(),
                                            type: "POST",
                                            success: function (result) {
                                                $('#product_id').html(result);
                                            }
                                        });
                                    } else {
                                        $('#product_id').html('<option value=""> -- Select Product -- </option>');
                                    }
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        .form-control.error{
            border: 1px solid #b94a48 !important;
        }
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #fff !important;
            cursor: default !important;
        }
    </style>
</section>
<!-- banner end-->