<!-- banner start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Product View Report'); ?> 
            <a href="javascript:void(0)" id="btnExport" class="label label-primary" style="float: right;"><?php echo __t('Export'); ?></a>
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <?php echo $this->load->view('admin/common/index_listing', $dataTableObject, true); ?>
        </div>
    </div>
</section>
<!-- banner end-->
<script>
     $(function(){
    $('#btnExport').click(function(){
       
            var url = '<?php echo base_url() ?>'+'report/exportToExcel';
            var searchParamFlag = $('.btn-info').attr('data-click');
            var data ={};
            if(searchParamFlag==1 && searchParamFlag!=undefined){
                var product_name  = $('.product_name').val();
                var counter = $('.counter').val();
                var dateFrom = $('.from_date').val();
                var dateTo  = $('.to_date').val() ;
                var data = {product_name:product_name,counter:counter,dateFrom:dateFrom,dateTo:dateTo};
            }
            //alert(product_name);
            $.ajax({
                method: "POST",
                url: url,
                data: data
                })
                .done(function( msg ) {
                  createExcel(msg);
                }).error(function(){
                    //alert('Please Try Again!')
                });
        });
        
        $('.btn-info').on('click',function(){
           $(this).attr('data-click','1');
        });
    });
    
    function createExcel(out)
    {
        var arrReturn = jQuery.parseJSON(out);
        if (arrReturn.message.indexOf("Error:") >= 0) {
           displayError('Product',arrReturn.message);
        } else {
            var dataobj = arrReturn.arrReport;
            var datacol = arrReturn.columns;
            $("#tblProductView").btechco_excelexport({
                containerid: "tblProductView"
                , datatype: $datatype.Json
                , dataset: dataobj
                , columns: datacol
                , name: 'Product View'
                , filename: 'Product view'
            });
        }
    }
    
</script>   