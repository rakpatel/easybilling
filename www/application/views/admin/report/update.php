<!-- banner start-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>assets/third-party/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
            if ($id == 0) {
                echo __t('Add Landing Page Banner');
                $formId = "CountryBannerAddForm";
            } else {
                echo __t('Edit Landing Page Banner');
                $formId = "CountryBannerEditForm";
            }
            ?>
        </span>
    </header>
    <div class="panel-body">
        <form class="cmxform form-horizontal" id="<?php echo $formId; ?>" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" value='<?php echo isset($countrybanner['id'])?$countrybanner['id']:"" ?>' name='id'/>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2 require"><?php echo __t('Country'); ?></label>
                <div class="col-lg-6">
                    <select placeholder="" id="country_id" name="country_id" class="form-control" required="">
                    <option value=""><?php echo __t(" -- Select Country -- "); ?></option>
                    <?php
                    if (isset($countries) && !empty($countries)) {
                        foreach ($countries as $k=>$v) {
                            $selected = ($countrybanner['country_id'] == $k) ? 'selected': '';
                            echo "<option value='" . $k . "'  $selected >" . $v . "</option>";
                        }
                    }
                    ?>
                     </select> 
                </div>
            </div>
             <div class="form-group">
                <label for="city_id" class="col-lg-2 control-label"><?php echo __t("City"); ?></label>
                <div class="col-lg-6">
                <select class="form-control" id="city_id" name="city_id">
                    <option value=''><?php __t("-- Select City -- "); ?></option>
                    <?php
                    if (isset($cities) && !empty($cities)) {
                        foreach ($cities as $city) {
                            $selected = ($countrybanner['city_id'] == $city['city_id']) ? 'selected': '';
                            echo "<option value='" . $city['city_id'] . "'  $selected >" . $city['city_name'] . "</option>";
                        }
                    }
                    ?>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><?php echo __t("Category"); ?></label>
                <div class="col-lg-6">
                    <select class="form-control" name="category_id" id="category_id">
                        <option value=""><?php echo __t(" -- Select Category -- "); ?></option>
                        <?php
                        if (isset($categories) && !empty($categories)) {
                            foreach ($categories as $category) {
                                $selected = ($countrybanner['category_id'] == $category['id']) ? 'selected': '';
                                echo "<option $selected value='" . $category['id'] . "'>" . $category['category_name'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><?php echo __t("Top Attraction"); ?></label>
                <div class="col-lg-6">
                    <select class="form-control" name="attraction_id" id="attraction_id">
                        <option value=""><?php echo __t(" -- Select Top Attractions -- "); ?></option>
                        <?php
                        if (isset($attractions) && !empty($attractions)) {
                            foreach ($attractions as $attraction) {
                                $selected = (isset($countrybanner['attraction_id']) && $attraction['id'] == $countrybanner['attraction_id']) ? "selected" : "";
                                echo "<option $selected  value='" . $attraction['id'] . "'>" . $attraction['attraction_name'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Description'); ?></label>
                <div class="col-lg-6">
                    <textarea class="form-control" name="description"><?php echo (isset($countrybanner['description']))?$countrybanner['description'] : ""; ?></textarea>
                </div>
            </div> 
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Meta Title'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="50" id="meta_title" name="meta_title" type="text" value="<?php echo (isset($countrybanner['meta_title'])) ? $countrybanner['meta_title'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Meta Description'); ?></label>
                <div class="col-lg-6">
                    <textarea class="form-control" name="meta_description"><?php echo (isset($countrybanner['meta_description'])) ? $countrybanner['meta_description'] : ""; ?></textarea>
                </div>
            </div>
            <div class="form-group ">
                <label for="firstname" class="control-label col-lg-2"><?php echo __t('Meta Keywords'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="50" id="meta_title" name="meta_keywords" type="text" value="<?php echo (isset($countrybanner['meta_keywords'])) ? $countrybanner['meta_keywords'] : ""; ?>" />
                </div>
            </div>
             <div class="form-group">
                <label for="status" class="col-lg-2 control-label require"><?php echo __t("Status"); ?></label>
                <div class="col-lg-6">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" id="optionsRadios1" name="status" id="status_active" <?php echo (isset($countrybanner['status']) && $countrybanner['status'] == 1 ) ? "checked" : ""; ?>>
                            <?php __t("Active"); ?>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" id="optionsRadios1" name="status" id="status_inactive" <?php echo (isset($countrybanner['status']) && $countrybanner['status'] == 0 ) ? "checked" : ""; ?>>
                            <?php __t("Inactive"); ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group ">
                <label for="email" class="control-label col-lg-2 require"><?php echo __t('Image'); ?></label>
                <?php if (isset($countrybanner['banner']) && !empty($countrybanner['banner'])) { ?>
                <div class="col-lg-3">
                        <div data-provides="fileupload" class="fileupload fileupload-exists"><input type="hidden" value="" name="">
                            <div style="max-width: 200px; max-height: 150px; line-height: 10px;" class="image_name_preview fileupload-preview fileupload-exists thumbnail">
                                <img class="<?php echo isset($countrybanner['id'])?$countrybanner['id']:""; ?>" src="<?php echo base_url() . BANNER_IMAGE_PATH . $countrybanner['banner']; ?>" alt="" />
                            </div>
                            <div>
                                <span class="btn btn-white btn-file">
                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Select image</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input type="file" data-flag="1" id="image_name" name="image_name" class="default image_name categoryImageFile" >
                                </span>
                               <a data-dismiss="fileupload" data-url='<?php echo base_url().'countrybanner/delete_image' ?>' data-name="<?php echo $countrybanner['banner'] ; ?>" data-id="<?php echo isset($countrybanner['id'])?$countrybanner['id']:""; ?>" class="btn btn-danger fileupload-exists removebtn removeImage" ><i class="fa fa-trash"></i>Remove</a>
                            </div>
                        </div>
                    </div>
                    
                <?php } else { ?>
                    <div class="col-lg-3">
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?php echo base_url().COMMON_IMAGE_PATH.'noimage.png' ?>" alt="" />
                            </div>

                            <div class="fileupload-preview fileupload-exists thumbnail image_name_preview" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-white btn-file">
                                    <span class="fileupload-new"><i class="fa fa-paper-clip"></i><?php echo __t("Select Image"); ?></span>
                                    <span class="fileupload-exists"><?php echo __t("Change"); ?></span>
                                    <input type="file" class="default image_name categoryImageFile" name="image_name" id="image_name" value="<?php echo (isset($countrybanner['banner'])) ? $countrybanner['banner'] : ""; ?>"/>
                                </span>
                                <!--<a href="javascript:void(0);" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i><?php echo __t("Remove"); ?></a>-->
                            </div>
                            <span class="note_css">Note: Please upload image with minimum image dimension 2000 X 833</span>
                        </div>
                    </div>
                <?php } ?>
            </div>
                
           
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button class="btn btn-primary" type="submit"><?php echo __t('Save'); ?></button>
                    <a class="btn btn-default" href="<?php echo $this->config->item('base_url'); ?>countrybanner"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        $('.wysihtml5').wysihtml5();
    });
</script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>assets/third-party/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>assets/third-party/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<!-- banner end-->
