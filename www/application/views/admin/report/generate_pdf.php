<?php
    $currencySelected = (isset($currencySelected['currency_code']) && !empty($currencySelected['currency_code'])) ? '(' . $currencySelected['currency_code'] . ')' : '';
?>
<table border="0" cellspacing="0" cellpadding="7" align="center">
  <tr>
    <td style="border-bottom:4px solid #0053B7;"><img src="<?php echo base_url(); ?>assets/frontend/images/logo.jpg" width="150" alt="" /></td>
    <td style="text-align:right; border-bottom:4px solid #0053B7;" colspan="2"><br/><h1>Commission Report</h1></td>
  </tr>
  <tr>
    <td><p style="text-align:left;"><strong>Date: </strong><?php echo date('d M Y'); ?></p></td>
    <td colspan="2"></td>
  </tr>
</table>
<table id='reportData' border="1" cellspacing="0" cellpadding="5" class='table table-bordered table-striped table-condensed'>
<thead><tr>
<th data-type="string">Date</th>
<th data-type="string">Booking Reference</th>
<th data-type="string">Product Title</th>
<th data-type="string" align="right" style="text-align:right;">Sales Amount <?php echo $currencySelected ; ?></th>
<th data-type="string" align="right" style="text-align:right;">Sales Amount After Special Discount <?php echo $currencySelected;?></th>
<th data-type="string" align="right" style="text-align:right;">Commission Amount</th>
<?php if (isset($role_id) && $role_id != 3) { ?>
    <th data-type="string" align="right" style="text-align:right;">Total Payable Amount</th>
<?php } else { ?>
    <th data-type="string" align="right" style="text-align:right;">Total Receivable Amount</th>
<?php } ?>
</tr></thead>
<?php
$saleAmtTotal = $saleAmtSpeDicTotal = $commiAmtTotal = $payableAmtTotal = 0;

echo "<tbody>";
foreach ($reportData as $repdata) {
    $json_data = json_decode($repdata['json_data'], true);
    //pr($json_data,false);
    echo "<tr>";
    echo "<td>" . date('d-m-Y', strtotime($repdata['date_booking'])) . "</td>";
    echo "<td>" . $repdata['reference_code'] . "</td>";
    echo "<td>" . $json_data['product']['product_name'] . "</td>";
    if (isset($json_data['specialOffer'])) {
        echo '<td align="right">' . ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']) . "</td>";
        echo '<td align="right">' . (ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) . "</td>";
        $saleAmtTotal += ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']);
        $saleAmtSpeDicTotal += (ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] );
    } else {
        echo '<td align="right">' . ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']) . "</td>";
        echo '<td align="right">' . ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']) . "</td>";
        $saleAmtTotal += ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']);
        $saleAmtSpeDicTotal += ceil($json_data['supplierPrice'] + $json_data['supplierPriceAddons']);
    }
    if (isset($repdata['commission_rate']) && !empty($repdata['commission_rate'])) {
        echo '<td align="right">' . ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100) . "</td>";
        echo '<td align="right">' . ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) - ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100)) . "</td>";
        $commiAmtTotal += ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100);
        $payableAmtTotal += ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) - ((ceil($json_data['supplierPrice'] - ($json_data['supplierPrice'] * $json_data['specialOffer']['percent'] / 100)) + $json_data['supplierPriceAddons'] ) * $repdata['commission_rate']['commission'] / 100));
    } else {
        echo "<td align='right'>0</td>";
        echo '<td align="right">' . $json_data['supplierPrice'] . "</td>";
        $payableAmtTotal += $json_data['supplierPrice'];
    }
    echo "</tr>";
}
?>
<tfoot>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td style="text-align: right;">Total</td>
<td style="text-align: right;"><?php echo $saleAmtTotal; ?></td>
<td style="text-align: right;"><?php echo $saleAmtSpeDicTotal; ?></td>
<td style="text-align: right;"><?php echo $commiAmtTotal; ?></td>
<td style="text-align: right;"><?php echo $payableAmtTotal; ?></td>
</tr>
</tfoot>
</tbody>
</table>
