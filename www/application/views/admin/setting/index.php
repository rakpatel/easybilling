<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Setting'); ?> 
        </span>
    </header>
    <div class="panel-body" style="min-height: 400px">
        <section class="panel">
            <div class="panel-body">
                <div class="tab-content step">
                    <div class="tab-pane active" id="general">
                        <form class="cmxform form-horizontal" id="general_settings" name="general_settings" method="post" action="" enctype="multipart/form-data">
                            <?php
                                if(!empty($arrSetting))
                                {
                                    foreach ($arrSetting as $key => $setting)
                                    {
                            ?>
                                <div class="form-group ">
                                    <label for="<?php echo $setting['name'];?>" class="control-label col-lg-2 require"><?php echo __t($setting['name']); ?></label>
                                    <div class="col-lg-12">
                                        <textarea class=" form-control" id="<?php echo $setting['name'];?>" name="<?php echo $setting['name'];?>" type="text"><?php echo (isset($setting['value']) && !empty($setting['value'])) ? $setting['value']:""; ?></textarea>
                                    </div>
                                </div>
                            <?php
                                    }
                                }
                            ?>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-6">
                                    <button class="btn btn-primary" type="submit"><?php echo __t('Save'); ?></button>
                                    <a class="btn btn-default" href="<?php echo __gurl('sales/create');?>"><?php echo __t('Cancel'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>