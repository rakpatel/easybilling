<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Database'); ?> 
        </span>
    </header>
    <div class="panel-body" style="min-height: 400px">
        <section class="panel">
            <div class="panel-body">
                <div class="tab-content step">
                    <div class="tab-pane active" id="general">
                        <form class="cmxform form-horizontal" id="general_settings" name="general_settings" method="post" action="" enctype="multipart/form-data">
                            <div class="form-group ">
                                    <label class="control-label col-lg-2 require">Query</label>
                                    <div class="col-lg-12">
                                        <textarea class=" form-control" id="query" name="query" type="text"></textarea>
                                    </div>
                                </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-6">
                                    <button class="btn btn-primary" type="submit"><?php echo __t('Run'); ?></button>
                                    <a class="btn btn-default" href="<?php echo __gurl('sales/create');?>"><?php echo __t('Cancel'); ?></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>