<!-- page start-->
<div class="row">
    <?php echo $this->load->view('admin/common/profile_sidebar', $user_data, true); ?>
    <aside class="profile-info col-lg-9">
        <section>
            <div class="panel panel-primary">
                <div class="panel-heading"> Sets New Password</div>
                <div class="panel-body">
                    <form class=" cmxform form-horizontal" role="form" id="ChangePasswordForm" method="post">
                        <div class="form-group">
                            <label  class="col-lg-2 control-label">New Password</label>
                            <div class="col-lg-6">
                                <input type="password" class="form-control" id="password" name="password" placeholder=" ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-lg-2 control-label">Re-type New Password</label>
                            <div class="col-lg-6">
                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder=" ">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-success  pull-right">Save</button>
                                <!--
                                <button type="button" class="btn btn-default  pull-right" style="margin-right: 20px;">Cancel</button>
                                -->
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </aside>
</div>