<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
                if ($id == 0) echo __t('Add Customer');
                else echo __t('Edit Customer');
                $formId = "CustomerForm";
            ?>
        </span>
    </header>
    <div class="panel-body">
        <div class="stepy-tab">
            <ul id="default-titles" class="stepy-titles clearfix">

            </ul>
        </div>
        <form class="cmxform form-horizontal" id="UserForm" method="post" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $id; ?>" name="id" id="id"/>
            <input type="hidden" value="<?php echo $role_id; ?>" name="role_id" id="role_id"/>
            <!--Basic-->
            <fieldset title="Basic" class="step" id="default-step-0" >
                <legend><?php echo __t("Basic Information"); ?> </legend>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Title"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="name" id="name" value="<?php echo (isset($user['name']) && !empty($user['name'])) ? $user['name'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Username"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="username" id="username" value="<?php echo (isset($user['username']) && !empty($user['username'])) ? $user['username'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Password"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="password" id="password" value="<?php echo (isset($user['password']) && !empty($user['password'])) ? $user['username'] : ""; ?>">
                    </div>
                </div>
            </fieldset>
            <div style="clear:both;"></div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <input type="submit" class="finish btn btn-danger" value="Save"/>
                    <a class="btn btn-default" href="<?php echo __gurl('user/index_'.lcfirst($user_label));?>"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    var base_url = 'http://127.0.0.1:<?php echo PORT;?>/index.php/';
</script>