
<div class="AdminLogo"></div>
<form class="form-signin" method="post" id="signupForm">
  <h2 class="form-signin-heading"><?php echo __t('sign in now'); ?></h2>
  <div class="login-wrap">
    <?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-block alert-danger fade in">
      <button type="button" class="close close-sm" data-dismiss="alert"> <i class="fa fa-times"></i> </button>
      <?php echo $this->session->flashdata('error'); ?> </div>
    <?php } ?>
    <div class="form-group">
      <label for="username">Username</label>
      <input type="text" class="form-control" maxlength="50" placeholder="Enter your username" name="username" id="username"  value="<?php if(isset($data['username']) && !empty($data['username'])) { echo $data['username'] ; } ?>">
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" maxlength="50" placeholder="Enter your password" name="password" id="password" value="<?php if(isset($data['password']) && !empty($data['password'])) { echo $data['password'] ; } ?>">
    </div>
<!--    <label class="checkbox"> 
      <span class="pull-right"> <a data-toggle="modal" href="#myModal"> <?php echo __t('Forgot Password?'); ?></a> </span> </label>-->
    <button class="btn btn-lg btn-login btn-block" type="submit"><?php echo __t('Sign in'); ?></button>
  </div>
<style type="text/css">
  .form-signin input[type="text"], .form-signin input[type="password"] { margin-bottom: 5px;}
</style>
</form>

<!-- Modal -->
<!--<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo __t('Forgot Password?'); ?></h4>
      </div>
      <div class="forgetmessage"></div>
      <form id='forgetPassword' method="POST" autocomplete="off">
        <div class="modal-body forgetPassword">
          <p><?php echo __t('Enter your e-mail address below to reset your password.'); ?></p>
          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
        </div>
        <div class="modal-footer">
          <button data-dismiss="modal" class="btn btn-default" type="button"><?php echo __t('Cancel'); ?></button>
          <button class="btn btn-success forgetPassword" type="submit"><?php echo __t('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>-->
<!-- modal --> 
