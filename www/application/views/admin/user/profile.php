<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
                echo __t('Edit Profile');
                $formId = "ProfileForm";
            ?>
        </span>
    </header>
    <div class="panel-body">
        <form class="cmxform form-horizontal" id="<?php echo $formId;?>" method="post" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo (isset($user_data['id']) && !empty($user_data['id'])) ? $user_data['id'] : ""; ?>" name="id" id="id"/>
            <!--Basic-->
            <fieldset title="Basic" class="step" id="default-step-0" >
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Name"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="name" id="name" value="<?php echo (isset($user_data['name']) && !empty($user_data['name'])) ? $user_data['name'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Username"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="username" id="username" value="<?php echo (isset($user_data['username']) && !empty($user_data['username'])) ? $user_data['username'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Password"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="password" id="password" value="">
                    </div>
                </div>
            </fieldset>
            <div style="clear:both;"></div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <input type="submit" class="finish btn btn-danger" value="Save"/>
                </div>
            </div>
        </form>
    </div>
</section>
<style>
    #CompanyForm.cmxform {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: -moz-use-text-color #7fba00 #7fba00;
        border-image: none;
        border-style: none solid solid;
        border-width: 0 1px 1px;
        padding: 10px;
    }
</style>