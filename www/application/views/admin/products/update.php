<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
                if ($id == 0) echo __t('Add Product');
                else echo __t('Edit Product');
                $formId = "ProductForm";
            ?>
        </span>
    </header>
    <div class="panel-body">
        <div class="stepy-tab">
            <ul id="default-titles" class="stepy-titles clearfix">

            </ul>
        </div>
        <form class="cmxform form-horizontal" id="ProductForm" method="post" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $id; ?>" name="id" id="id"/>
            <!--Basic-->
            <fieldset title="Basic" class="step" id="default-step-0" >
                <legend><?php echo __t("Basic Information"); ?> </legend>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Title"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="p_name" id="p_name" value="<?php echo (isset($product['p_name']) && !empty($product['p_name'])) ? $product['p_name'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Reference Code"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="p_code" id="p_code" value="<?php echo (isset($product['p_code']) && !empty($product['p_code'])) ? $product['p_code'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("Manufacturer"); ?></label>
                    <div class="col-lg-6">
                        <select name="manufacturer_id" id="manufacturer_id" class="form-control">
                            <option value="">-- Select Manufacturer --</option>
                            <?php foreach ($arrManu as $keym => $man){ ?>
                            <option value="<?php echo $man['id'];?>" <?php echo (isset($product['manufacturer_id']) && $product['manufacturer_id']=$man['id']) ? "selected" : ""; ?>><?php echo $man['name'];?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("Product Type"); ?></label>
                    <div class="col-lg-6">
                        <select name="type_id" id="type_id" class="form-control">
                            <option value="">-- Select Type --</option>
                            <?php foreach ($arrTypes as $keyt => $type){ ?>
                            <option value="<?php echo $type['id'];?>" <?php echo (isset($product['type_id']) && $product['type_id'] == $type['id']) ? "selected" : ""; ?>><?php echo $type['name'];?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("Product Unit"); ?></label>
                    <div class="col-lg-6">
                        <select name="unit_id" id="unit_id" class="form-control">
                            <option value="">-- Select Unit --</option>
                            <?php foreach ($arrUnits as $keyu => $unit){ ?>
                            <option value="<?php echo $unit['id'];?>" <?php echo (isset($product['unit_id']) && $product['unit_id'] == $unit['id']) ? "selected" : ""; ?>><?php echo $unit['name'];?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("No. of QTY"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="no_of_qty" id="no_of_qty" value="<?php echo (isset($product['no_of_qty'])) ? $product['no_of_qty'] : ""; ?>">
                    </div>
                </div>
            </fieldset>
            <div style="clear:both;"></div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <input type="submit" class="finish btn btn-danger" value="Save"/>
                    <a class="btn btn-default" href="<?php echo __gurl('products/index');?>"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    var base_url = 'http://127.0.0.1:<?php echo PORT;?>/index.php/';
</script>