<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
            if ($id == 0) {
                echo __t('Add Email Template');
                $formId = "EmaiTemplateForm";
            } else {
                echo __t('Edit Email Template');
                $formId = "EmaiTemplateForm";
            }
            ?>
        </span>
    </header>
    <div class="panel-body">
        <form class="cmxform form-horizontal" id="<?php echo $formId; ?>" method="post" action="">
            <div class="form-group ">
                <label for="email_name" class="control-label col-lg-2 require"><?php echo __t('Email Name'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="200" id="email_name" name="email_name" type="text" value="<?php echo (isset($email['email_name'])) ? $email['email_name'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="website_area" class="col-lg-2 control-label require"><?php echo __t("Website Area"); ?></label>
                <div class="col-lg-6">
                <select class="form-control" id="website_area" name="website_area" >
                    <option value=''><?php __t("-- Select Area -- "); ?></option>
                    <option value='Admin Reviews' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Admin Reviews')?"selected":""?>><?php __t("Admin Reviews"); ?></option>
                    <option value='Admin Manage Bookings' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Admin Manage Bookings')?"selected":""?>><?php __t("Admin Manage Bookings"); ?></option>
                    <option value='Admin Manage Suppliers' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Admin Manage Suppliers')?"selected":""?>><?php __t("Admin Manage Suppliers"); ?></option>
                    <option value='Admin Manage Products' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Admin Manage Products')?"selected":""?>><?php __t("Admin Manage Products"); ?></option>
                    <option value='Create New Product' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Create New Product')?"selected":""?>><?php __t("Create New Product"); ?></option>
                    <option value='Customer Reviews' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Customer Reviews')?"selected":""?>><?php __t("Customer Reviews"); ?></option>
                    <option value='Manage Bookings' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Manage Bookings')?"selected":""?>><?php __t("Manage Bookings"); ?></option>
                    <option value='Notifications' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Notifications')?"selected":""?>><?php __t("Notifications"); ?></option>
                    <option value='Product Purchase' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Product Purchase')?"selected":""?>><?php __t("Product Purchase"); ?></option>
                    <option value='Product Details' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Product Details')?"selected":""?>><?php __t("Product Details"); ?></option>
                    <option value='Request Changes' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Request Changes')?"selected":""?>><?php __t("Request Changes"); ?></option>
                    <option value='Supplier Administration' <?php echo (isset($email['website_area']) && $email['website_area'] == 'Supplier Administration')?"selected":""?>><?php __t("Supplier Administration"); ?></option>
                    <option value='User Login' <?php echo (isset($email['website_area']) && $email['website_area'] == 'User Login')?"selected":""?>><?php __t("User Login"); ?></option>
                </select>
                </div>
            </div>
            <div class="form-group ">
                <label for="email_subject" class="control-label col-lg-2 require"><?php echo __t('Email Subject'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="200" id="email_subject" name="email_subject" type="text" value="<?php echo (isset($email['email_subject'])) ? $email['email_subject'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label require"><?php echo __t("Email Template"); ?></label>
                <div class="col-lg-6">
                    <textarea class="form-control" name="email_template" id="email_template"><?php echo (isset($email['email_template'])) ? $email['email_template'] : ""; ?></textarea>
                </div>
            </div>
            <div class="form-group ">
                <label for="function" class="control-label col-lg-2 require"><?php echo __t('Function'); ?></label>
                <div class="col-lg-6">
                    <input class=" form-control" maxlength="200" id="function" name="function" type="text" value="<?php echo (isset($email['function'])) ? $email['function'] : ""; ?>" />
                </div>
            </div>
            <div class="form-group ">
                <label for="is_active" class="control-label col-lg-2"><?php echo __t('Is Active'); ?></label>
                <div class="col-lg-1">
                    <label class="checkbox">
                        <input class="checkbox form-control chkClass " id="is_display" name="is_active" type="checkbox" value="1" <?php echo (isset($email['is_active']) && $email['is_active'] == 1 ) ? "checked" : ""; ?>/>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <button class="btn btn-primary" type="submit"><?php echo __t('Save'); ?></button>
                    <a class="btn btn-default" href="<?php echo $this->config->item('base_url'); ?>system_email"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>assets/third-party/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'email_template', {
        filebrowserBrowseUrl : base_url+'assets/third-party/ckeditor/filemanager/browser/default/browser.html?Connector='+base_url+'/assets/third-party/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl : base_url+'assets/third-party/ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector='+base_url+'/assets/third-party/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl : base_url+'assets/third-party/ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector='+base_url+'/assets/third-party/ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl  : base_url+'assets/third-party/ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl : base_url+'assets/third-party/ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl : base_url+'assets/third-party/ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });
    //CKEDITOR.config.autoParagraph = false;
</script>
<!-- page end-->
