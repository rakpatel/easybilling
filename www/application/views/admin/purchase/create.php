<link rel="stylesheet" type="text/css" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/advanced-datatable/media/css/jquery.dataTables.css">
<link rel="stylesheet" href="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/jquery-ui/jquery-ui-1.11.4.custom.min.css">
<script type="text/javascript" language="javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/advanced-datatable/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="http://127.0.0.1:<?php echo PORT;?>/assets/third-party/jquery-ui/jquery-ui-1.11.4.custom.min.js"></script>

<style>
    .fw-container{min-height: 490px; }
    .form-group{margin-bottom:5px;}
    .panel-body{padding-bottom: 0px;}
    .input-width {width:70px !important;}
    .input-half-width {width:60px !important;}
</style>
<?php
    $today_date = date('d-m-Y');
?>
<!-- page start-->
<section class="panel">
    <div class="panel-body">
        <!--Basic-->
        <fieldset title="Basic" class="step" id="default-step-0" >
            <div class="form-group">
                <div class="col-lg-3">
                    <input type="text"  maxlength="200" class="form-control" name="bill_no" id="bill_no" placeholder="Purchase No">
                </div>
                <div class="col-lg-3">
                    <input type="text"  maxlength="200" class="form-control" name="company_name" id="company_name" placeholder="Party Name">
                    <input type="hidden"  maxlength="200" class="form-control" name="c_id" id="c_id">
                </div>
                <div class="col-lg-3">
                    <input type="text"  maxlength="200" class="form-control default-date-picker dpd1" placeholder="Purchase Date" name="p_date" id="p_date" value="<?php echo $today_date;?>">
                </div>
            </div>
        </fieldset>
        <div style="clear:both;"></div>
        <div class="fw-container">
            <div class="fw-body">
                <div class="content" style="min-height:150px;">
                            <table id="PurchaseTable" class="display" cellspacing="0" width="100%">
                                    <thead>
                                            <tr>
                                                <th style="width:250px !important;">Particular</th>
                                                <th class="input-half-width">Pack</th>
                                                <th class="input-width">Mfg.</th>
                                                <th class="input-width">Batch</th>
                                                <th class="input-width">Exp.</th>
                                                <th class="input-half-width">QTY.</th>
                                                <th class="input-width">MRP</th>
                                                <th class="input-width">Rate</th>
                                                <th class="input-half-width">Vat</th>
                                                <th class="input-half-width">Add</th>
                                                <th class="input-width">Net</th>
                                                <th class="input-width">Amount</th>
                                            </tr>
                                    </thead>
                                    <tbody class="scrollContent">
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="id" id="id">
                                                    <input style="width:250px !important;" type="text" name="p_name" id="p_name" placeholder="Product">
                                                </td>
                                                <td><input class="input-half-width" type="text" name="pack" id="pack" placeholder="Pack"></td>
                                                <td><input class="input-width" type="text" name="mfg" id="mfg" placeholder="MFG"></td>
                                                <td><input class="input-width" type="text" name="batch" id="batch" placeholder="Batch"></td>
                                                <td><input class="default-date-picker dpd1 input-width" type="text" name="exp" id="exp" value="<?php echo $today_date?>"></td>
                                                <td><input class="input-half-width" type="text" name="qty" id="qty" placeholder="QTY"></td>
                                                <td><input class="input-width" type="text" name="mrp" id="mrp" placeholder="MRP"></td>
                                                <td><input class="input-width" type="text" name="rate" id="rate" placeholder="Rate"></td>
                                                <td><input class="input-half-width" type="text" name="vat" id="vat" placeholder="Vat"></td>
                                                <td><input class="input-half-width" type="text" name="add" id="add" placeholder="Add"></td>
                                                <td><input class="input-width" type="text" name="net" id="net" placeholder="Net"></td>
                                                <td><input class="input-width" type="text" name="amount" id="amount" placeholder="Amount"></td>
                                            </tr>
                                    </tbody>
                            </table>
                    </div>
            </div>
            <div class="form-group" style="float:right !important;">
                <div style="margin-top:10px;">
                    <input style="margin-right: 17px;width: 100px;text-align: right;" type="text" name="total" id="total">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <button class="btn btn-default" id="save">Save</button>
                <button class="btn btn-default" id="cancle">Cancel</button>
                <button class="btn btn-default" id="remove">Remove</button>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" class="init">
var today_date = '<?php echo $today_date;?>';
var table;
$(document).ready(function(){
    table = $('#PurchaseTable').DataTable({
            "bPaginate": false,
            "bFilter": false,
            "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,3,4,5,6,7,8,9,10,11 ] }],
            "aaSorting": [],
            "bDestroy": true,
            "bInfo": false 
    });
    $('#PurchaseTable').on( 'click', 'tr:gt(1)', function () {
            if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
            }
            else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
            }
    } );
    $('#remove').click( function () {
        var product_cart_remove_url = '<?php echo __gurl("purchase/removeProduct");?>';
        var remove_index = $('#PurchaseTable tbody .selected').find("td:first")[0].lastElementChild.defaultValue;
        var arrProduct = {index : remove_index};
        
        $.ajax({
                type: "POST",
                url: product_cart_remove_url,
                data: arrProduct,
                success:function(data){
                    var arrVal = data.split('~~~');
                    $('#PurchaseTable tr:gt(1)').empty();
                    $('#PurchaseTable tbody tr:last').after(arrVal[1]);
                    $( "#total" ).val(Math.round(arrVal[0],2));
                }
        });
    } );
    
    $('#cancle').click( function () {
        var product_cart_cancle_url = '<?php echo __gurl("purchase/cancleOrder");?>';
        $.ajax({
                type: "POST",
                url: product_cart_cancle_url,
                data: {},
                success:function(data){
                    $("#PurchaseTable tr:gt(1)").detach();
                    $( "#total" ).val('');
                }
        });
    } );

    $('#save').click( function () {
        if($( "#bill_no" ).val() == '')
        {
            showAlert('Please enter purchase number', 'Purchase Invoice');
            $( "#bill_no" ).focus();
            return false;
        }
        else if($( "#company_name" ).val() == '') 
        {
            showAlert('Please select company', 'Purchase Invoice');
            $( "#company_name" ).focus();
            return false;
        }

        var order_url = '<?php echo __gurl("purchase/placeOrder");?>';
        var arrPurchase = {purchase_invoice_id : $( "#bill_no" ).val(), c_id:$( "#c_id" ).val(), purchase_date:$( "#p_date" ).val()};
        $.ajax({
                type: "POST",
                url: order_url,
                data: arrPurchase,
                success:function(data){
                    $("#PurchaseTable tr:gt(1)").detach();
                    $( "#bill_no" ).val('');
                    $( "#total" ).val('');
                    $( "#company_name" ).val('');
                }
        });
    } );
    
    var dataProduct = <?php echo $arrProJson;?>;
    $( "#p_name" ).autocomplete({
      source: dataProduct,
      change: function (event, ui) { 
        var product_url = '<?php echo __gurl("purchase/getProduct");?>';
        var arrP = (this.value).split('_');
        var p_id = arrP[1];
        $( "#p_name" ).val(arrP[0]);
        $( "#id" ).val(arrP[1]);
        $.ajax({
                type: "POST",
                url: product_url,
                data: {id:p_id},
                success:function(data){
                    data = jQuery.parseJSON(data);
                    $( "#id" ).val(data.id);
                    $( "#p_name" ).val(data.p_name);
                    $( "#pack" ).val(data.unit_name);
                    $( "#mfg" ).val(data.short_name);
                }
        });
      }
    });
    
    var dataStockist = <?php echo $arrStoJson;?>;
    $( "#company_name" ).autocomplete({
      source: dataStockist,
      change: function (event, ui) { 
        var arrS = (this.value).split('_');
        $( "#company_name" ).val(arrS[0]);
        $( "#c_id" ).val(arrS[1]);
      }
    });
    
    $( "#rate" ).on('change',function(){
        var rate = parseFloat(this.value);
        $( "#net" ).val(rate.toFixed(2));
        if($( "#qty" ).val() != '')
        {
            var qty = parseFloat($( "#qty" ).val());
            var amount = parseFloat(rate * qty);
            $( "#amount" ).val(amount.toFixed(2));
        }
    });

    $( "#vat" ).on('change',function(){
        var vat = parseFloat(this.value);
        var rate = parseFloat($( "#rate" ).val());
        var vat_amount = parseFloat((rate*vat)/100);
        var net = parseFloat(vat_amount + rate);
        if($( "#qty" ).val() != '')
        {
            var qty = parseFloat($( "#qty" ).val());
            var amount = parseFloat(net * qty);
            $( "#net" ).val(net.toFixed(2))
            $( "#amount" ).val(amount.toFixed(2));
        }
    });

    $( "#add" ).on('change',function(){
        var rate = parseFloat($( "#rate" ).val());
        var vat = parseFloat($( "#vat" ).val());
        var vat_amount = parseFloat((rate*vat)/100);
        var add = parseFloat(this.value);
        var add_amount = parseFloat((rate*add)/100);
        var net = parseFloat(vat_amount + add_amount + rate);
        if($( "#qty" ).val() != '')
        {
            var qty = parseFloat($( "#qty" ).val());
            var amount = parseFloat(net * qty);
            $( "#net" ).val(net.toFixed(2))
            $( "#amount" ).val(amount.toFixed(2));
        }
    });
    
    $( "#net" ).on('change',function(){
        var net = parseFloat(this.value);
        if($( "#qty" ).val() != '')
        {
            var qty = parseFloat($( "#qty" ).val());
            var amount = parseFloat(net * qty);
            $( "#amount" ).val(amount.toFixed(2));
        }
    });

    $( "#qty" ).on('change',function(){
        var qty = parseFloat(this.value);
        if($( "#net" ).val() != '')
        {
            var net = parseFloat($( "#net" ).val());
            var amount = parseFloat(net * qty);
            $( "#amount" ).val(amount.toFixed(2));
        }
    });

    $( "#amount" ).on('keydown',function(){
        
        if($( "#id" ).val() == '')
        {
            showAlert('Please select product', 'Purchase Invoice');
            $( "#p_name" ).focus();
            return false;
        }
        else if($( "#qty" ).val() == '') 
        {
            showAlert('Please enter quantity', 'Purchase Invoice');
            $( "#qty" ).focus();
            return false;
        }
        else if($( "#mrp" ).val() == '') 
        {
            showAlert('Please enter MRP', 'Purchase Invoice');
            $( "#mrp" ).focus();
            return false;
        }
        else if($( "#rate" ).val() == '') 
        {
            showAlert('Please enter rate', 'Purchase Invoice');
            $( "#rate" ).focus();
            return false;
        }
        else if($( "#amount" ).val() == '') 
        {
            showAlert('Please provide proper details to store this product purchase', 'Purchase Invoice');
            $( "#qty" ).focus();
            return false;
        }

        var product_cart_url = '<?php echo __gurl("purchase/addProduct");?>';
        var arrProduct = {id:$( "#id" ).val(), p_name:$( "#p_name" ).val(), qty:$( "#qty" ).val(), amount:$( "#amount" ).val(), batch:$( "#batch" ).val(), exp:$( "#exp" ).val(), rate:$( "#rate" ).val(), net:$( "#net" ).val(), pack:$( "#pack" ).val(), mfg:$( "#mfg" ).val(), vat:$( "#vat" ).val(), add:$( "#add" ).val(), mrp:$( "#mrp" ).val()};
        $.ajax({
                type: "POST",
                url: product_cart_url,
                data: arrProduct,
                success:function(data){
                    data = jQuery.parseJSON(data);
                    $("#PurchaseTable tr:last").after(data.html);
                    $( "#total" ).val(data.cart_total.toFixed(2));
                }
        });
        $( "#id" ).val('');
        $( "#p_name" ).val('');
        $( "#rate" ).val('');
        $( "#pack" ).val('');
        $( "#mfg" ).val('');
        $( "#qty" ).val('')
        $( "#amount" ).val('')
        $( "#batch" ).val('');
        $( "#exp" ).val(today_date);
        $( "#net" ).val('');
        $( "#vat" ).val('');
        $( "#add" ).val('');
        $( "#mrp" ).val('');
        $( "#p_name" ).focus();
    });

});
</script>