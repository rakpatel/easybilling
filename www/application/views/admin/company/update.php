<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
                if ($id == 0) echo __t('Add Company');
                else echo __t('Edit Company');
                $formId = "CompanyForm";
            ?>
        </span>
    </header>
    <div class="panel-body">
        <div class="stepy-tab">
            <ul id="default-titles" class="stepy-titles clearfix">

            </ul>
        </div>
        <form class="cmxform form-horizontal" id="<?php echo $formId;?>" method="post" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $id; ?>" name="id" id="id"/>
            <!--Basic-->
            <fieldset title="Basic" class="step" id="default-step-0" >
                <legend><?php echo __t("Basic Information"); ?> </legend>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Name"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="name" id="name" value="<?php echo (isset($company['name']) && !empty($company['name'])) ? $company['name'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Short Name"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="short_name" id="short_name" value="<?php echo (isset($company['short_name']) && !empty($company['short_name'])) ? $company['short_name'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("Number"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="number" id="number" value="<?php echo (isset($company['number']) && !empty($company['number'])) ? $company['number'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("Address"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="address" id="address" value="<?php echo (isset($company['address']) && !empty($company['address'])) ? $company['address'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("CIN No."); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="cin_num" id="cin_num" value="<?php echo (isset($company['cin_num']) && !empty($company['cin_num'])) ? $company['cin_num'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("TIN No."); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="tin_num" id="tin_num" value="<?php echo (isset($company['tin_num']) && !empty($company['tin_num'])) ? $company['tin_num'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("Is stockist"); ?></label>
                    <div class="col-lg-6">
                        <input type="checkbox" name="is_stockist" id="is_stockist" <?php echo (isset($company['is_stockist']) && $company['is_stockist'] == '1') ? "checked=checked" : ""; ?>>
                    </div>
                </div>
            </fieldset>
            <div style="clear:both;"></div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <input type="submit" class="finish btn btn-danger" value="Save"/>
                    <a class="btn btn-default" href="<?php echo __gurl('company/index');?>"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<style>
    #CompanyForm.cmxform {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: -moz-use-text-color #7fba00 #7fba00;
        border-image: none;
        border-style: none solid solid;
        border-width: 0 1px 1px;
        padding: 10px;
    }
</style>