<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Batch'); ?> 
            <a href="<?php echo __gurl('batch/update/0'); ?>" class="btn label label-primary" style="float: right;"><?php echo __t('Add Batch'); ?></a>
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <?php echo $this->load->view('admin/common/index_listing_product', $dataTableObject, true); ?>
        </div>
    </div>
</section>
<!-- page end-->
