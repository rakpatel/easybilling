<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php echo __t('Manage Types'); ?> 
            <a href="<?php echo __gurl('type/update/0'); ?>" class="btn label label-primary" style="float: right;"><?php echo __t('Add Type'); ?></a>
        </span>
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <?php echo $this->load->view('admin/common/index_listing_product', $dataTableObject, true); ?>
        </div>
    </div>
</section>
<!-- page end-->
