<!-- page start-->
<section class="panel">
    <header class="panel-heading tab-bg-dark-navy-blue">
        <span class="wht-color">
            <?php
                if ($id == 0) echo __t('Add Type');
                else echo __t('Edit Type');
                $formId = "TypeForm";
            ?>
        </span>
    </header>
    <div class="panel-body">
        <div class="stepy-tab">
            <ul id="default-titles" class="stepy-titles clearfix">

            </ul>
        </div>
        <form class="cmxform form-horizontal" id="<?php echo $formId;?>" method="post" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $id; ?>" name="id" id="id"/>
            <!--Basic-->
            <fieldset title="Basic" class="step" id="default-step-0" >
                <legend><?php echo __t("Basic Information"); ?> </legend>
                <div class="form-group">
                    <label class="col-lg-2 control-label require"><?php echo __t("Name"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="200" class="form-control" name="name" id="name" value="<?php echo (isset($type['name']) && !empty($type['name'])) ? $type['name'] : ""; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label"><?php echo __t("Description"); ?></label>
                    <div class="col-lg-6">
                        <input type="text"  maxlength="50" class="form-control" name="desc" id="desc" value="<?php echo (isset($type['desc']) && !empty($type['desc'])) ? $type['desc'] : ""; ?>">
                    </div>
                </div>
            </fieldset>
            <div style="clear:both;"></div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-6">
                    <input type="submit" class="finish btn btn-danger" value="Save"/>
                    <a class="btn btn-default" href="<?php echo __gurl('type/index');?>"><?php echo __t('Cancel'); ?></a>
                </div>
            </div>
        </form>
    </div>
</section>
<style>
    #TypeForm.cmxform {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: -moz-use-text-color #7fba00 #7fba00;
        border-image: none;
        border-style: none solid solid;
        border-width: 0 1px 1px;
        padding: 10px;
    }
</style>