<?php
class Transaction_model extends App_model {

    public function __construct() {
        parent::__construct();
    }

    public function getProductStock($where)
    {
        //$where['eb.is_deleted'] = 0;
        $where['es.qty >'] = 0;
        $data = $this->db->select('es.id, es.b_id, es.pur_id, es.p_id, sum(es.qty) qty, eb.b_name, eb.is_deleted, epd.exp, epd.net, epd.vat, epd.add, epd.mrp')
                        ->join('eb_product ep', 'ep.id = es.p_id', 'LEFT')
                        ->join('eb_batch eb', 'eb.id = es.b_id AND eb.p_id = es.p_id AND eb.is_deleted = 0', 'LEFT')
                        ->join('eb_purchase_detail epd', 'epd.pur_id = es.pur_id AND epd.p_id = es.p_id AND epd.b_id = es.b_id', 'LEFT')
                        ->where($where)
                        ->order_by("es.datetime_modified",'DESC',false)
                        ->group_by("es.b_id")
                        ->get('eb_stock es')->result_array();
        //echo $this->db->last_query();
        return $data;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function savePurchaseData($data, $id)
    {
        if($id==0)
        {
            $data['datetime_created'] = date('Y-m-d H:i:s');
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $this->db->insert('eb_purchase', $data);
            $success = $this->db->insert_id();
        }
        else
        {
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $success = $this->db->update('eb_product', $data, array('id'=>$id));
        }
        return $success;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function savePurchaseDetailData($data, $id = 0)
    {
        if($id==0)
        {
            $this->db->insert('eb_purchase_detail', $data);
            $success = $this->db->insert_id();
        }
        else
        {
            $success = $this->db->update('eb_purchase_detail', $data, array('id'=>$id));
        }
        return $success;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveStockDetailData($data, $id = 0)
    {
        if($id==0)
        {
            $data['datetime_created'] = date('Y-m-d H:i:s');
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $this->db->insert('eb_stock', $data);
            $success = $this->db->insert_id();
        }
        else
        {
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $success = $this->db->update('eb_stock', $data, array('id'=>$id));
        }
        return $success;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveSalesData($data, $id)
    {
        if($id==0)
        {
            $data['datetime_created'] = date('Y-m-d H:i:s');
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $this->db->insert('eb_sales', $data);
            $success = $this->db->insert_id();
        }
        else
        {
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $success = $this->db->update('eb_sales', $data, array('id'=>$id));
        }
        return $success;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveSalesDetailData($data, $id = 0)
    {
        if($id==0)
        {
            $this->db->insert('eb_sales_detail', $data);
            $success = $this->db->insert_id();
        }
        else
        {
            $success = $this->db->update('eb_sales_detail', $data, array('id'=>$id));
        }
        return $success;
    }

    public function getSaleData($where = '', $is_last = false)
    {
        if($where != '') $this->db->where($where);
        $this->db->select('s.*, eu.username')
                        ->join('eb_users eu', 'eu.id = s.u_id', 'LEFT')
                        ->order_by("s.datetime_modified",'DESC',false);
        if($is_last == true) $data = $this->db->limit(1)->get('eb_sales s')->result_array();
        else $data = $this->db->get('eb_sales s')->result_array();
        return $data;
    }

    public function getPurchaseData($where = '', $is_last = false)
    {
        if($where != '') $this->db->where($where);
        $this->db->select('p.*, ebc.name')
                        ->join('eb_company ebc', 'ebc.id = p.c_id', 'LEFT')
                        ->order_by("p.datetime_modified",'ASC',false);
        if($is_last == true) $data = $this->db->limit(1)->get('eb_purchase p')->result_array();
        else $data = $this->db->get('eb_purchase p')->result_array();
        return $data;
    }

    public function getPurchaseDetailData($where = array())
    {
        $this->db->select('p.purchase_invoice_id, pd.*, ep.p_name, ep.p_code, eb.b_name')
                        ->join('eb_purchase_detail pd', 'p.id=pd.pur_id', 'LEFT')
                        ->join('eb_product ep', 'ep.id=pd.p_id', 'LEFT')
                        ->join('eb_batch eb', 'eb.id=pd.b_id', 'LEFT')
                        ->where($where)
                        ->order_by("p.datetime_modified",'DESC',false);
        $data = $this->db->get('eb_purchase p')->result_array();
        return $data;
    }

    public function getSaleDetailData($where = array())
    {
        $this->db->select('s.invoice_id, sd.*, ep.p_name, ep.p_code, eb.b_name')
                        ->join('eb_sales_detail sd', 's.id=sd.s_id', 'LEFT')
                        ->join('eb_product ep', 'ep.id=sd.p_id', 'LEFT')
                        ->join('eb_batch eb', 'eb.id=sd.b_id', 'LEFT')
                        ->where($where)
                        ->order_by("s.datetime_modified",'DESC',false);
        $data = $this->db->get('eb_sales s')->result_array();
        return $data;
    }

    /**
     * Delete stock
     */
    public function delete_stock($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('eb_stock', $where);
        else
            return false;
    }

    /**
     * Delete stock
     */
    public function delete_purchase_details($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('eb_purchase_detail', $where);
        else
            return false;
    }

    /**
     * Delete stock
     */
    public function delete_purchase($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('eb_purchase', $where);
        else
            return false;
    }
}