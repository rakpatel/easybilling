<?php
class User_model extends App_model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Performs the check of the given user credentials. 
     * 
     * @param string $username Given user's name. 
     * @param type $password Given user's password (not hashed yet).
     * @return array|null Returns the session data of the logged in user or null on 
     * failure.
     */
    public function check_login($username, $password)
    {
        $password = md5($password);
        $user_data = $this->db
                        ->select('u.*,ud.*')
                        ->join('eb_user_details ud', 'ud.u_id = u.id', 'left')
                        ->from('eb_users u')
                        ->where(array('username' => $username, 'password' => $password))
                        ->get()->row_array();
        return ($user_data) ? $user_data : NULL;
    }

    /**
     * Get all users
     * @param type $where
     * @return type
     */
    public function getUsers($where = array())
    {
        $data = $this->db->select('eu.*, edu.*, eu.id as id')
                        ->join('eb_user_details edu','eu.id = edu.u_id','LEFT')
                        ->where($where)
                        ->get('eb_users eu')->result_array();
        return $data;
    }

    /**
     * Update/Insert User
     */
    public function update_users($user = array(), $where = array())
    {
        if (count($where) > 0 && $where['id'] > 0 && count($user) > 0)
            return $this->db->update('eb_users', $user, $where);
        else if (count($user) > 0)
        {
            unset($user['id']);
            $this->db->insert('eb_users', $user);
            return $this->db->insert_id();
        }
        else return false;
    }

    /**
     * Delete User
     */
    public function delete_users($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('user', $where);
        else
            return false;
    }
    
    public function reset_password($email = null) {
        $data = array('password' => md5($this->input->post('password')));
        if ($this->db->update('user', $data, array('email' => $email))) {
            return true;
        } else {
            return false;
        }
    }
}