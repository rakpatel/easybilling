<?php
class Location_model extends App_model {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get Countries
     */
    public function get_countries($where = array()) {
        $where['countries.is_deleted'] = 0;
        if (count($where) > 0) {
            $data = $this->db->where($where)->order_by('country_name','ASC')->get('countries')->result_array();
            return $data;
        } else
            return $this->db->get('countries')->result_array();
    }
    
    /**
     * Get Countries List
     */
    public function get_countries_list($where = array()) {
        $where['countries.is_deleted'] = 0;
        if (count($where) > 0)
            $countries = $this->db->select('country_id,country_name')->where($where)->order_by('country_name','ASC')->get('countries')->result_array();
        else
            $countries = $this->db->select('country_id,country_name')->order_by('country_name','ASC')->get('countries')->result_array();

        $data = array();
        foreach ($countries as $country) {
            $data[$country['country_id']] = $country['country_name'];
        }
        return $data;
    }

    /**
     * Update/Insert Country
     */
    public function update_country($data = array(), $where = array()) {
        
        foreach ($data as $key => $value) {
            if ($value=='') {
                unset($data[$key]);
            }
        }
        if (count($where) > 0 && count($data) > 0)
            return $this->db->update('countries', $data, $where);
        else if (count($data) > 0)
            return $this->db->insert('countries', $data);
        else
            return false;
    }

    /**
     * Delete Country
     */
    public function delete_country($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('countries', $where);
        else
            return false;
    }

    /**
     * Get States
     */
    public function get_states($where = array()) {
        $where['states.is_deleted'] = 0;
        if (count($where) > 0) {
            $data = $this->db->select('states.*,countries.country_id,countries.country_name')->join('countries', 'countries.country_id = states.country_id')->where($where)->order_by('states.state_name','ASC')->get('states')->result_array();
            return $data;
        } else
            return $this->db->select('states.*,countries.country_id,countries.country_name')->join('countries', 'countries.country_id = states.country_id')->order_by('states.state_name','ASC')->get('states')->result_array();
    }

    /**
     * Update/Insert State
     */
    public function update_state($data = array(), $where = array()) {
        foreach ($data as $key => $value) {
            if (empty($value)) {
                unset($data[$key]);
            }
        }
        if (count($where) > 0 && count($data) > 0)
            return $this->db->update('states', $data, $where);
        else if (count($data) > 0)
            return $this->db->insert('states', $data);
        else
            return false;
    }

    /**
     * Delete State
     */
    public function delete_state($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('states', $where);
        else
            return false;
    }

    /**
     * Get Cities
     */
    public function get_cities($where = array()) {
        $where['cities.is_deleted'] = 0;
        if (count($where) > 0) {
            $data = $this->db->select('cities.*,states.state_id,states.state_name,countries.country_id,countries.country_name')
                            ->join('countries', 'countries.country_id = cities.country_id')
                            ->join('states', 'states.state_id = cities.state_id', 'left')
                            ->where($where)
                            ->order_by('cities.city_name','ASC')    
                            ->get('cities')->result_array();
            return $data;
        } else {
            return $this->db->select('cities.*,states.state_id,states.state_name,countries.country_id,countries.country_name')
                            ->join('countries', 'countries.country_id = cities.country_id')
                            ->join('states', 'states.state_id = cities.state_id', 'left')
                             ->order_by('cities.city_name','ASC')      
                            ->get('cities')->result_array();
        }
    }
    
    /**
     * Get Cities
     */
    public function get_cities_list($where = array()) {
        $where['cities.is_deleted'] = 0;
        if (count($where) > 0)
            $cities = $this->db->select('city_id,city_name')->where($where)->order_by('cities.city_name','ASC')->get('cities')->result_array();
        else
            $cities = $this->db->select('city_id,city_name')->order_by('cities.city_name','ASC')->get('cities')->result_array();

        $data = array();
        foreach ($cities as $city) {
            $data[$city['city_id']] = $city['city_name'];
        }
        return $data;
    }

    /**
     * Update/Insert City
     */
    public function update_city($data = array(), $where = array()) {
        foreach ($data as $key => $value) {
            if (empty($value)) {
                unset($data[$key]);
            }
        }
        if (count($where) > 0 && count($data) > 0)
            return $this->db->update('cities', $data, $where);
        else if (count($data) > 0)
            return $this->db->insert('cities', $data);
        else
            return false;
    }

    /**
     * Delete city
     */
    public function delete_city($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('cities', $where);
        else
            return false;
    }
    
    public function get_country_base_on_city($id=null){
       
        $query = $this->db->select('cities.*,countries.*')->join('countries','countries.country_id=cities.country_id','inner')
                      ->where('cities.city_id',$id)->get('cities')->row_array()  ;
        
       return $query ;
    }
    
    
    
}