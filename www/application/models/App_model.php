<?php
class App_model extends CI_Model {
    
    public static $message =  "Record cann't be deleted have a dependent data in other table" ;  
    public static $toppick = '23';
    public function __construct() {
        parent::__construct();
        //$this->load->model('Email_Template_model');
    }

    /**
     * Check for user login or not
     */
    public function is_login() {
        if ($this->session->userdata('user')) {
            return $this->session->userdata('user');
        }    
        else{
            __red('user/login');
        }
    }

   

    /**
     * Generate Slug from name
     */
    public function generate_slug($str) {
        $str = strtolower(trim($str));
        $str = str_replace('-', '_', $str);
        $str = preg_replace('/[^a-z0-9-]/', '_', $str);
        $str = preg_replace('/_+/', "-", $str);
        return $str;
    }

    /**
     * Update currency rate with latest rate
     * @param string $base
     * @param bool $require
     */
    public function update_currency_detail($base = '', $require = '')
    {
        $currency_base = "http://openexchangerates.org/api";
        
        $query_URl = "/latest.json?app_id=f3639b45a1144eb688b7527be93d1901&base=AUD";
        $query_URl = "/latest.json?app_id=f3639b45a1144eb688b7527be93d1901";
        $ch = curl_init($currency_base.$query_URl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Get the data:
        $json = curl_exec($ch);
        curl_close($ch);
        // Decode JSON response:
        $exchangeRates = json_decode($json);
        //echo "<pre>"; print_r($exchangeRates);echo "</pre>";
        //var_dump($exchangeRates);
        if(isset($exchangeRates->rates) && !empty($exchangeRates->rates)) {
            foreach($exchangeRates->rates as $key => $val ) {
                //echo $key . " " . $val . "<br/>";
                $val = $val / $exchangeRates->rates->AUD;
                $val = round($val, 2);
                $this->db->update('currency',array('currency_rate' => $val),array('currency_code' => $key));
            }
        }
        echo "Sucess";exit;
    }
    
    /**  change date format from d-m-Y to Y-m-d  
     *  @params date $date
     */
    public function changeDateFormat($date = NULL){
       return date(DEFINE_DATE_Ymd_FORMATE,strtotime($date));
    }
    
    /**  change date format from Y-m-d to d-m-Y
     *   @params date $date
     */
    public function normalDateFormat($date = NULL){
        return date(DEFINE_DATE_FORMATE,strtotime($date));
    }
    
    public function check_relation($where = array(), $table,$redirect) {
        $data = $this->db->where($where)->get($table)->num_rows();
        if ($data > 0) {
            $this->session->set_flashdata('error', self::$message);
            redirect($redirect);
        }
        //return $data;
    }
    /**
     * update delete status
     * @param type $mytable
     * @param type $condition
     * @return type
     */
    public function update_delete_status($mytable,$condition){
        $data = array('is_deleted'=>1) ; 
        return $this->db->update($mytable,$data,$condition); 
    }
    
    /**
     * Get Products Lowest Price..
     */
    
    public function get_product_price($id) {
        if(!empty($id)) {
            $options = $this->db->select('product_option.*,product_pricing_group.price as group_price,product_pricing_person.price as person_price,currency.currency_code as supplier_currency')
                        ->join('product','product.id = product_option.product_id','left')
                        ->join('supplier_detail','supplier_detail.user_id = product.supplier_id','left')
                        ->join('currency','currency.id = supplier_detail.currency_id','left')
                        ->join('product_pricing_person','product_pricing_person.product_option_id = product_option.id','left')
                        ->join('product_pricing_group','product_pricing_group.product_option_id = product_option.id','left')
                        ->where(array('product_id' => $id,'product_option.status' => 1))
                        ->get('product_option')->result_array();
            if(isset($options) && !empty($options)) {
                $price = array();
                foreach ($options as $option) {
                    if($option['price_type'] == 'persona') {
                        $prices = json_decode($option['person_price']);
                        if(isset($prices) && !empty($prices)) {
                            foreach($prices as $key => $val) {
                                if($key != 'adult') continue;
                                if(isset($val->price) && !empty($val->price)) {
                                    foreach ($val->price as $p ) {
                                        $p = trim($p);
                                        if( isset($p) && !empty($p) && $p > 0){ 
                                            $price[] = $p;
                                        }
                                    }
                                }
                                
                            }
                        }
                    } else if($option['price_type'] == 'group') {
                        //if(!empty($option['group_price']) && $option['group_price'] > 0)
                        //$price[] = $option['group_price'];
                    }
                }
                //pr($price,false);
                if (!($this->session->userdata('currCurrency'))) {
                    $this->session->set_userdata('currCurrency', "AUD");
                }
                $currencies = $this->session->userdata('currencies');
                $currencies = $this->session->userdata('currenciesList');
                if(isset($currencies[$this->session->userdata('currCurrency')])) {
                    $currDetails = $currencies[$this->session->userdata('currCurrency')];
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )? $currDetails['currency_symbol']: "";
                } else {
                    $currDetails = $currencies["AUD"];
                    $this->session->set_userdata('currCurrency', "AUD");
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )?$currDetails['currency_symbol'] : "";
                }
                if(empty($price)){
                    foreach ($options as $option) {
                        if($option['price_type'] == 'group') {
                            if(!empty($option['group_price']) && $option['group_price'] > 0)
                                $price[] = $option['group_price'];
                        }
                    }
                    if(empty($price))
                    return $this->session->userdata('currCurrency') . " " .$symbol . "0" ;
                }
                
                //pr($currencies);
                foreach ($price as $k => $v) {
                    if(empty($v) || floatval($v) <= 0 ) {
                        unset($price[$k]);
                    }
                }
                
                if(isset($currencies[$this->session->userdata('currCurrency')])) {
                    $currDetails = $currencies[$this->session->userdata('currCurrency')];
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )? $currDetails['currency_symbol']: "";
                    //return $this->session->userdata('currCurrency') . " " .$symbol . "" . (number_format(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*min($price),0));
                    //echo ($currDetails['currency_rate'] . " / " . $currencies[$option['supplier_currency']]['currency_rate']) .  " * " .  min($price) ."<br/>";
                    return $this->session->userdata('currCurrency') . " " .$symbol . "" . (number_format(ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*min($price)),0));
                } else {
                    $currDetails = $currencies["AUD"];
                    $this->session->set_userdata('currCurrency', "AUD");
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )?$currDetails['currency_symbol'] : "";
                    //return $this->session->userdata('currCurrency') . " " .$symbol . "" . (number_format(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*min($price),0));
                    return $this->session->userdata('currCurrency') . " " .$symbol . "" . (number_format(ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*min($price)),0));
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
        
    public function get_product_special_price($id,$percent) {
        if(!empty($id)) {
            $options = $this->db->select('product_option.*,product_pricing_group.price as group_price,product_pricing_person.price as person_price,currency.currency_code as supplier_currency')
                        ->join('product','product.id = product_option.product_id','left')
                        ->join('supplier_detail','supplier_detail.user_id = product.supplier_id','left')
                        ->join('currency','currency.id = supplier_detail.currency_id','left')
                        ->join('product_pricing_person','product_pricing_person.product_option_id = product_option.id','left')
                        ->join('product_pricing_group','product_pricing_group.product_option_id = product_option.id','left')
                        ->where(array('product_id' => $id,'product_option.status' => 1))
                        ->get('product_option')->result_array();
            if(isset($options) && !empty($options)) {
                $price = array();
                foreach ($options as $option) {
                    if($option['price_type'] == 'persona') {
                        $prices = json_decode($option['person_price']);
                        if(isset($prices) && !empty($prices)) {
                            foreach($prices as $key => $val) {
                                if($key != 'adult') continue;
                                if(isset($val->price) && !empty($val->price)) {
                                    foreach ($val->price as $p ) {
                                        $p = trim($p);
                                        if( isset($p) && !empty($p)){ 
                                            $price[] = $p;
                                        }
                                    }
                                }
                                
                            }
                        }
                    } else if($option['price_type'] == 'group') {
                        //if(!empty($option['group_price']))
                        //$price[] = $option['group_price'];
                    }
                }
                if (!($this->session->userdata('currCurrency'))) {
                    $this->session->set_userdata('currCurrency', "AUD");
                }
                $currencies = $this->session->userdata('currencies');
                $currencies = $this->session->userdata('currenciesList');
                if(isset($currencies[$this->session->userdata('currCurrency')])) {
                    $currDetails = $currencies[$this->session->userdata('currCurrency')];
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )? $currDetails['currency_symbol']: "";
                } else {
                    $currDetails = $currencies["AUD"];
                    $this->session->set_userdata('currCurrency', "AUD");
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )?$currDetails['currency_symbol'] : "";
                }
                //if(empty($price)) return $this->session->userdata('currCurrency') . " " .$symbol . "0";
                if(empty($price)) {
                    foreach ($options as $option) {
                        if($option['price_type'] == 'group') {
                            if(!empty($option['group_price']) && $option['group_price'] > 0)
                                $price[] = $option['group_price'];
                        }
                    }
                    if(empty($price))
                    return $this->session->userdata('currCurrency') . " " .$symbol . "0" ;
                }
                foreach ($price as $k => $v) {
                    if(empty($v) || floatval($v) <= 0 ) {
                        unset($price[$k]);
                    }
                }
                $priceArr = array();
                
                if(isset($currencies[$this->session->userdata('currCurrency')])) {
                    $currDetails = $currencies[$this->session->userdata('currCurrency')];
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )? $currDetails['currency_symbol']: "";
                    $net = (ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate']) * min($price))) - floor((ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate']) * min($price))) * $percent *.01) ; 
                    //echo min($price) ." - ". (min($price) * $percent *.01) ;
                    $priceArr['special_price'] = $this->session->userdata('currCurrency') . " " .$symbol . "" . $net;
                    $priceArr['net_price'] =  $symbol . "" .(number_format(ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*min($price)),0)); 
                    //pr($priceArr);
                    return $priceArr ;
                    //return $this->session->userdata('currCurrency') . " " .$symbol . "" . (number_format(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*$net,0));
                } else {
                    $currDetails = $currencies["AUD"];
                    $this->session->set_userdata('currCurrency', "AUD");
                    //$net = min($price) - floor(min($price) * $percent *.01) ; 
                    $net = (ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate']) * min($price))) - floor((ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate']) * min($price))) * $percent *.01) ; 
                    //echo min($price) ." - ". (min($price) * $percent *.01) ;
                    $symbol = (isset($currDetails['currency_symbol'])&& !empty($currDetails['currency_symbol']) )?$currDetails['currency_symbol'] : "";
                    $priceArr['special_price'] = $this->session->userdata('currCurrency') . " " .$symbol . "" . (number_format(fllor(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*$net),0));
                    $priceArr['net_price'] = $symbol . "" .(number_format(ceil(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*min($price)),0));
                    //pr($priceArr);
                    return $priceArr ;
                    //return $this->session->userdata('currCurrency') . " " .$symbol . "" . (number_format(($currDetails['currency_rate']/$currencies[$option['supplier_currency']]['currency_rate'])*$net,0));
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    
}