<?php
class Customer_model extends App_model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get Carrencies
     */
    public function get_customers($where = array()) {
        $where['is_deleted']  = 0 ;
        if (count($where) > 0) {
            $data = $this->db->select('customer.*')->where($where)->get('customer')->result_array();
            return $data;
        } else {
            $data =  $this->db->where($where)->get('customer')->result_array();
            return $data ;
        }
    }
    
    public function get_customers_profile($where = array()) {
        $where['customer.is_deleted']  = 0 ;
        if (count($where) > 0) {
            $data = $this->db->select('customer.*,customer_detail.*')->join('customer_detail','customer_detail.customer_id=customer.id','LEFT')->where($where)->get('customer')->result_array();
            return $data;
        } else
            return $this->db->where($where)->get('customer')->result_array();
    }

    /**
     * Update/Insert Customer
     */
    public function update_customer($data = array(), $where = array()) {
        if (count($where) > 0 && count($data) > 0)
            return $this->db->update('customer', $data, $where);
        else if (count($data) > 0)
            return $this->db->insert('customer', $data);
        else
            return false;
    }

    /**
     * Delete Customer
     */
    public function delete_customer($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('customer', $where);
        else
            return false;
    }
    
    public function get_fb_customer($id,$email=null){
        if(!empty($email))
        {
            return $data = $this->db->select('customer.*')->where('email',$email)->get('customer')->row_array();
        }
        else
        {
            return $data = $this->db->select('customer.*')->where('facebook_reference_id',$id)->get('customer')->row_array();
        }
    }
    /*
    public function fb_customer_login() {
        $data = $this->input->post();
        $fbcheck = $this->get_fb_customer($data['fbprofile']['fbCredential']['authResponse']['userID']);
        //echo $this->db->last_query(); die ;
        $fbArray = array('first_name' => isset($data['fbprofile']['graph']['name'])?$data['fbprofile']['graph']['name'] : isset($fbcheck['first_name'])?$fbcheck['first_name']:"" , 'login_type' => 2, 'status' => 1, 'facebook_reference_id' => $data['fbprofile']['fbCredential']['authResponse']['userID']);
        if (count($fbcheck) <= 0) {
            $this->db->insert('customer', $fbArray);
            $insert_id = $this->db->insert_id();
            if (!empty($insert_id)) {
                $detail = array('customer_id' => $insert_id, 'is_subscribe' => 0);
                $this->db->insert('customer_detail', $detail);
                $newdata = array(
                    'Name' => isset($data['fbprofile']['graph']['name']) ? $data['fbprofile']['graph']['name'] : '',
                    'front_id' => $insert_id,
                    'fb_id' => $data['fbprofile']['fbCredential']['authResponse']['userID']
                );
                
                
                
                $this->session->set_userdata($newdata);
            }
        } else {
            $this->db->where('id', $fbcheck['id']);
            if ($this->db->update('customer', $fbArray)) {
                $newdata = array(
                    'Name' => $fbcheck['first_name'] . ' ' . $fbcheck['last_name'],
                    'front_id' => $fbcheck['id'],
                    'fb_id' => $data['fbprofile']['fbCredential']['authResponse']['userID']
                );
                $this->session->set_userdata($newdata);
            }
        }
    }
    */
    
    public function fb_customer_login_28112015() {
        $data = $this->input->post();
        $fbcheck = $this->get_fb_customer($data['fbprofile']['fbCredential']['authResponse']['userID'],@$data['fbprofile']['graph']['email']);
        if(!empty($fbcheck)){
            $fbArray = array('first_name' => isset($fbcheck['first_name'])?$fbcheck['first_name']:"", 'login_type' => 2, 'status' => 1, 'facebook_reference_id' => $data['fbprofile']['fbCredential']['authResponse']['userID']);
        }
        else
        {
            $fbArray = array('first_name' => isset($data['fbprofile']['graph']['first_name'])?$data['fbprofile']['graph']['first_name'] : '','last_name'=>isset($data['fbprofile']['graph']['last_name'])?$data['fbprofile']['graph']['last_name'] : '', 'login_type' => 2, 'status' => 1,'email'=>isset($data['fbprofile']['graph']['email'])?$data['fbprofile']['graph']['email']:'','facebook_reference_id' => $data['fbprofile']['fbCredential']['authResponse']['userID']);
        }
        if (count($fbcheck) <= 0) {
            $this->db->insert('customer', $fbArray);
            $insert_id = $this->db->insert_id();
            if (!empty($insert_id)) {
                $detail = array('customer_id' => $insert_id, 'is_subscribe' => 1);
                $this->db->insert('customer_detail', $detail);
                $newdata = array(
                    'Name' => isset($data['fbprofile']['graph']['first_name']) ? $data['fbprofile']['graph']['first_name'] : '',
                    'front_id' => $insert_id,
                    'fb_id' => $data['fbprofile']['fbCredential']['authResponse']['userID']
                );
                
                
                
                $this->session->set_userdata($newdata);
            }
        } else {
            $this->db->where('id', $fbcheck['id']);
            if ($this->db->update('customer', $fbArray)) {
                $newdata = array(
                    'Name' => $fbcheck['first_name'] . ' ' . $fbcheck['last_name'],
                    'front_id' => $fbcheck['id'],
                    'fb_id' => $data['fbprofile']['fbCredential']['authResponse']['userID']
                );
                $this->session->set_userdata($newdata);
            }
        }
    }
    public function fb_customer_login() {
        $data = $this->input->post();
        if(!empty($data['fbprofile']['id'])) {
        $fbcheck = $this->get_fb_customer($data['fbprofile']['id'],@$data['fbprofile']['email']);
        if(!empty($fbcheck)){
            $fbArray = array('first_name' => isset($fbcheck['first_name'])?$fbcheck['first_name']:"", 'login_type' => 2, 'status' => 1, 'facebook_reference_id' => $data['fbprofile']['id']);
        }
        else
        {
            $fbArray = array('first_name' => isset($data['fbprofile']['first_name'])?$data['fbprofile']['first_name'] : '','last_name'=>isset($data['fbprofile']['last_name'])?$data['fbprofile']['last_name'] : '', 'login_type' => 2, 'status' => 1,'email'=>isset($data['fbprofile']['email'])?$data['fbprofile']['email']:'','facebook_reference_id' => $data['fbprofile']['id']);
        }
        if (count($fbcheck) <= 0) {
            $this->db->insert('customer', $fbArray);
            $insert_id = $this->db->insert_id();
            if (!empty($insert_id)) {
                $detail = array('customer_id' => $insert_id, 'is_subscribe' => 1);
                $this->db->insert('customer_detail', $detail);
                $newdata = array(
                    'Name' => isset($data['fbprofile']['first_name']) ? $data['fbprofile']['first_name'] : '',
                    'front_id' => $insert_id,
                    'fb_id' => $data['fbprofile']['id']
                );
                $this->session->set_userdata($newdata);
            }
        } else {
            $this->db->where('id', $fbcheck['id']);
            if ($this->db->update('customer', $fbArray)) {
                $newdata = array(
                    'Name' => $fbcheck['first_name'] . ' ' . $fbcheck['last_name'],
                    'front_id' => $fbcheck['id'],
                    'fb_id' => $data['fbprofile']['id']
                );
                $this->session->set_userdata($newdata);
            }
        }
        }
    }
    
    
    public function customer_login(){
        $where = array('status'=>'1','email'=>$this->input->post('email'),'password'=>md5($this->input->post('password')));
        $query = $this->db->select('customer.*')->where($where)->get('customer') ;
        
        if($query->num_rows()==1){
            $data =  $query->row_array() ;
            $rememberme = $this->input->post('remeber_me') ;
            $sessiondata = array('front_id'=>$data['id'],'Name'=>$data['first_name'].''.$data['last_name']);
            if(isset($rememberme) && $rememberme=='on'){
                $sessiondata['rememberme'] = 1;
            }
            $this->session->set_userdata($sessiondata);
            $product_id = $this->input->post('product_id');
            //echo "Product id : " . $product_id;
            if(isset($product_id) && !empty($product_id)) {
                $exists = $this->db->where(array('product_id' => $this->input->post('product_id') ,'customer_id' => $this->session->userdata('front_id')))->get('product_wishlist')->result_array();
                if(!($exists)) {
                    $this->db->insert('product_wishlist',array('product_id' => $this->input->post('product_id') ,'customer_id' => $this->session->userdata('front_id')));
                }
                
            }
            return 'true';
        }
        else 
         return 'false' ;
    }
    
    /**
     * Get favority Products
     */
    public function get_wishlist($id) {
        $favourites = array();
        if($id) {
            $data = $this->db->where('customer_id',$id)->get('product_wishlist')->result_array();
            if($data) {
                foreach ($data as $fav) {
                    $favourites[] = $fav['product_id'];
                }
            }
        }
        return $favourites;
    }
    
    public function update_profile($id=null){
        $data= $this->input->post();
        //_p($data,1);
        foreach($data['customer'] as $key=>$val ):
            if($val=='')
                unset($data['customer'][$key]);
        endforeach;
        
        foreach($data['detail'] as $key=>$val ):
            if($val=='')
                unset($data['detail'][$key]);
        endforeach;
        
        //_p($data,1);
        $data['detail']['is_subscribe'] = isset($data['detail']['is_subscribe'])?$data['detail']['is_subscribe']:0;
        if(!empty($data)){
                unset($data['customer']['password']);
            if(!empty($data['customer']['newpassword']))
                $data['customer']['password']  = md5($data['customer']['newpassword']) ;
                
                //_p($data,1);
            unset($data['customer']['newpassword']);
            unset($data['customer']['conformpassword']);
              //_p($data,1);
            if($this->db->update('customer',$data['customer'],array('id'=>$id))){
                if(count($data['detail']) > 0 ) {
                    $this->db->update('customer_detail',$data['detail'],array('customer_id'=>$id));
                    $arr = array('email'=>$data['customer']['email'],'status'=>$data['detail']['is_subscribe']) ;
                    $this->db->update('newsletter_subscriber',$arr,array('customer_id'=>$id)) ;
                }
                return true ;
            }
        }
        else{
            return false ;
        }
    }
    public function front_customer_registration(){
        $post = $this->input->post();
        $data = array();
        if(!empty($post)) {
            
            $data = array('first_name' => $post['first_name'],
                            'last_name' => $post['last_name'],
                            'login_type' => 1,
                            'status' => 1,
                            'password' => md5($post['password']), 
                            'email' => $post['email']
                         );
           
            $this->db->insert('customer', $data);
            $insert_id = $this->db->insert_id();
            /**Login Created */
            $newdata = array(
                'Name' => $post['first_name'] . ' ' . $post['last_name'],
                'front_id' => $insert_id,
            );
            $this->session->set_userdata($newdata);
            /**Login Created */
            if (!empty($insert_id)) {
                $detail = array('customer_id' => $insert_id, 'is_subscribe' => isset($post['is_subscribe']) ? 1 : 0);
                $this->db->insert('customer_detail', $detail);

                $arr = array('email'=>$post['email'],'customer_id'=>$insert_id,'status'=>$detail['is_subscribe']);
                $this->db->insert('newsletter_subscriber',$arr);
                
                
            }
            return $insert_id;
        }
    }
    public function reset_password($email=null){
        $data = array('password'=>md5($this->input->post('password')));
        if($this->db->update('customer',$data,array('email'=>$email)))
        {
            return true ;
        }
        else
        {
            return false ;
        }
    }
    
    public function activate_account($customer_id){
        $data = array('status'=>1);
        $this->db->update('customer',$data,array('id'=>$customer_id));
    }

}