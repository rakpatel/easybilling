<?php
class Banner_model extends App_model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get Carrencies
     */
    public function get_banners($where = array()) {
        $where['is_deleted']=0 ;
        if (count($where) > 0) {
            $data = $this->db->select('banner_management.*')->where($where)->order_by('banner_management.order','ASC')->get('banner_management')->result_array();
            return $data;
        } else {
            return $this->db->get('banner_management')->order_by('banner_management.order','ASC')->result_array();
        }
    }

    /**
     * Update/Insert Banner
     */
    public function update_banner($data = array(), $where = array()) {
        if (isset($data['title']) && !empty($data['title'])) {
            $data['slug'] = $this->generate_slug($data['title']);
        }
        if (count($where) > 0 && count($data) > 0)
            return $this->db->update('banner_management', $data, $where);
        else if (count($data) > 0)
            return $this->db->insert('banner_management', $data);
        else
            return false;
    }

    /**
     * Delete Banner
     */
    public function delete_banner($where = array()) {
        if (count($where) > 0)
            return $this->db->delete('banner_management', $where);
        else
            return false;
    }
    
    public function deletImage() {
        $data = array('image_name' => '');
        $condition = array('id' => $this->input->post('id'));
        if ($this->db->update('banner_management', $data, $condition)) {
            $originalfilename = BANNER_IMAGE_PATH . $this->input->post('image_name');
            @unlink($originalfilename);
        }
    }

}