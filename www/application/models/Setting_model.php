<?php
class Setting_model extends App_model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get Carrencies
     */
    public function get_setting($key = "") {
        if (!empty($key)) {
            $data = $this->db->select('eb_settings.*')->where(array('name' => $key))->get('eb_settings')->result_array();
            return ($data)?$data:false;
        } else
            return $this->db->select('eb_settings.*')->get('eb_settings')->result_array();;
    }

    /**
     * Update/Insert Currency
     */
    public function update_setting($key, $value) {
        
        if (($key) && ($value)) {
            $exits = $this->get_setting($key);
            if($exits) {
                $exits = $exits[0];
                $exits['value'] = $value;
                $where = array('id' => $exits['id']);
                return $this->db->update('eb_settings', $exits, $where);
            } else {
                $data = array('name'=> $key,'value' => $value);
                return $this->db->insert('eb_settings', $data);
            } 
        } else
            return false;
    }

    public function updateDatabase($query)
    {
        if($query != '')
        {
            $this->db->query($query);
        }
    }
}