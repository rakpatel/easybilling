<?php
class Company_model extends App_model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get all company
     * @param type $where
     * @return type
     */
    public function getCompany($where = array())
    {
        $data = $this->db->select('ec.*')
                        ->where($where)
                        ->get('eb_company ec')->result_array();
        return $data;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveData($data, $id)
    {
        if($id==0)
        {
            unset($data['id']);
            if(isset($data['is_stockist']) && $data['is_stockist'] == 'on') $data['is_stockist'] = 1;
            else $data['is_stockist'] = 0;

            $data['datetime_created'] = date('Y-m-d H:i:s');
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $success = $this->db->insert('eb_company', $data);
        }
        else
        {
            if(isset($data['is_stockist'])) $data['is_stockist'] = 1;
            else $data['is_stockist'] = 0;
            $success = $this->db->update('eb_company', $data, array('id'=>$id));
        }
        return $success;
    }
}