<?php
class Product_model extends App_model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get all products
     * @param type $where
     * @return type
     */
    public function getProducts($where = array())
    {
        $where['p.is_deleted'] = 0;
        foreach ($where as $key => $value) {
            if ($key == "OR") {
                $this->db->or_where($value);
                unset($where[$key]);
            }
        }
        foreach ($where as $key => $value) {
            if (is_array($value)) {
                $this->db->where_in($key, $value);
                unset($where[$key]);
            }
        }
        $data = $this->db->select('p.id, p.p_name, p.p_code, p.no_of_qty, ept.id type_id, ept.name type_name, epu.id unit_id,epu.name unit_name, ec.id manufacturer_id,ec.name m_name, ec.short_name')
                        ->join('eb_product_type ept', 'ept.id = p.type_id', 'LEFT')
                        ->join('eb_product_unit epu', 'epu.id = p.unit_id', 'LEFT')
                        ->join('eb_company ec', 'ec.id = p.manufacturer_id', 'LEFT')
                        ->where($where)
                        ->order_by("p.datetime_modified",'DESC',false)
                        ->get('eb_product p')->result_array();
        return $data;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveData($data, $id)
    {
        if($id==0)
        {
            unset($data['id']);
            $data['datetime_created'] = date('Y-m-d H:i:s');
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $success = $this->db->insert('eb_product', $data);
        }
        else
        {
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $success = $this->db->update('eb_product', $data, array('id'=>$id));
        }
        return $success;
    }
    
    /**
     * Get all types
     * @param type $where
     * @return type
     */
    public function getType($where = array())
    {
        $data = $this->db->select('ept.*')
                        ->where($where)
                        ->get('eb_product_type ept')->result_array();
        return $data;
    }

    public function getUnit($where = array())
    {
        $data = $this->db->select('epu.*')
                ->where($where)
                ->get('eb_product_unit epu')->result_array();
        return $data;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveDataUnit($data, $id)
    {
        if($id==0)
        {
            unset($data['id']);
            $success = $this->db->insert('eb_product_unit', $data);
        }
        else $success = $this->db->update('eb_product_unit', $data, array('id'=>$id));
        return $success;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveDataType($data, $id)
    {
        if($id==0)
        {
            unset($data['id']);
            $success = $this->db->insert('eb_product_type', $data);
        }
        else $success = $this->db->update('eb_product_type', $data, array('id'=>$id));
        return $success;
    }

    public function getBatch($where = array())
    {
        $data = $this->db->select('eb.*,p.*')
                ->join('eb_product p', 'p.id = eb.p_id', 'LEFT')
                ->where($where)
                ->get('eb_batch eb')->result_array();
        return $data;
    }

    /**
     * Insert data
     * @param type $data
     * @param type $id
     */
    public function saveBatchData($data, $id)
    {
        if($id==0)
        {
            if(isset($data['id'])) unset($data['id']);
            $data['datetime_created'] = date('Y-m-d H:i:s');
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $this->db->insert('eb_batch', $data);
            $success = $this->db->insert_id();
        }
        else
        {
            $data['datetime_modified'] = date('Y-m-d H:i:s');
            $success = $this->db->update('eb_batch', $data, array('id'=>$id));
        }
        return $success;
    }
}