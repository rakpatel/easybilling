<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Setting extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('setting_model');
        $logIndata = $this->session->userdata('user');
        if(isset($logIndata) && $logIndata['role_id'] != 1)
        {
            echo "You do not have rights for this module.";exit;
        }
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $arrPost = $this->input->post();
        if (!empty($arrPost))
        {
            foreach ($arrPost as $key => $value)
            {
                $this->setting_model->update_setting($key, $value);
            }
        }
        $this->data['arrSetting'] = $this->setting_model->get_setting();
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Setting');
        $this->template->write_view('content', 'admin/setting/index', $this->data);
        $this->template->render();
    }

    /**
     * Index Page for this controller.
     */
    public function updateDatabase()
    {
        $arrPost = $this->input->post();
        if (!empty($arrPost))
        {
            $query = $arrPost['query'];
            $this->setting_model->updateDatabase($query);
        }
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Database');
        $this->template->write_view('content', 'admin/setting/database', $this->data);
        $this->template->render();
    }
}