<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Company extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('company_model');
    }

    public function get_data_table_data() {
        $readonly = '' ;
        $where['c.is_deleted'] = 0;
        $this->datatables->select('c.name, c.short_name, c.number, c.cin_num, c.tin_num, (case when c.is_stockist =1 then "Yes" else "No" end) as stockist, c.id', false)
                        ->where($where)
                        ->from('eb_company c');

        echo $this->datatables->generate_products('UTF-8', 'company');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $company = array();
        $search_feilds = array(
            'name' => array('type' => 'text', 'lable' => 'Party/Manufacturer Name'),
            'short_name' => array('type' => 'text', 'lable' => 'Short Name'),
            'stockist' => array('type' => 'select', 'lable' => 'Is Stockist', 'options' => array('Yes'=>'Yes', 'No'=>'No')),
        );
        $columns = array(
            'Name' => array('index' => 0, 'type' => 'text','width' => '45% !important '),
            'Short Name' => array('index' => 0, 'type' => 'text','width' => '11% !important '),
            'Number' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '20% !important '),
            'CIN Number'       => array('index' => 2, 'type' => 'text','width' => '10% !important '),
            'TIN Number'=>array('index' => 3, 'type' => 'text','width' => '10% !important '),
            'Is Stockist' =>array('index' => 4, 'width' => '5% !important ', 'type' => 'text'),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text'),
        );
        $db_fields_list = array('name','short_name','number','cin_num','tin_num','stockist','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $company,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('company/get_data_table_data');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Company');
        $this->template->write_view('content', 'admin/company/index', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function update($id = 0)
    {
        $company = array();
        $this->data['id'] = $id;
        $this->data['company'] = array();
        if ($this->input->post())
        {
            $company = $this->input->post();
            $success = $this->company_model->saveData($company, $id);
            if($success) __red('company/index');
        }
        else
        {
            if($id > 0)
            {
                $company = $this->company_model->getCompany(array('ec.id'=>$id));
                if(!empty($company)) $company = $company[0];
            }
        }
        $this->data['company'] = $company;

        $this->template->set_template('admin');
        if ($id == 0)
            $this->template->write('title', 'Easy Billing - Add Company');
        else
            $this->template->write('title', 'Easy Billing - Edit Company');
        $this->template->write_view('content', 'admin/company/update', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function delete($id = 0)
    {
        if($id > 0)
        {
            $company['is_deleted'] = 1;
            $success = $this->company_model->saveData($company, $id);
            if($success) __red('company/index');
        }
    }
}