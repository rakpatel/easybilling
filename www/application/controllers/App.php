<?php defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller
{
    public $data = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('setting_model');
        $arrVal = $this->setting_model->get_setting('system_check');
        $db_val = $arrVal[0]['value'];
        $check = md5('easybillingrakesh'.date('Y'));
        $arrPath = explode('/', $_SERVER['REQUEST_URI']);
        if($db_val != $check && isset($arrPath[2]) && $arrPath[2] != 'setting')
        {
            echo "System expire. Please call system administrator to renew subscription for ".date('Y').". Call on (+91) 909 908 6983 to get more details";exit;
        }
    }

    /*
     * @function is used to generate pdf
     * @params
     * 
     */

    public function generatePDF($html, $filename = '', $flag = 'I') {
        $filename = preg_replace('/\\\\/', DIRECTORY_SEPARATOR, $filename);
        if (!file_exists(dirname($filename)))
            mkdir(dirname($filename));
        $this->load->library('tcpdf/tcpdf');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage();

        // create some HTML content
        // output the HTML content
        $pdf->writeHTML($html, true, 0, true, 0);

        // reset pointer to the last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        // Close and output PDF document
        $data = $pdf->Output($filename, $flag);
    }
    
    
    public function generatePDFWATERMARK($html, $filename = '', $flag = 'I') {
        $filename = preg_replace('/\\\\/', DIRECTORY_SEPARATOR, $filename);
        if (!file_exists(dirname($filename)))
            mkdir(dirname($filename));
        $this->load->library('tcpdf/tcpdf');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $img_file = K_PATH_MAIN.FRONTEND_IMAGES.'gift_voucher.jpg';
        $pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------
        // set font
        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage();

        // create some HTML content
        // output the HTML content
        $pdf->writeHTML($html, true, 0, true, 0);

        // reset pointer to the last page
        $pdf->lastPage();

        // ---------------------------------------------------------
        // Close and output PDF document
        $data = $pdf->Output($filename, $flag);
    }
    
    /*
     *  Send Mail
     */
    public function send_email($to, $subject, $mail_body, $from = '', $attach = '', $cc = '', $bcc = '') {
        
        if($_SERVER['SERVER_NAME']=='localhost'){
        
            $this->load->library('email');
            $this->email->clear(TRUE);
            $this->email->from($from,'Easy Billing');
            $this->email->to($to);
            $this->email->subject($subject);
            $this->email->message($mail_body);

            if ($cc != '') {
                if (is_array($cc))
                    foreach ($cc as $id)
                        $this->email->cc($id);
                else
                    $this->email->cc($cc);
            }
            if ($bcc != '') {
                if (is_array($bcc))
                    foreach ($bcc as $id)
                        $this->email->bcc($id);
                else
                    $this->email->bcc($bcc);
            }
            if ($attach != '') {
                if (is_array($attach)){
                    foreach ($attach as $a) {
                      
                        $this->email->attach($a);
                    }
                } else {
                    $this->email->attach($attach);
                }
            }

            $this->email->send();
            $this->email->clear(TRUE);
             //echo'<pre>'; print_r($this->email->print_debugger()); die('dsd'); 
            return;
        
        } else {

            /*$filename = array_reverse(explode('/', $attach));
            $filename = $filename[0];
            $file_size = filesize($attach);
            $handle = fopen($attach, "r");
            $content = fread($handle, $file_size);
            fclose($handle);

            $content = chunk_split(base64_encode($content));

            //$from = ($from == '') ? $ci->config->item('mail_from') : $from;
            $header = "From: Easy Billing \r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
            $header .= "This is a multi-part message in MIME format.\r\n";
            $header .= "--" . $uid . "\r\n";
            $header .= "Content-type:text/html; charset=iso-8859-1\r\n";
            $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $header .= $mail_body . "\r\n\r\n";
            $header .= "--" . $uid . "\r\n";
            $header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n"; // use different content types here
            $header .= "Content-Transfer-Encoding: base64\r\n";
            $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $header .= $content . "\r\n\r\n";
            $header .= "--" . $uid . "--";*/

            
            if(!empty($from))
            $headers = "From: ".$from." \r\n";
            else
            $headers = "From: Easy Billing Team <info@easybilling.com> \r\n";
            $headers .= "Reply-To: ". strip_tags($to) . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $send = mail($to, $subject, $mail_body, $headers);

            if ($send)
                return true;
            else
                return false;
        }
    }
}