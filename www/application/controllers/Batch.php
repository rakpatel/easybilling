<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Batch extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function get_data_table_data() {
        $where['eb.is_deleted'] = 0;
        $this->datatables->select('eb.b_name, p.p_name, eb.id', false)
                        ->join('eb_product p', 'p.id = eb.p_id', 'LEFT')
                        ->where($where)
                        ->from('eb_batch eb');

        echo $this->datatables->generate_products('UTF-8', 'batch');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $batch = array();
        $search_feilds = array(
            'b_name' => array('type' => 'text', 'lable' => 'Batch Name'),
            'p_name' => array('type' => 'text', 'lable' => 'Product Name'),
        );
        $columns = array(
            'Batch Name' => array('index' => 0, 'type' => 'text','width' => '30% !important '),
            'Product Name' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '60% !important '),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text', 'style'=>'width:10% !important;'),
        );
        $db_fields_list = array('b_name','p_name','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $batch,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('batch/get_data_table_data');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Batch');
        $this->template->write_view('content', 'admin/batch/index', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function update($id = 0)
    {
        $batch = array();
        $this->data['id'] = $id;
        $this->data['batch'] = array();
        $user_data = $this->session->userdata('user');
        if ($this->input->post())
        {
            $batch = $this->input->post();
            $success = $this->product_model->saveBatchData($batch, $id);
            if($success) __red('batch/index');
        }
        else
        {
            if($id > 0)
            {
                $batch = $this->product_model->getBatch(array('eb.id'=>$id));
                if(!empty($batch)) $batch = $batch[0];
            }
        }
        $arrProduct = $this->product_model->getProducts();
        $this->data['arrProduct'] = $arrProduct;
        $this->data['batch'] = $batch;

        $this->template->set_template('admin');
        if ($id == 0)
            $this->template->write('title', 'Easy Billing - Add Batch');
        else
            $this->template->write('title', 'Easy Billing - Edit Batch');
        $this->template->write_view('content', 'admin/batch/update', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Batch
     */
    public function delete($id = 0)
    {
        if($id > 0)
        {
            $batch['is_deleted'] = 1;
            $success = $this->product_model->saveBatchData($batch, $id);
            if($success) __red('batch/index');
        }
    }
}