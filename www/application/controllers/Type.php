<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Type extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function get_data_table_data() {
        $readonly = '' ;
        $where['ept.is_deleted'] = 0;
        $this->datatables->select('ept.name, ept.desc, ept.id', false)
                        ->where($where)
                        ->from('eb_product_type ept');

        echo $this->datatables->generate_products('UTF-8', 'type');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $type = array();
        $search_feilds = array(
            'name' => array('type' => 'text', 'lable' => 'Type Name')
        );
        $columns = array(
            'Name' => array('index' => 0, 'type' => 'text','width' => '50% !important '),
            'Description' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '40% !important '),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text'),
        );
        $db_fields_list = array('name','desc','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $type,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('type/get_data_table_data');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Type');
        $this->template->write_view('content', 'admin/type/index', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function update($id = 0)
    {
        $type = array();
        $this->data['id'] = $id;
        $this->data['type'] = array();
        if ($this->input->post())
        {
            $type = $this->input->post();
            $success = $this->product_model->saveDataType($type, $id);
            if($success) __red('type/index');
        }
        else
        {
            if($id > 0)
            {
                $type = $this->product_model->getType(array('ept.id'=>$id));
                if(!empty($type)) $type = $type[0];
            }
        }
        $this->data['type'] = $type;

        $this->template->set_template('admin');
        if ($id == 0)
            $this->template->write('title', 'Easy Billing - Add Type');
        else
            $this->template->write('title', 'Easy Billing - Edit Type');
        $this->template->write_view('content', 'admin/type/update', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function delete($id = 0)
    {
        if($id > 0)
        {
            $type['is_deleted'] = 1;
            $success = $this->product_model->saveDataType($type, $id);
            if($success) __red('type/index');
        }
    }
}