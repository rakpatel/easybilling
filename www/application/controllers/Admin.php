<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends App {

    public $dataTableObject = array();
    public $hasAccess = false;
    public $currentUser;
    
    public static $status = array('Rejected','Approved','Pending');
    public static $message =  "Record cann't be deleted have a dependent data in other table" ;   

    public function __construct() {
        parent::__construct();

        // Load all common model of admin here
        $this->load->model('app_model');
        $this->load->model('user_model');

        $controller = $this->uri->segment(1);
        $action = $this->uri->segment(2);
        if ($action != 'login' && $action != 'register') {
            $this->app_model->is_login();
        }
        if ($this->session->userdata('user'))
        {
            $user_data = $this->session->userdata('user');
            $user_accesible_pages = array();
            $exclude_actions_array = array('login', 'register','delete_product_image', 'getstates','forget_password','checkEmailAddressforgetPassword', 'getcities','getcities_front','getcitiesjson', 'dashboard', 'logout');
            $this->load->model('setting_model');
            $this->data['arrModules'] = explode(",", $this->setting_model->get_setting('enable_modules')[0]['value']);
            $this->data['user_data'] = $user_data;
            $this->template->write_view('header', 'admin/common/header', $this->data);
        }
    }

    public function cropImage($imagePath, $destinationPath, $startX, $startY, $width, $height) {
        $imagick = new \Imagick(realpath($imagePath));
        $imagick->cropImage($width, $height, $startX, $startY);
        $imagick->writeImage($destinationPath);
    }

    public function upload($filename, $path, $type = 'image') {
        
        $config = array();
        $extention = pathinfo($_FILES[$filename]['name'], PATHINFO_EXTENSION);
        //$config['file_name'] = str_replace('.' . $extention, '_' . time() . '.' . $extention, $_FILES[$filename]['name']);
        $config['file_name'] = time().'_'.rand(). '.' .$extention;
        $config['upload_path'] = $path;
        if ($type == 'image') {
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
        } else {
            $config['allowed_types'] = 'gif|jpg|png|jpeg|txt|pdf|doc|docx|csv|xls|xlsx';
        }
        $config['create_thumb'] = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload($filename);
        $upload_arr = $this->upload->data();
        if (isset($upload_arr) && !empty($upload_arr['file_name'])) {
            return $upload_arr['file_name'];
        } else {
            return false;
        }
    }

    public function resize($source_image, $destination, $tn_w, $tn_h, $quality = 100, $wmsource = false) {
        $color_code_1 = 255;
        $color_code_2 = 255;
        $color_code_3 = 255;

        $info = getimagesize($source_image);
        $imgtype = image_type_to_mime_type($info[2]);

        #assuming the mime type is correct
        switch ($imgtype) {
            case 'image/jpeg':
                $source = imagecreatefromjpeg($source_image);
                break;
            case 'image/gif':
                $source = imagecreatefromgif($source_image);
                break;
            case 'image/png':
                $source = imagecreatefrompng($source_image);
                break;
            default:
                die('Invalid image type.');
        }

        #Figure out the dimensions of the image and the dimensions of the desired thumbnail
        $src_w = imagesx($source);
        $src_h = imagesy($source);

        #Do some math to figure out which way we'll need to crop the image
        #to get it proportional to the new size, then crop or adjust as needed
        $x_ratio = $tn_w / $src_w;
        $y_ratio = $tn_h / $src_h;

        if (($src_w <= $tn_w) && ($src_h <= $tn_h)) {
            $new_w = $src_w;
            $new_h = $src_h;
        } elseif (($x_ratio * $src_h) < $tn_h) {
            $new_h = ceil($x_ratio * $src_h);
            $new_w = $tn_w;
        } else {
            $new_w = ceil($y_ratio * $src_w);
            $new_h = $tn_h;
        }

        $newpic = imagecreatetruecolor(round($new_w), round($new_h));
        $backgroundColor = imagecolorallocate($newpic, $color_code_1, $color_code_2, $color_code_3);

        imagefill($newpic, 0, 0, $backgroundColor);
        imagecopyresampled($newpic, $source, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
        $final = imagecreatetruecolor($tn_w, $tn_h);
        $backgroundColor = imagecolorallocate($final, $color_code_1, $color_code_2, $color_code_3);
        imagefill($final, 0, 0, $backgroundColor);
        imagecopy($final, $newpic, (($tn_w - $new_w) / 2), (($tn_h - $new_h) / 2), 0, 0, $new_w, $new_h);

        #if we need to add a watermark
        if ($wmsource) {
            #find out what type of image the watermark is
            $info = getimagesize($wmsource);
            $imgtype = image_type_to_mime_type($info[2]);

            #assuming the mime type is correct
            switch ($imgtype) {
                case 'image/jpeg':
                    $watermark = imagecreatefromjpeg($wmsource);
                    break;
                case 'image/gif':
                    $watermark = imagecreatefromgif($wmsource);
                    break;
                case 'image/png':
                    $watermark = imagecreatefrompng($wmsource);
                    break;
                default:
                    die('Invalid watermark type.');
            }

            #if we're adding a watermark, figure out the size of the watermark
            #and then place the watermark image on the bottom right of the image
            $wm_w = imagesx($watermark);
            $wm_h = imagesy($watermark);
            imagecopy($final, $watermark, $tn_w - $wm_w, $tn_h - $wm_h, 0, 0, $tn_w, $tn_h);
        }
        if (imagejpeg($final, $destination, $quality)) {
            return true;
        }
        return false;
    }

    public function crop($width, $height, $source_path, $destination) {

        list($source_width, $source_height, $source_type) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;
            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
        }

        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $width / $height;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            /*
             * Triggered when source image is wider
             */
            $temp_height = $height;
            $temp_width = (int) ($height * $source_aspect_ratio);
        } else {
            /*
             * Triggered otherwise (i.e. source image is similar or taller)
             */
            $temp_width = $width;
            $temp_height = (int) ($width / $source_aspect_ratio);
        }

        /*
         * Resize the image into a temporary GD image
         */

        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
                $temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
        );

        /*
         * Copy cropped region from temporary image into the desired GD image
         */

        $x0 = ($temp_width - $width) / 2;
        $y0 = ($temp_height - $height) / 2;
        $desired_gdim = imagecreatetruecolor($width, $height);
        imagecopy(
                $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $width, $height
        );

        /*
         * Render the image
         * Alternatively, you can save the image in file-system or database
         */
        imagejpeg($desired_gdim, $destination);
    }

    

}
