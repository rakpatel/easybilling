<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Purchase extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('Transaction_model');
        $this->load->model('company_model');
        $this->load->model('user_model');
    }

    public function get_data_table_data() {
        $user_data = $this->session->userdata('user');
        $this->datatables->select('p.purchase_invoice_id,ebc.name,p.purchase_date,p.discount_amount,p.total,p.id')
                        ->join('eb_company ebc', 'ebc.id = p.c_id', 'LEFT')
                        ->from('eb_purchase p');

        echo $this->datatables->generate_products('UTF-8', 'purchase');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $purchase = array();
        $search_feilds = array(
            'purchase_invoice_id' => array('type' => 'text', 'lable' => 'Purchase No.'),
            'name' => array('type' => 'text', 'lable' => 'Company Name'),
            'purchase_date' => array('type' => 'text', 'lable' => 'Purchase Date'),
        );
        $columns = array(
            'Purchase No.' => array('index' => 0, 'type' => 'text','width' => '10% !important '),
            'Company Name' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '50% !important '),
            'Purchase Date'       => array('index' => 2, 'type' => 'text','width' => '15% !important '),
            'Discount' =>array('index' => 4, 'width' => '5% !important ', 'type' => 'text'),
            'Total' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text'),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text', 'style'=>'width:10% !important;')
        );
        $db_fields_list = array('purchase_invoice_id','name','purchase_date','discount_amount','total','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $purchase,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('purchase/get_data_table_data');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Purchase');
        $this->template->write_view('content', 'admin/purchase/index', $this->data);
        $this->template->render();
    }
    
    public function create($id = 0) {
        $view = array();
        $arrProducts = $this->product_model->getProducts();
        $arrStockist = $this->company_model->getCompany(array('is_stockist'=>1));
        
        $arrProJson = array();
        foreach ($arrProducts as $key => $pro)
        {
            $resP['label'] = $pro['p_name'];
            $resP['value'] = $pro['p_name'].'_'.$pro['id'];
            $arrProJson[] = $resP;
        }

        $arrStoJson = array();
        foreach ($arrStockist as $key => $sto)
        {
            $resS['label'] = $sto['name'];
            $resS['value'] = $sto['name'].'_'.$sto['id'];
            $arrStoJson[] = $resS;
        }

        $this->data['arrProJson'] = json_encode($arrProJson);
        $this->data['arrStoJson'] = json_encode($arrStoJson);
        $this->template->set_template('admin');
        $this->template->write_view('content', 'admin/purchase/create', $this->data);
        $this->template->render();
    }

    public function getProduct()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $id = $arrPost['id'];
            $arrProduct = $this->product_model->getProducts(array('p.id'=>$id));
            if(isset($arrProduct[0])) echo json_encode($arrProduct[0]);
            else echo "error";
        }
    }

    public function addProduct()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $cart_total = 0;
            $arrCart = array();
            $arrCart = $this->session->userdata('cart');
            $cart_total = $this->session->userdata('cart_total');
            $cart_total = $cart_total + $arrPost['amount'];
            $cntCart = count($arrCart) + 1;
            $arrCart[$cntCart] = $arrPost;
            $this->session->set_userdata('cart', $arrCart);
            $this->session->set_userdata('cart_total', $cart_total);

            $html = '<tr>
                        <td><input type="hidden" name="index" id=index" value="'.$cntCart.'">'.$arrPost['p_name'].'</td>
                        <td>'.$arrPost['pack'].'</td>
                        <td>'.$arrPost['mfg'].'</td>
                        <td>'.$arrPost['batch'].'</td>
                        <td>'.$arrPost['exp'].'</td>
                        <td>'.$arrPost['qty'].'</td>
                        <td>'.$arrPost['mrp'].'</td>
                        <td>'.$arrPost['rate'].'</td>
                        <td>'.$arrPost['vat'].'</td>
                        <td>'.$arrPost['add'].'</td>
                        <td>'.$arrPost['net'].'</td>
                        <td>'.$arrPost['amount'].'</td>
                    </tr>';
            $retArray = json_encode(array('html'=>$html, 'cart_total'=>$cart_total));
            echo $retArray;
        }
    }

    public function removeProduct()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $arrCart = array();
            $arrCart = $this->session->userdata('cart');
            $arrCurrentProduct = $arrCart[$arrPost['index']];
            $cart_total = $this->session->userdata('cart_total');
            $cart_total = $cart_total - $arrCurrentProduct['amount'];
            unset($arrCart[$arrPost['index']]);
            $this->session->set_userdata('cart', $arrCart);
            $this->session->set_userdata('cart_total', $cart_total);
            
            $html = '';
            if(!empty($arrCart))
            {
                foreach ($arrCart as $key => $arrPost)
                {
                    $html .= '<tr>
                        <td><input type="hidden" name="index" id=index" value="'.$key.'">'.$arrPost['p_name'].'</td>
                        <td>'.$arrPost['pack'].'</td>
                        <td>'.$arrPost['mfg'].'</td>
                        <td>'.$arrPost['batch'].'</td>
                        <td>'.$arrPost['exp'].'</td>
                        <td>'.$arrPost['qty'].'</td>
                        <td>'.$arrPost['mrp'].'</td>
                        <td>'.$arrPost['rate'].'</td>
                        <td>'.$arrPost['vat'].'</td>
                        <td>'.$arrPost['add'].'</td>
                        <td>'.$arrPost['net'].'</td>
                        <td>'.$arrPost['amount'].'</td>
                    </tr>';
                }
            }
            
            echo $cart_total.'~~~'.$html;
        }
    }

    public function cancleOrder()
    {
        $arrCart = array();
        $this->session->set_userdata('cart', $arrCart);
        $this->session->set_userdata('cart_total', 0);
        echo 'success';
    }

    public function placeOrder()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $arrCart = $this->session->userdata('cart');
            $cart_total = $this->session->userdata('cart_total');
            if(!empty($arrCart))
            {
                $arrPost['subtotal'] = $cart_total;
                $arrPost['total'] = $cart_total;
                $arrPost['vat_amount'] = 0;
                $arrPost['add_vat_amount'] = 0;
                $arrPost['vat_id'] = 0;
                $arrPost['add_vat_id'] = 0;
                $arrPost['discount_amount'] = 0;
                $arrPost['discount_percent'] = 0;
                
                $purchase_id = $this->Transaction_model->savePurchaseData($arrPost, 0);
                foreach ($arrCart as $key => $pro)
                {
                    if($pro['batch'] != '')
                    {
                        $arrBatch = $this->product_model->getBatch(array('b_name'=>$pro['batch'], 'p_id'=>$pro['id']));
                        if(!empty($arrBatch)) $b_id = $arrBatch[0]['id'];
                        else
                        {
                            $b_data = array('id'=>0, 'b_name'=>$pro['batch'], 'p_id'=>$pro['id']);
                            $b_id = $this->product_model->saveBatchData($b_data, 0);
                        }
                    }
                    else $b_id = 0;
                    $pro['p_id'] = $pro['id'];
                    $pro['mrp'] = $pro['mrp'];
                    $pro['b_id'] = $b_id;
                    $pro['pur_id'] = $purchase_id;
                    $pro['total_price'] = $pro['amount'];

                    unset($pro['p_name']);
                    unset($pro['id']);
                    unset($pro['batch']);
                    unset($pro['pack']);
                    unset($pro['mfg']);
                    unset($pro['amount']);
                    $this->Transaction_model->savePurchaseDetailData($pro);
                    unset($pro['rate']);
                    unset($pro['mrp']);
                    unset($pro['total_price']);
                    unset($pro['exp']);
                    unset($pro['net']);
                    unset($pro['vat']);
                    unset($pro['add']);
                    $this->Transaction_model->saveStockDetailData($pro);
                }
            }
            $this->session->set_userdata('cart', array());
            $this->session->set_userdata('cart_total', 0);
        }
    }

    public function getDetail($pur_id)
    {
        $data = array();
        $data['arrPurchase'] = $this->Transaction_model->getPurchaseDetailData(array('p.id'=>$pur_id));
        $html = $this->load->view('admin/purchase/purchase_detail', $data, true);
        echo $html;exit;
    }

    /**
     * Delete purchase and related purchase detail and stock
     * @param type $pur_id
     */
    public function delete($pur_id)
    {
        $arrPurchase = $this->Transaction_model->getPurchaseDetailData(array('p.id'=>$pur_id));
        if(!empty($arrPurchase))
        {
            foreach ($arrPurchase as $key => $product)
            {
                $this->Transaction_model->delete_stock(array('pur_id'=>$product['pur_id'], 'p_id'=>$product['p_id'], 'b_id'=>$product['b_id']));
            }
            $this->Transaction_model->delete_purchase_details(array('pur_id'=>$pur_id));
            $this->Transaction_model->delete_purchase(array('id'=>$pur_id));
        }
        __red('purchase/index');
    }
}