<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Unit extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function get_data_table_data() {
        $readonly = '' ;
        $where['epu.is_deleted'] = 0;
        $this->datatables->select('epu.name, epu.symbol, epu.id', false)
                        ->where($where)
                        ->from('eb_product_unit epu');

        echo $this->datatables->generate_products('UTF-8', 'unit');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $unit = array();
        $search_feilds = array(
            'name' => array('type' => 'text', 'lable' => 'Unit Name')
        );
        $columns = array(
            'Name' => array('index' => 0, 'type' => 'text','width' => '70% !important '),
            'Symbol' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '20% !important '),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text'),
        );
        $db_fields_list = array('name','symbol','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $unit,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('unit/get_data_table_data');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Unit');
        $this->template->write_view('content', 'admin/unit/index', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function update($id = 0)
    {
        $unit = array();
        $this->data['id'] = $id;
        $this->data['unit'] = array();
        if ($this->input->post())
        {
            $unit = $this->input->post();
            $success = $this->product_model->saveDataUnit($unit, $id);
            if($success) __red('unit/index');
        }
        else
        {
            if($id > 0)
            {
                $unit = $this->product_model->getUnit(array('epu.id'=>$id));
                if(!empty($unit)) $unit = $unit[0];
            }
        }
        $this->data['unit'] = $unit;

        $this->template->set_template('admin');
        if ($id == 0)
            $this->template->write('title', 'Easy Billing - Add Unit');
        else
            $this->template->write('title', 'Easy Billing - Edit Unit');
        $this->template->write_view('content', 'admin/unit/update', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function delete($id = 0)
    {
        if($id > 0)
        {
            $unit['is_deleted'] = 1;
            $success = $this->product_model->saveDataUnit($unit, $id);
            if($success) __red('unit/index');
        }
    }
}