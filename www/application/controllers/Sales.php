<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Sales extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('Transaction_model');
        $this->load->model('company_model');
        $this->load->model('user_model');
    }

    public function get_data_table_data() {
        $user_data = $this->session->userdata('user');
        $this->datatables->select('s.invoice_id,ebu.name,s.sales_date,s.discount_amount,s.total,s.id')
                        ->join('eb_users ebu', 'ebu.id = s.u_id', 'LEFT')
                        ->from('eb_sales s');

        echo $this->datatables->generate_products('UTF-8', 'sales');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $sales = array();
        $search_feilds = array(
            'invoice_id' => array('type' => 'text', 'lable' => 'Purchase No.'),
            'name' => array('type' => 'text', 'lable' => 'Customer Name'),
            'sales_date' => array('type' => 'text', 'lable' => 'Sales Date'),
        );
        $columns = array(
            'Sale No.' => array('index' => 0, 'type' => 'text','width' => '10% !important '),
            'Customer Name' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '50% !important '),
            'Sales Date'       => array('index' => 2, 'type' => 'text','width' => '20% !important '),
            'Discount' =>array('index' => 4, 'width' => '5% !important ', 'type' => 'text'),
            'Total' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text'),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text', 'style'=>'width:10% !important;')
        );
        $db_fields_list = array('invoice_id','name','sales_date','discount_amount','total','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $sales,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('sales/get_data_table_data');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Sales');
        $this->template->write_view('content', 'admin/sales/index', $this->data);
        $this->template->render();
    }

    public function create($id = 0)
    {
        $view = array();
        $lastRec = $this->Transaction_model->getSaleData(array(), true);
        if(empty($lastRec)) $this->data['invoice_id'] = 'SB1';
        else
        {
            $next_id = $lastRec[0]['id']+1;
            $this->data['invoice_id'] = 'SB'.$next_id;
        }
        $arrCustomer = $this->user_model->getUsers(array('role_id'=>3));
        $arrDoctor = $this->user_model->getUsers(array('role_id'=>4));
        $arrProducts = $this->product_model->getProducts();
        $arrStockist = $this->company_model->getCompany(array('is_stockist'=>1));
        
        $arrProJson = array();
        foreach ($arrProducts as $key => $pro)
        {
            $resP['label'] = $pro['p_name'];
            $resP['value'] = $pro['p_name'].'_'.$pro['id'];
            $arrProJson[] = $resP;
        }

        $arrCusJson = array();
        foreach ($arrCustomer as $key => $cus)
        {
            $resC['label'] = $cus['name'];
            $resC['value'] = $cus['name'].'_'.$cus['id'];
            $arrCusJson[] = $resC;
        }

        $arrDocJson = array();
        foreach ($arrDoctor as $key => $cus)
        {
            $resD['label'] = $cus['name'];
            $resD['value'] = $cus['name'].'_'.$cus['id'];
            $arrDocJson[] = $resD;
        }

        $this->data['arrProJson'] = json_encode($arrProJson);
        $this->data['arrCusJson'] = json_encode($arrCusJson);
        $this->data['arrDocJson'] = json_encode($arrDocJson);
        $this->template->set_template('admin');
        $this->template->write_view('content', 'admin/sales/create', $this->data);
        $this->template->render();
    }

    public function getProduct()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $id = $arrPost['id'];
            $arrProduct = $this->product_model->getProducts(array('p.id'=>$id));
            if(isset($arrProduct[0])) echo json_encode($arrProduct[0]);
            else echo "error";
        }
    }

    public function getBatch()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $p_id = $arrPost['id'];
            $qty = $arrPost['qty'];
            $arrProductStock = $this->Transaction_model->getProductStock(array('es.p_id'=>$p_id));
            if(!empty($arrProductStock))
            {
                $arrUseBatch = array();
                $arrBatchName = array();
                $rQty = $qty;
                foreach ($arrProductStock as $key =>$batch)
                {
                    if($rQty > 0)
                    {
                        if($batch['qty'] > $rQty)
                        {
                            $sQty = $batch['qty'] - $rQty;
                            $rQty = 0;
                        }
                        else
                        {
                            $rQty = $rQty - $batch['qty'];
                            $sQty = 0;
                        }
                        $arrUseBatch[] = $batch['id'].'__'.$batch['b_id'].'__'.$sQty.'__'.$batch['net'].'__'.$batch['exp'].'__'.$batch['vat'].'__'.$batch['add'].'__'.$batch['mrp'];
                        $arrBatchName[] = $batch['b_name'];
                    }
                    else break;
                }
                $arrReturn = array('avail'=>$qty-$rQty, 'batches'=>implode(', ', $arrBatchName), 'arrUseBatch'=>$arrUseBatch);
                echo json_encode($arrReturn);
            }
            else echo "error";
        }
    }

    public function addProduct()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $cart_total = 0;
            $arrCart = array();
            $arrCart = $this->session->userdata('sale_cart');
            $cart_total = $this->session->userdata('sale_cart_total');
            $cart_total = $cart_total + $arrPost['amount'];
            $cntCart = count($arrCart) + 1;
            $arrCart[$cntCart] = $arrPost;
            $this->session->set_userdata('sale_cart', $arrCart);
            $this->session->set_userdata('sale_cart_total', $cart_total);

            $html = '<tr>
                        <td><input type="hidden" name="index" id=index" value="'.$cntCart.'">'.$arrPost['p_name'].'</td>
                        <td>'.$arrPost['pack'].'</td>
                        <td>'.$arrPost['mfg'].'</td>
                        <td>'.$arrPost['qty'].'</td>
                        <td>'.$arrPost['batch'].'</td>
                        <td>'.$arrPost['exp'].'</td>
                        <td>'.$arrPost['mrp'].'</td>
                        <td>'.$arrPost['dis'].'</td>
                        <td>'.$arrPost['damt'].'</td>
                        <td>'.$arrPost['amount'].'</td>
                    </tr>';
            $retArray = json_encode(array('html'=>$html, 'cart_total'=>$cart_total));
            echo $retArray;
        }
    }

    public function removeProduct()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $arrCart = array();
            $arrCart = $this->session->userdata('sale_cart');
            $arrCurrentProduct = $arrCart[$arrPost['index']];
            $cart_total = $this->session->userdata('sale_cart_total');
            $cart_total = $cart_total - $arrCurrentProduct['amount'];
            unset($arrCart[$arrPost['index']]);
            $this->session->set_userdata('sale_cart', $arrCart);
            $this->session->set_userdata('sale_cart_total', $cart_total);
            

            $html = '';
            if(!empty($arrCart))
            {
                foreach ($arrCart as $key => $arrPost)
                {
                    $html .= '<tr>
                                <td><input type="hidden" name="index" id=index" value="'.$key.'">'.$arrPost['p_name'].'</td>
                                <td>'.$arrPost['pack'].'</td>
                                <td>'.$arrPost['mfg'].'</td>
                                <td>'.$arrPost['qty'].'</td>
                                <td>'.$arrPost['batch'].'</td>
                                <td>'.$arrPost['exp'].'</td>
                                <td>'.$arrPost['mrp'].'</td>
                                <td>'.$arrPost['dis'].'</td>
                                <td>'.$arrPost['damt'].'</td>
                                <td>'.$arrPost['amount'].'</td>
                            </tr>';
                }
            }
            
            echo $cart_total.'~~~'.$html;
        }
    }

    public function cancleOrder()
    {
        $arrCart = array();
        $this->session->set_userdata('sale_cart', $arrCart);
        $this->session->set_userdata('sale_cart_total', 0);
        echo 'success';
    }

    public function placeOrder1()
    {
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Sales');
        $this->template->write_view('content', 'admin/sales/print_1', $this->data);
        $this->template->render();
    }
    public function placeOrder()
    {
        $arrPost = $this->input->post();
        if(!empty($arrPost))
        {
            $arrPrint = array();
            $arrCart = $this->session->userdata('sale_cart');
            $cart_total = $this->session->userdata('sale_cart_total');
            if(!empty($arrCart))
            {
                $arrPost['subtotal'] = $cart_total;
                $arrPost['total'] = $cart_total;
                $arrPost['vat_amount'] = 0;
                $arrPost['add_vat_amount'] = 0;
                $arrPost['vat_id'] = 0;
                $arrPost['add_vat_id'] = 0;
                $arrPost['discount_amount'] = 0;
                $arrPost['discount_percent'] = 0;

                $arrPrint['discount_amount'] = 0;
                $arrPrint['subtotal'] = $cart_total;
                $arrPrint['total'] = $cart_total;
                $arrPrint['invoice_id'] = $arrPost['invoice_id'];
                $arrPrint['sales_date'] = $arrPost['sales_date'];

                $dr_id = $arrPost['dr_id'];
                if($dr_id == '')
                {
                    $doctor_name = $arrPost['doctor_name'];
                    if($doctor_name != '')
                    {
                        $docData = array('username'=>$doctor_name, 'password'=>md5('admineasybilling'), 'name'=>$doctor_name, 'role_id'=>4, 'datetime_created'=>date('Y-m-d H:i:s'), 'datetime_modified'=>date('Y-m-d H:i:s'));
                        $dr_id = $this->user_model->update_users($docData);
                    }
                }
                $u_id = $arrPost['u_id'];
                if($u_id == '')
                {
                    $customer_name = $arrPost['customer_name'];
                    if($customer_name != '')
                    {
                        $cusData = array('username'=>$customer_name, 'password'=>md5('admineasybilling'), 'name'=>$customer_name, 'role_id'=>3, 'datetime_created'=>date('Y-m-d H:i:s'), 'datetime_modified'=>date('Y-m-d H:i:s'));
                        $u_id = $this->user_model->update_users($cusData);
                    }
                }
                
                $arrPrint['doctor_name'] = $arrPost['doctor_name'];
                $arrPrint['customer_name'] = $arrPost['customer_name'];

                $arrPost['dr_id'] = $dr_id;
                $arrPost['u_id'] = $u_id;
                unset($arrPost['doctor_name']);
                unset($arrPost['customer_name']);

                $arrPrintItem = array();
                $sales_id = $this->Transaction_model->saveSalesData($arrPost, 0);
                foreach ($arrCart as $key => $pro)
                {
                    $b_id = array();
                    if($pro['batch_info'] != '')
                    {
                        $arrBatchDetail = explode(",", $pro['batch_info']);
                        if(!empty($arrBatchDetail))
                        {
                            foreach ($arrBatchDetail as $key => $batchinfo)
                            {
                                $arrBatchDetail = explode("__", $batchinfo);
                                $s_id = $arrBatchDetail[0];
                                $b_id[] = $arrBatchDetail[1];
                                $b_qty = $arrBatchDetail[2];
                                $this->Transaction_model->saveStockDetailData(array('qty'=>$b_qty), $s_id);
                            }
                        }
                    }

                    $pro['p_id'] = $pro['id'];
                    $pro['b_id'] = implode(",", $b_id);
                    $pro['s_id'] = $sales_id;
                    $pro['total_price'] = $pro['amount'];

                    $arrPrintItem[] = $pro;

                    unset($pro['p_name']);
                    unset($pro['id']);
                    unset($pro['rate']);
                    unset($pro['batch']);
                    unset($pro['pack']);
                    unset($pro['mfg']);
                    unset($pro['amount']);
                    //unset($pro['exp']);
                    unset($pro['net']);
                    unset($pro['vat']);
                    unset($pro['add']);
                    unset($pro['batch_info']);
                    $this->Transaction_model->saveSalesDetailData($pro);
              }
              $arrPrint['arrPrintItem'] = $arrPrintItem;
            }
            $this->session->set_userdata('sale_cart', array());
            $this->session->set_userdata('sale_cart_total', 0);
            $this->printBill($arrPrint);
        }
    }

    public function printBill($arrPrint)
    {
        $this->load->model('setting_model');
        $arrPrint['business_name'] = $this->setting_model->get_setting('business_name')[0]['value'];
        $arrPrint['address'] = $this->setting_model->get_setting('address')[0]['value'];
        $arrPrint['footer_message'] = $this->setting_model->get_setting('footer_message')[0]['value'];
        $product_print_column = explode(",",$this->setting_model->get_setting('product_print_column')[0]['value']);
        $arrPrint['product_print_column'] = $product_print_column;
        $arrPrint['cnt_product_print_column'] = count($product_print_column)+1;
        
        $arrPrint['tin_no'] = $this->setting_model->get_setting('tin_no')[0]['value'];

        $htmlPrint = $this->load->view('admin/sales/print', $arrPrint, true);
        echo $htmlPrint;exit;
    }

    public function getDetail($sales_id)
    {
        $data = array();
        $data['arrSales'] = $this->Transaction_model->getSaleDetailData(array('s.id'=>$sales_id));
        $html = $this->load->view('admin/sales/sale_detail', $data, true);
        echo $html;exit;
    }
}