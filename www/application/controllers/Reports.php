<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends Admin {

    public function __construct() {
        parent::__construct();
        $this->template->write('meta_keywords', 'Tours and Travels');
        $this->template->write('meta_description', 'Tours and Travels');
        $this->load->model('product_model');
        $this->load->model('Transaction_model');
        $this->load->model('company_model');
        $this->load->model('user_model');
    }

    /**
     * Different types of Reports
     */
    public function index() {
        $user_data = $this->session->userdata('user');
        $reportData = array();
        $reportFieldData = array();
        if ($this->input->post()) {
            $data = $this->input->post();
            $select = "";
            $where = "";
            $group = "";
            $join = "";
            switch ($data['report_type']) {
                case 'sales' : 
                    if($data['from_date'] != '') $where .= "sales_date >=  '" . $data['from_date'] . "' ";
                    if($data['to_date'] != '') $where .= " AND sales_date <=  '" . $data['to_date'] . "' ";
                    $reportFieldData = array('Invoice No.', 'Customer Name', 'Sales Date', 'Discount', 'Total');
                    $reportData = $this->Transaction_model->getSaleData($where);
                    break;
                case 'purchase' : 
                    if($data['from_date'] != '') $where .= "purchase_date >=  '" . $data['from_date'] . "' ";
                    if($data['to_date'] != '') $where .= " AND purchase_date <=  '" . $data['to_date'] . "' ";
                    $reportFieldData = array('Purchase No.', 'Company Name', 'Purchase Date', 'Discount', 'Total');
                    $reportData = $this->Transaction_model->getPurchaseData($where);
                    break;
            }
            
            $this->data['reportData'] = $reportData;
            $this->data['reportFieldData'] = $reportFieldData;
            $this->data['data'] = $data;
        }
        $this->template->write('title', 'Easy Billing - Manage Reports');
        $this->template->write_view('content', 'admin/report/index', $this->data);
        $this->template->render();
    }
    
    /**
     * Different types of Commission Reports
     */
    public function commission_report() {
        $user_data = $this->session->userdata('user');
        $this->data['role_id'] = $user_data['role_id'] ;
        
        if ($this->input->post()) {
            $data = $this->input->post();
            $select = "";
            $where = " booking_product.status IN ('confirmed','confirmreschedule', 'canclelledin72hr' )";
            $group = "";
            $join = "";
            if($user_data['role_id']!=3) {
                $pIds = $this->db->select('id')->where('supplier_id = ' . $data['supplier_id'])->get('product')->result_array();
            } else {
                $pIds = $this->db->select('id')->where('supplier_id = ' . $user_data['id'])->get('product')->result_array();
            }
            $currencySelected = $this->db->select('currency.currency_code')->where('user_id = ' . $data['supplier_id'])->join('currency','currency.id =supplier_detail.currency_id','inner')->get('supplier_detail')->row_array();
            //echo $this->db->last_query();
            foreach ($pIds as $pId) {
                $productIds[] = $pId['id'];
            }
            //pr($currencySelected,false);
            $this->db->join('product'," booking_product.product_id = product.id ",'inner');
            
            $select = " DATE_FORMAT(date_booking,'%b-%Y') as months , booking_product.* , booking_payment.* , product.product_name";
            $where .= ' AND booking_product.product_id  IN ( ' . implode(',', $productIds) . " ) ";
            //$where .= " AND booking_payment.currency_id =  " . $data['currencies'] . " ";
            if($user_data['role_id']!=3) {
                //5f6e4eb29519b9bb63a28bc8688e83bd
                if(isset($data['product_id']) && !empty($data['product_id'])) {
                    $where .= " AND booking_product.product_id =  " . $data['product_id'] . " ";
                }
            }
            $where .= " AND DATE_FORMAT(date_booking,'%b-%Y') =  '" . $data['month'] . "'";
            
            $group = "";
            
            $reportData = $this->report_model->report_data($select,$where,$group,$join);
            //echo $this->db->last_query();
            
            foreach($reportData as &$rdata) {
                $orderData = json_decode($rdata['json_data'],true);
                $rdata['commission_rate'] = $this->user_model->get_commission_rate($orderData['product']['supplier_id']);
            }
            $this->data['reportData'] = $reportData;
            $this->data['data'] = $data;
            
            
            if($user_data['role_id']!=3) {
                $where = array();
                $where['product.status'] = 1;
                if(isset($data['supplier_id']) && !empty($data['supplier_id'])) {
                    $where['product.supplier_id'] = $data['supplier_id'];
                }
                $this->data['products'] = $this->product_model->get_products($where);
            }
            
        }
        
        $this->db->order_by('company_name');
        $this->data['suppliers'] = $this->user_model->get_suppliers(array('user.status'=>1));
        $this->data['currencies'] = $this->currency_model->get_currencies(array('is_display' => '1','status'=>'1'));
        $this->data['currencySelected'] = $currencySelected;
        
        $this->template->write('title', 'Easy Billing - Manage Commission Reports');
        $this->template->write_view('content', 'admin/report/commission_report', $this->data);
        $this->template->render();
    }
    
    public function generate_pdf($data) {
        $user_data = $this->session->userdata('user');
        $this->data['role_id'] = $user_data['role_id'] ;
        
        if ($data) {
            $data = json_decode(base64_decode($data),true);
            $select = "";
            $where = " booking_product.status IN ('confirmed','confirmreschedule', 'canclelledin72hr' )";
            $group = "";
            $join = "";
            if($user_data['role_id']!=3) {
                $pIds = $this->db->select('id')->where('supplier_id = ' . $data['supplier_id'])->get('product')->result_array();
            } else {
                $pIds = $this->db->select('id')->where('supplier_id = ' . $user_data['id'])->get('product')->result_array();
            }
            $currencySelected = $this->db->select('currency.currency_code')->where('user_id = ' . $data['supplier_id'])->join('currency','currency.id =supplier_detail.currency_id','inner')->get('supplier_detail')->row_array();
            //echo $this->db->last_query();
            foreach ($pIds as $pId) {
                $productIds[] = $pId['id'];
            }
            //pr($currencySelected,false);
            $this->db->join('product'," booking_product.product_id = product.id ",'inner');
            
            $select = " DATE_FORMAT(date_booking,'%b-%Y') as months , booking_product.* , booking_payment.* , product.product_name";
            $where .= ' AND booking_product.product_id  IN ( ' . implode(',', $productIds) . " ) ";
            //$where .= " AND booking_payment.currency_id =  " . $data['currencies'] . " ";
            if($user_data['role_id']!=3) {
                //5f6e4eb29519b9bb63a28bc8688e83bd
                if(isset($data['product_id']) && !empty($data['product_id'])) {
                    $where .= " AND booking_product.product_id =  " . $data['product_id'] . " ";
                }
            }
            $where .= " AND DATE_FORMAT(date_booking,'%b-%Y') =  '" . $data['month'] . "'";
            
            $group = "";
            
            $reportData = $this->report_model->report_data($select,$where,$group,$join);
            //echo $this->db->last_query();
            
            foreach($reportData as &$rdata) {
                $orderData = json_decode($rdata['json_data'],true);
                $rdata['commission_rate'] = $this->user_model->get_commission_rate($orderData['product']['supplier_id']);
            }
            $this->data['reportData'] = $reportData;
            $this->data['data'] = $data;
            
            
            if($user_data['role_id']!=3) {
                $where = array();
                $where['product.status'] = 1;
                if(isset($data['supplier_id']) && !empty($data['supplier_id'])) {
                    $where['product.supplier_id'] = $data['supplier_id'];
                }
                $this->data['products'] = $this->product_model->get_products($where);
            }
            
        }
        
        $this->data['suppliers'] = $this->user_model->get_suppliers(array('user.status'=>1));
        $this->data['currencies'] = $this->currency_model->get_currencies(array('is_display' => '1','status'=>'1'));
        $this->data['currencySelected'] = $currencySelected;
        
        //$this->template->write_view('content', 'admin/report/generate_pdf', $this->data);
        $pdf_html = $this->load->view('admin/report/generate_pdf', $this->data, true);
        //echo $pdf_html;exit;
        $this->generatePDF($pdf_html, getcwd() . '/assets/invoice/Commission_Report_' . time() . '.pdf', 'FD');
        
    }
    
    /**
     * Get Supplier Products Ajax
     */
    public function get_supplier_products($id) {
        if($id) {
            $where['product.supplier_id'] = $id;
            $products = $this->product_model->get_products($where);
            echo '<option value=""> -- Select Product -- </option>';
            foreach ($products as $product) {
                echo '<option value="' . $product['id'] . '">' . $product['product_name'] . '</option>';
            }
        } else {
            echo '<option value=""> -- Select Product -- </option>';
        }
        exit;
    }
    
    /**
     * Index Banner for this controller.
     */
    public function product_view() {
        $user_data = $this->session->userdata('user');
        $where = array();
        if($user_data['role_id']==3)
        $where = array('product.supplier_id'=>$user_data['id']);    
        
        $productview = $this->report_model->get_product_view($where);
        
        foreach ($productview as $key => &$arrView) {
            $arrView['datetime_modifiedfilter'] = date('Ymd', strtotime($arrView['datetime_modified']));
        }
        $this->data['productview'] = $productview;
        //_p($arrView,1);
        $this->dataTableObject = array(
            'columns' => array(
                'Product Name' => array('index' => 1, 'width' => '350px', 'type' => 'text', 'class' => 'id_class'),
                'Count' => array('index' => 2, 'width' => '60px', 'type' => 'text', 'class' => 'id_class'),
                'Last View On' => array('index' => 3, 'width' => '40px', 'type' => 'text'),
                'datetime_modifiedfilter' => array('index' => 6, 'type' => 'hidden'),
              ),
            'search_feilds' => array(
                'product_name' => array('type' => 'text', 'lable' => 'Product Name','class'=>'product_name'),
                'counter' => array('type' => 'text', 'lable' => 'Count','class'=>''),
                'datetime_modifiedfilter' => array('type' => 'daterange', 'lable' => 'Date', 'class' => 'default-date-picker datetime_modifiedfilter'),
            ),
            'records' => $productview,
            'db_fields_list' => array('product_name','counter','datetime_modified','datetime_modifiedfilter'),
            'table_id' => 'tblProductView',
            'sort_field' => array(),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = $this->config->item('base_url').'index.php/report/get_data_table_data';
        //_p($this->data,1) ;
        $this->template->write('title', 'Easy Billing - Manage Product View Report');
        $this->template->write_view('content', 'admin/report/product_view', $this->data);
        $this->template->render();
    }
    
    
    public function get_data_table_data() {

        $this->datatables->select("product_click_report.counter,DATE_FORMAT(product_click_report.datetime_modified,'%d-%m-%Y') as datetime_modified,product.product_name")
                ->join('product', 'product.id=product_click_report.product_id')
                //->unset_column('cities.city_id')
                ->from('product_click_report');


        echo $this->datatables->generate();
        die;
    }
    
    public function exportToExcel($id=null, $is_group=null) {
        $columns = array();
        $columns = array(
            array('headertext' => 'Product Name', 'datatype' => 'string', 'datafield' => 'product_name', 'width' => '200px', 'ishidden' => false),
            array('headertext' => 'Counter', 'datatype' => 'string', 'datafield' => 'counter', 'width' => '100px', 'ishidden' => false),
            //array('headertext' => 'Last Modified on', 'datatype' => 'string', 'datafield' => 'datetime_modified', 'width' => '200px', 'ishidden' => false),
        );
        $arrReport = array();
        //$where = array();
        $data = $this->input->post();
        $sqlQuery= '';
        if(!empty($data['product_name'])){
           if($sqlQuery!='')
            $sqlQuery.=" AND product.product_name='{$data['product_name']}'";
            else 
            $sqlQuery.=" product.product_name LIKE '{$data['product_name']}%'";    
        }   
        if(!empty($data['counter'])){
            if($sqlQuery!='')
            $sqlQuery.=" AND product_click_report.counter={$data['counter']}";
            else 
            $sqlQuery.=" product_click_report.counter={$data['counter']}";   
        }
        if(!empty($data['dateFrom']) && empty($data['dateTo'])){
            $dateFrom= date(DEFINE_DATE_Ymd_FORMATE,strtotime($data['dateFrom']));
            $dateTo  = date('Y-m-d H:i:s');
            if($sqlQuery!='')
            $sqlQuery.=" AND product_click_report.datetime_modified BETWEEN '$dateFrom' AND '$dateTo' ";
            else 
            $sqlQuery.=" AND product_click_report.datetime_modified BETWEEN '$dateFrom' AND '$dateTo' ";
        }
        if(empty($data['dateFrom']) && !empty($data['dateTo'])){
            //$dateFrom= date(DEFINE_DATE_Ymd_FORMATE,strtotime($data['dateFrom']));
            $dateTo  = date('Y-m-d');
            if($sqlQuery!='')
            $sqlQuery.=" AND DATE(product_click_report.datetime_modified) ='$dateTo' ";
            else 
            $sqlQuery.=" DATE(product_click_report.datetime_modified) ='$dateTo' ";
        }
        if(!empty($data['dateFrom']) && !empty($data['dateTo'])){
            $dateFrom= date(DEFINE_DATE_Ymd_FORMATE,strtotime($data['dateFrom']));
            $dateTo  = date(DEFINE_DATE_Ymd_FORMATE,strtotime($data['dateTo']));
            if($sqlQuery!='')
            $sqlQuery.=" AND product_click_report.datetime_modified BETWEEN '$dateFrom' AND '$dateTo' ";
            else 
            $sqlQuery.=" AND product_click_report.datetime_modified BETWEEN '$dateFrom' AND '$dateTo' ";
        }
        
        $arrReport = $this->report_model->get_product_view_export($sqlQuery);
        if (!empty($arrReport))
            $message = "product reported export successfully";
        else
            $message = "Error: No eort available";
        $arrReturn = json_encode(array('message' => $message, 'columns' => $columns, 'arrReport' => $arrReport));
        echo $arrReturn;
        exit;
    }

    /**
     * Add/Edit Banner
     */
    
    
    
    public function update($id = 0) {
        
        $countries = $this->location_model->get_countries();
        $countrylist = array();
        foreach ($countries as $country):
            $countrylist[$country['country_id']] = $country['country_name'];
        endforeach;
        $this->data['countries'] = $countrylist ;
        $this->data['categories'] = $this->category_model->get_categories();
        $this->data['attractions'] = $this->top_attractions_model->get_top_attractions();
        if ($this->input->post()) {
            $data = $this->input->post();
            if($image_name = $this->upload('image_name', BANNER_IMAGE_PATH)) 
                $data['banner'] = $image_name;
            if (!empty($data['id'])) {
                $this->country_banner_model->update_banner($data, array('id' => $id));
                $this->session->set_flashdata('success', 'Record Updated Successfully.');
            } else {
                $this->country_banner_model->update_banner($data);
                $this->session->set_flashdata('success', 'Record Added Successfully.');
            }
            redirect('countrybanner');
        }
        $countrybanner = $this->country_banner_model->get_banners(array('country_banners.id' => $id));

        if ((isset($countrybanner) && count($countrybanner) == 0) && $id != 0) {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect('countrybanner');
        } else {
            if (isset($countrybanner) && count($countrybanner) != 0) {
                $this->data['countrybanner'] = $countrybanner[0];
                $this->data['cities'] = $this->location_model->get_cities(array('cities.country_id' => $countrybanner[0]['country_id']));
            } else {
                $this->data['countrybanner'] = array();
            }
        }
        $this->data['id'] = $id;
        if ($id == 0) {
            $this->template->write('title', 'Easy Billing - Add Landing Page Banner');
        } else {
            $this->template->write('title', 'Easy Billing - Edit Landing Page Banner');
        }
        $this->template->write_view('content', 'admin/country_banner/update', $this->data);
        $this->template->render();
    }

    /**
     * Delete Banner
     */
    public function delete($id = 0) {
        if ($this->app_model->update_delete_status('country_banners', array('id' => $id))) {
            $this->session->set_flashdata('success', 'Record Deleted Successfully.');
            redirect('countrybanner');
        } else {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect('countrybanner');
        }
    }
    
    public function delete_image(){
        if(($this->input->is_ajax_request())) {
            $this->country_banner_model->deletImage();
        }
    }
}