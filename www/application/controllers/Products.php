<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('company_model');
    }

    public function get_data_table_data() {
        $user_data = $this->session->userdata('user');
        $readonly = '' ;
        $where['p.is_deleted'] = 0;
        $this->datatables->select('p.p_name,p.p_code, ept.name type_name, epu.name unit_name, ec.name m_name, p.id')
                        ->join('eb_product_type ept', 'ept.id = p.type_id', 'LEFT')
                        ->join('eb_product_unit epu', 'epu.id = p.unit_id', 'LEFT')
                        ->join('eb_company ec', 'ec.id = p.manufacturer_id', 'LEFT')
                        ->where($where)
                        ->from('eb_product p');

        echo $this->datatables->generate_products('UTF-8', 'products');
    }

    /**
     * Index Page for this controller.
     */
    public function index() {
        $products = array();
        $search_feilds = array(
            'p_name' => array('type' => 'text', 'lable' => 'Product Name'),
            'p_code' => array('type' => 'text', 'lable' => 'Reference Code'),
        );
        $columns = array(
            'Product Name' => array('index' => 0, 'type' => 'text','width' => '35% !important '),
            'Code' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '30% !important '),
            'Type'       => array('index' => 2, 'type' => 'text','width' => '5% !important '),
            'Unit'=>array('index' => 3, 'type' => 'text','width' => '5% !important '),
            'Manufacturer' =>array('index' => 4, 'width' => '20% !important ', 'type' => 'text'),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text', 'style'=>'width:10% !important;'),
        );
        $db_fields_list = array('p_name','p_code','type_name','unit_name','m_name','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $products,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('products/get_data_table_data');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Products');
        $this->template->write_view('content', 'admin/products/index', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function update($id = 0)
    {
        $product = array();
        $this->data['id'] = $id;
        $this->data['product'] = array();
        $user_data = $this->session->userdata('user');
        if ($this->input->post())
        {
            $product = $this->input->post();
            $success = $this->product_model->saveData($product, $id);
            if($success) __red('products/index');
        }
        else
        {
            if($id > 0)
            {
                $product = $this->product_model->getProducts(array('p.id'=>$id));
                if(!empty($product)) $product = $product[0];
            }
        }
        $arrTypes = $this->product_model->getType();
        $arrUnits = $this->product_model->getUnit();
        $arrManu = $this->company_model->getCompany(array('is_stockist'=>0, 'is_deleted'=>0));
        
        $this->data['arrTypes'] = $arrTypes;
        $this->data['arrUnits'] = $arrUnits;
        $this->data['arrManu'] = $arrManu;
        $this->data['product'] = $product;

        $this->template->set_template('admin');
        if ($id == 0)
            $this->template->write('title', 'Easy Billing - Add Product');
        else
            $this->template->write('title', 'Easy Billing - Edit Product');
        $this->template->write_view('content', 'admin/products/update', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function delete($id = 0)
    {
        if($id > 0)
        {
            $product['is_deleted'] = 1;
            $success = $this->product_model->saveData($product, $id);
            if($success) __red('products/index');
        }
    }

    /**
     * Check if product exists or not
     */
    public function checkname($type)
    {
        if($type == 'product')
            $product = $this->product_model->getProducts(array('p_name' => $_REQUEST['p_name']));
        else
            $product = $this->product_model->getProducts(array('p_code' => $_REQUEST['p_code']));
        if (isset($product) && !empty($product)) echo 'false';
        else echo 'true';
    }

    /**
     * Display stock for current product
     * @param type $p_id
     */
    public function getStockDetail($p_id)
    {
        $this->load->model('Transaction_model');
        $data = array();
        $data['arrStock'] = $this->Transaction_model->getProductStock(array('es.p_id'=>$p_id));
        $html = $this->load->view('admin/products/product_stock_detail', $data, true);
        echo $html;exit;
    }

    public function get_data_table_data_stock() {
        $user_data = $this->session->userdata('user');
        $readonly = '' ;
        $this->datatables->select('ep.p_name, ep.p_code, sum(es.qty) as tqty, ep.id')
                        ->join('eb_product ep', 'ep.id = es.p_id', 'LEFT')
                        ->group_by('es.p_id')
                        ->from('eb_stock es');

        echo $this->datatables->generate_products('UTF-8', 'stock');
    }

    /**
     * Index Page for this controller.
     */
    public function stock() {
        $products = array();
        $search_feilds = array(
            'p_name' => array('type' => 'text', 'lable' => 'Product Name'),
            'p_code' => array('type' => 'text', 'lable' => 'Reference Code'),
        );
        $columns = array(
            'Product Name' => array('index' => 0, 'type' => 'text','width' => '35% !important '),
            'Code' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '30% !important '),
            'QTY' => array('index' => 2, 'type' => 'text','width' => '5% !important '),
            'Action' => array('index' => 5, 'width' => '5% !important ', 'type' => 'text', 'style'=>'width:10% !important;'),
        );
        $db_fields_list = array('p_name','p_code','tqty','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $products,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('products/get_data_table_data_stock');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Stock');
        $this->template->write_view('content', 'admin/products/stock', $this->data);
        $this->template->render();
    }
}