<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User extends Admin
{
    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index()
    {
        __red('user/login');
    }

    /**
     * Login Action for Admin
     * @throws Exception
     */
    public function login() {
        $view = array();
        if ($_POST)
        {
            $arrVal = $this->setting_model->get_setting('trial');
            $db_val = $arrVal[0]['value'];
            if($db_val == 'true')
            {
                $arrVal = $this->setting_model->get_setting('start_date');
                $time = strtotime($arrVal[0]['value']);
                $final_date = date("Y-m-d", strtotime("+1 month", $time));
                if($final_date < date('Y-m-d'))
                {
                    echo "Your trial period expires, please contact support person to continue...";
                    exit;
                }
            }

            $view['data'] = $this->input->post();
            if (!isset($_POST['username']) || !isset($_POST['password'])) {
                throw new Exception('Invalid credentials given!');
            }
            $user_data = $this->user_model->check_login($_POST['username'], $_POST['password']);
            if ($user_data)
            {
                $this->session->set_userdata('user', $user_data); // Save data on user's session.
                //$this->session->set_flashdata('success', 'You are logged in successfully.');
                __red('sales/create');
            } else {
                $this->session->set_flashdata('error', 'Please enter correct username or password.');
            }
        }

        $this->template->set_template('login');
        $this->template->write_view('content', 'admin/user/index', $view);
        $this->template->render();
    }

    /**
     * Logout and destroy user session
     */
    public function logout() {
        $this->session->sess_destroy();
        __red('user/login');
    }

    /**
     * User Dashboard action
     */
    public function dashboard() {
        $this->template->write('title', 'Easy Billing - Dashboard');
        $this->template->write_view('content', 'admin/user/dashboard');
        $this->template->render();
    }

    /**
     * User Profile action
     */
    public function profile() {
        $logIndata = $this->session->userdata('user');
        if ($this->input->post()) {
            $userdata = $this->input->post();
            $logIndata = $this->session->userdata('user');
            $data = array();
            $data['username'] = $userdata['username'];
            $data['name'] = $userdata['name'];
            if($userdata['password'] != '') $data['password'] = md5($userdata['password']);
            $where = array('id' => $logIndata['id']);
            $this->user_model->update_users($data,$where);
            
            $logIndata['username'] = $userdata['username'];
            $logIndata['name'] = $userdata['name'];
            $this->session->set_userdata('user', $logIndata); // Save data on user's session.
            
            $this->session->set_flashdata('success', 'User Data Updated Successfully.');
            __red('user/profile');
        }
        $user = $this->user_model->getUsers(array('eu.id' => $logIndata['id']));
        $this->data['user_data'] = $user[0];
        $this->template->write('title', 'Easy Billing - User Profile');
        $this->template->write_view('content', 'admin/user/profile', $this->data);
        $this->template->render();
    }

    /**
     * User Dashboard action
     */
    public function change_password() {
        if ($this->input->post()) {
            $userdata = $this->input->post();
            $logIndata = $this->session->userdata('user');
            $data = array();
            $data['password'] = md5($userdata['password']);
            
            $where = array('id' => $logIndata['id']);
            if(!empty($userdata['password'])) {
                $this->user_model->update_users($data,$where);
                $this->session->set_flashdata('success', 'Password Changed Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Please provide correct password');
            }
            redirect('user/change_password');
        }
        $this->template->write('title', 'Easy Billing - Change Password');
        $this->template->write_view('content', 'admin/user/change_password');
        $this->template->render();
    }

    public function get_data_table_data($type = 'customer')
    {
        $user_data = $this->session->userdata('user');
        $readonly = '' ;
        if($type == 'customer') $where['ebu.role_id'] = 3;
        elseif($type == 'doctor') $where['ebu.role_id'] = 4;
        else $where['ebu.role_id'] = 2;

        $this->datatables->select('ebu.name, epd.number, epd.email, ebu.id')
                        ->join('eb_user_details epd', 'ebu.id = epd.u_id', 'LEFT')
                        ->where($where)
                        ->from('eb_users ebu');

        echo $this->datatables->generate_products('UTF-8', $type);
    }

    /**
     * Index Page for this controller.
     */
    public function index_doctor() {
        $doctors = array();
        $search_feilds = array(
            'name' => array('type' => 'text', 'lable' => 'Doctor Name'),
            'number' => array('type' => 'text', 'lable' => 'Number'),
            'email' => array('type' => 'text', 'lable' => 'Email'),
        );
        $columns = array(
            'Doctor Name' => array('index' => 0, 'type' => 'text','width' => '35% !important '),
            'Number' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '30% !important '),
            'Email'       => array('index' => 2, 'type' => 'text','width' => '5% !important ')
        );
        $db_fields_list = array('name','number','email','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $doctors,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('user/get_data_table_data/doctor');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Doctor');
        $this->template->write_view('content', 'admin/user/index_doctor', $this->data);
        $this->template->render();
    }

    /**
     * Index Page for this controller.
     */
    public function index_customer() {
        $customer = array();
        $search_feilds = array(
            'name' => array('type' => 'text', 'lable' => 'Customer Name'),
            'number' => array('type' => 'text', 'lable' => 'Number'),
            'email' => array('type' => 'text', 'lable' => 'Email'),
        );
        $columns = array(
            'Customer Name' => array('index' => 0, 'type' => 'text','width' => '35% !important '),
            'Number' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '30% !important '),
            'Email'       => array('index' => 2, 'type' => 'text','width' => '5% !important ')
        );
        $db_fields_list = array('name','number','email','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $customer,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('user/get_data_table_data/customer');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Customer');
        $this->template->write_view('content', 'admin/user/index_customer', $this->data);
        $this->template->render();
    }

    /**
     * Index Page for this controller.
     */
    public function index_admin() {
        $customer = array();
        $search_feilds = array(
            'name' => array('type' => 'text', 'lable' => 'Customer Name'),
            'number' => array('type' => 'text', 'lable' => 'Number'),
            'email' => array('type' => 'text', 'lable' => 'Email'),
        );
        $columns = array(
            'Admin Name' => array('index' => 0, 'type' => 'text','width' => '35% !important '),
            'Number' => array('index' => 1, 'type' => 'text','class'=>'widthClass','width' => '30% !important '),
            'Email'       => array('index' => 2, 'type' => 'text','width' => '5% !important ')
        );
        $db_fields_list = array('name','number','email','action');

        $this->dataTableObject = array(
            'columns' => $columns,
            'search_feilds' => $search_feilds,
            'records' => $customer,
            'db_fields_list' => $db_fields_list,
            'table_id' => 'tblProductList',
            'sort_field' => array(1),
            'sort_field_default' => array(1, 'asc')
        );
        $this->data['dataTableObject'] = $this->dataTableObject;
        $this->data['ajaxUrl'] = __gurl('user/get_data_table_data/admin');
        $this->template->set_template('admin');
        $this->template->write('title', 'Easy Billing - Manage Admin');
        $this->template->write_view('content', 'admin/user/index_admin', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function update($id = 0, $role_id = 3)
    {
        $user = array();
        $this->data['id'] = $id;
        $this->data['user'] = array();
        $user_data = $this->session->userdata('user');
        
        if ($this->input->post())
        {
            $user = $this->input->post();
            $user_label_post = $user['role_id'] == 3 ? "Customer" : "Doctor";
            $success = $this->user_model->update_users($user, array("id"=>$id));
            if($success) __red('user/index_'.$user_label_post);
        }
        else
        {
            if($id > 0)
            {
                $user = $this->user_model->getUsers(array('u.id'=>$id));
                if(!empty($user)) $user = $user[0];
            }
        }
        $user_label = $role_id == 3 ? "Customer" : "Doctor";
        $this->data['role_id'] = $role_id;
        $this->data['user_label'] = $user_label;

        $this->template->set_template('admin');
        if ($id == 0)
            $this->template->write('title', 'Easy Billing - Add '.$user_label);
        else
            $this->template->write('title', 'Easy Billing - Edit '.$user_label);
        $this->template->write_view('content', 'admin/user/update', $this->data);
        $this->template->render();
    }

    /**
     * Add/Edit Product
     */
    public function delete($id = 0)
    {
        if($id > 0)
        {
            $product['is_deleted'] = 1;
            $success = $this->user_model->saveData($product, $id);
            if($success) __red('products/index');
        }
    }

    /**
     * Check if user exists or not
     */
    public function checkname()
    {
        $user = $this->user_model->getUsers(array('username' => $_REQUEST['username']));
        if (isset($user) && !empty($user)) echo 'false';
        else echo 'true';
    }
}