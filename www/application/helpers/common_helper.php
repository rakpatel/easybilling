<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('__t')) {

    function __t($text) {
        $CI = &get_instance();
        $trans_text = $CI->lang->line($text);
        if (!$trans_text) {
            echo $text;
        } else {
            echo $trans_text;
        }
    }

}
if (!function_exists('crypto_rand_secure')) {
    function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }
}
if (!function_exists('getToken')) {
    function getToken($length) {
        //$flag = true;
        //while($flag) {
            $token = "";
            $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
            $codeAlphabet.= "0123456789";
            $max = strlen($codeAlphabet) - 1;
            for ($i=0; $i < $length; $i++) {
                $token .= $codeAlphabet[crypto_rand_secure(0, $max)];
            }
            
        //}
        return $token;
    }
}

if (!function_exists('strP')) {
    function strP($data=null){
        $content  = str_pad($data,8,'0',STR_PAD_LEFT);
        return $content ;
    }
}
if (!function_exists('create_slug')) {

     function create_slug($str) {
        $str = strtolower(trim($str));
        $str = str_replace(' ', '-', $str);
        $str = str_replace('-', '_', $str);
        $str = preg_replace('/[^a-z0-9-]/', '_', $str);
        $str = preg_replace('/_+/', "-", $str);
        return $str;
    }

}
/*
 *  function to format print value for debugging
 *  @author Rohit Chittora
 *  @created on 27July2015
 */
if(!function_exists('_p')){
    
    function _p($data,$flag=0){
        echo'<pre>';
        print_r($data);
        ($flag==0)?'':die('done') ;
    }
}

if(!function_exists('imageexist')){
    
    function imageexist($path = null) {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        
        if($ext=='')
        return false ;   
        $extcheck = strtolower($ext);
        $file = array('jpeg','jpg','gif','png');
        
        if(!in_array($extcheck,$file))
        return false ;
        
        if (file_exists($path)) {
            return true;
        } else {
            return false;
        }
    }

}



 /*
     * Print for Debug for developers only
     */
     function pr($pr, $is_exit = true) {
        if ($is_exit == true) {
            print '<pre>';
            print_r($pr);
            echo "<pre>";
            exit;
        } else {
            print '<pre>';
            print_r($pr);
            echo "<pre>";
        }
    }
/**
 * Common Function for Limited Char
 */

    function limited_char($str,$limit) {
        if(strlen($str) > $limit) {
            return substr($str, 0 ,$limit) . "...";
        } else {
            return  $str;
        }
    }
    
    function checkAvailibity ($option,$date) {
        $flag = true;
        if(strtotime(date('d-m-Y')) > strtotime($date)) {
            $flag = false;
        }
        
        if(!in_array(strtolower(date('l',  strtotime($date))), json_decode($option['available_days']))) {
            $flag = false;
        }
        if(isset($option['date_varies']) && $option['date_varies'] == 1 ) {
            $loopflag = false;
            foreach ($option['priceData'] as $optionPrice ) {
                if(strtotime($optionPrice['start_date']) <= strtotime($date) && strtotime($optionPrice['end_date']) >= strtotime($date)) {
                    $loopflag = true;
                }
            }
        }
        if(isset($loopflag) && $loopflag == false) {
            $flag = false;
        }
        if(isset($option['notAvailableDays']) &&  !empty($option['notAvailableDays'])) {
            foreach ($option['notAvailableDays'] as $notAvilable) {
                if(date('Y-m-d',strtotime($notAvilable)) == date('Y-m-d',strtotime($date))) {
                    $flag = false;
                }
            }
        }
        if(isset($option['specialDates']) &&  !empty($option['specialDates'])) {
            foreach ($option['specialDates'] as $splDate) {
                if(date('Y-m-d',strtotime($splDate)) == date('Y-m-d',strtotime($date))) {
                    $flag = false;
                }
            }
        }
        return $flag;
    }
    
    /* @ Common function for money format
     * 
     */
    if(!function_exists('number_pattern')){
        function number_pattern($data=null){
            //$pattern = sprintf('%0.0f', $data); 
            $pattern = ceil($data); 
            return $pattern ;
        }
    }
    
    if(!function_exists('money_pattern')){
        function money_pattern($data=null){
            //$pattern = sprintf('%0.2f', $data); 
            $pattern = ceil($data); 
            return $pattern;
        }
    }
    
    if (!function_exists('chkurl')) {

        function chkurl($data = null) {
            return '16';
        }

    }
    
    if (!function_exists('getISO')) {

        function getISO($data = null) {
            return 'AUS';
        }

    }

    if (!function_exists('__red')) {
    function __red($url = '') {
        if($url != '')
        {
            //echo "hi";exit;
            header("Location: http://127.0.0.1:".PORT."/index.php/".$url);
        }
    }

    if (!function_exists('__gurl')) {
    function __gurl($url = '') {
        if($url != '')
        {
            return CUS_HOST.PORT."/index.php/".$url;
        }
    }
    }
}
?>